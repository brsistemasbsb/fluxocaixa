unit UnPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Phys.PGDef,
  FireDAC.Phys.PG, Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Comp.DataSet, Math, FuncoesDLL, StrUtils, IniFiles, Vcl.DBCtrls, Vcl.Mask, blcksock, DateUtils,
  Vcl.WinXPickers, Vcl.WinXCalendars, Vcl.Samples.Calendar;

type
  TFormPrincipal = class(TForm)

    PanelTitulo: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGridDiario: TDBGrid;
    GroupBox1: TGroupBox;
    cbApr: TComboBox;
    StaticText1: TStaticText;
    Apresenta��o: TStaticText;
    bbSelecionar: TButton;
    DBGrid1: TDBGrid;
    GroupBox2: TGroupBox;
    cbAprM: TComboBox;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    bbSelecionarMes: TButton;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    bbImprimir: TBitBtn;
    bbImprimirMensal: TBitBtn;
    Label2: TLabel;
    EditCxCod: TEdit;
    bbCx: TBitBtn;
    EditCxDes: TEdit;
    DTPDe: TDateTimePicker;
    DTPDeM: TDateTimePicker;
    StaticText4: TStaticText;
    cbTipo: TComboBox;
    StaticText5: TStaticText;
    cbFormatoDia: TComboBox;
    Label1: TLabel;
    EditCxCodM: TEdit;
    bbCxM: TBitBtn;
    EditCxDesM: TEdit;
    StaticText6: TStaticText;
    cbTipoM: TComboBox;
    StaticText7: TStaticText;
    cgFormatoMensal: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure bbSelecionarClick(Sender: TObject);
    procedure DBGridDiarioDblClick(Sender: TObject);
    procedure bbSelecionarMesClick(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGridDiarioDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure bbImprimirClick(Sender: TObject);
    procedure bbImprimirMensalClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure EditCxCodChange(Sender: TObject);
    procedure bbCxClick(Sender: TObject);
    procedure bbCxMClick(Sender: TObject);
    procedure DBGridDiarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure selecionarDia;
    procedure selecionarMes;
  end;

var
  FormPrincipal: TFormPrincipal;
  Servidor: String;
  Porta: String;
  Usuario: String;
  Senha: String;
  Banco: String;

  CodigoEmpresaLogada: Integer;
  CodigoFuncionarioLogado: Integer;

  UsuarioLogado: String;
  Outros: String;

  CodigoBusca: Integer;
  DescricaoBusca: String;

implementation

{$R *.dfm}

uses UnDM, Printers, UnDetalhe, UnBusca, UnRelatorioJasper;

procedure TFormPrincipal.selecionarMes;
begin
     DM.FDQueryFluxoM.DisableControls;
     DM.FDQueryFluxoM.Close;
     DM.FDQueryFluxoM.SQL.Clear;
     DM.FDQueryFluxoM.SQL.Add('select ');
     DM.FDQueryFluxoM.SQL.Add('c.ano, ');
     DM.FDQueryFluxoM.SQL.Add('case ');
     DM.FDQueryFluxoM.SQL.Add('     when '+IntToStr(cbAprM.ItemIndex)+'=0 then classifconta ');
     DM.FDQueryFluxoM.SQL.Add('     when '+IntToStr(cbAprM.ItemIndex)+'=1 then grupoconta ');
     DM.FDQueryFluxoM.SQL.Add('     when '+IntToStr(cbAprM.ItemIndex)+'=2 then conta ');
     DM.FDQueryFluxoM.SQL.Add('end as apr, ');
     DM.FDQueryFluxoM.SQL.Add('ordem, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 1 then coalesce (c.vlnat,0) end ,0)) as vl_01, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 2 then coalesce (c.vlnat,0) end ,0)) as vl_02, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 3 then coalesce (c.vlnat,0) end ,0)) as vl_03, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 4 then coalesce (c.vlnat,0) end ,0)) as vl_04, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 5 then coalesce (c.vlnat,0) end ,0)) as vl_05, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 6 then coalesce (c.vlnat,0) end ,0)) as vl_06, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 7 then coalesce (c.vlnat,0) end ,0)) as vl_07, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 8 then coalesce (c.vlnat,0) end ,0)) as vl_08, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 9 then coalesce (c.vlnat,0) end ,0)) as vl_09, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 10 then coalesce (c.vlnat,0) end ,0)) as vl_10, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 11 then coalesce (c.vlnat,0) end ,0)) as vl_11, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce(case when c.mes = 12 then coalesce (c.vlnat,0) end ,0)) as vl_12, ');
     DM.FDQueryFluxoM.SQL.Add('sum (coalesce (c.vlnat,0)) as vl_total ');
     DM.FDQueryFluxoM.SQL.Add('from  ( ');
     DM.FDQueryFluxoM.SQL.Add('select ');
     DM.FDQueryFluxoM.SQL.Add('	cliente_id, ');
     DM.FDQueryFluxoM.SQL.Add('	filial_id, ');
     DM.FDQueryFluxoM.SQL.Add('	to_char(dtconc,''yyyy'')::integer as ano, ');
     DM.FDQueryFluxoM.SQL.Add('	to_char(dtconc,''mm'')::integer as mes, ');
     DM.FDQueryFluxoM.SQL.Add('	to_char(dtconc,''dd'')::integer as dia, ');
     DM.FDQueryFluxoM.SQL.Add('	case when natureza = ''CREDITO'' then 1 else 2 end as ordem, ');
     DM.FDQueryFluxoM.SQL.Add('	vlnat, ');
     DM.FDQueryFluxoM.SQL.Add('	grupoconta_id, ');
     DM.FDQueryFluxoM.SQL.Add('	grupoconta, ');
     DM.FDQueryFluxoM.SQL.Add('	conta_id, ');
     DM.FDQueryFluxoM.SQL.Add('	conta, ');
     DM.FDQueryFluxoM.SQL.Add('	case when classifconta not in (''EMPRESTIMO'',''RECEITA_FINANCEIRA'',''RECEITA'',''CUSTO'',''IMPOSTO_VENDA'',''DESPESA_VENDA'',''DESPESA_FIXA'',''DESPESA_EVENTUAL'' ');
     DM.FDQueryFluxoM.SQL.Add('	,''FUNCIONARIO'',''DESPESA_FINANCEIRA'',''AMORTIZACAO'',''IPI_ST'',''IRPJ_CSLL'',''IMOBILIZADO'',''VEICULO'',''INVESTIMENTO'') then ''OUTROS'' ');
     DM.FDQueryFluxoM.SQL.Add('	else classifconta end as classifconta ');
     DM.FDQueryFluxoM.SQL.Add('	,tipo,caixa_id ');
     DM.FDQueryFluxoM.SQL.Add('from sd.vw_cxmv_dados ');
     DM.FDQueryFluxoM.SQL.Add(')c ');
     DM.FDQueryFluxoM.SQL.Add('where c.ano = '+FormatDateTime('yyyy',DTPDeM.Date)+' ');

     if cbTipoM.ItemIndex = 0
        then
            DM.FDQueryFluxoM.SQL.Add(' and c.tipo = ''CAIXA'' ')
     else
          DM.FDQueryFluxoM.SQL.Add(' and c.tipo = ''DOC'' ');

     if CodigoBusca > 0  then DM.FDQueryFluxoM.SQL.Add(' and c.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoM.SQL.Add('group by c.ano, apr ,ordem ');
     DM.FDQueryFluxoM.SQL.Add('order by ano,ordem ');

     //Mensagem(DM.FDQueryFluxoM.SQL.Text, Informacao);
     DM.FDQueryFluxoM.Open;
     DM.FDQueryFluxoM.EnableControls;


     DM.FDQueryFluxoMTotal.DisableControls;
     DM.FDQueryFluxoMTotal.Close;
     DM.FDQueryFluxoMTotal.SQL.Clear;
     DM.FDQueryFluxoMTotal.SQL.Add('select ');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id, ');
     DM.FDQueryFluxoMTotal.SQL.Add('ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('ordem,');
     DM.FDQueryFluxoMTotal.SQL.Add('cast(apr as varchar),');
     DM.FDQueryFluxoMTotal.SQL.Add('vl_01,vl_02,vl_03,vl_04,vl_05,vl_06,vl_07,vl_08,vl_09,vl_10,');
     DM.FDQueryFluxoMTotal.SQL.Add('vl_11,vl_12,vl_total');
     DM.FDQueryFluxoMTotal.SQL.Add('from');
     DM.FDQueryFluxoMTotal.SQL.Add('(');
     DM.FDQueryFluxoMTotal.SQL.Add('select');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoMTotal.SQL.Add('ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('1 as ordem,');
     DM.FDQueryFluxoMTotal.SQL.Add('''Saldo inicial'' as apr,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 1 then coalesce(vlsaldoinicial,0) end) as vl_01,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 2 then coalesce(vlsaldoinicial,0) end) as vl_02,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 3 then coalesce(vlsaldoinicial,0) end) as vl_03,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 4 then coalesce(vlsaldoinicial,0) end) as vl_04,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 5 then coalesce(vlsaldoinicial,0) end) as vl_05,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 6 then coalesce(vlsaldoinicial,0) end) as vl_06,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 7 then coalesce(vlsaldoinicial,0) end) as vl_07,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 8 then coalesce(vlsaldoinicial,0) end) as vl_08,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 9 then coalesce(vlsaldoinicial,0) end) as vl_09,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 10 then coalesce(vlsaldoinicial,0) end) as vl_10,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 11 then coalesce(vlsaldoinicial,0) end) as vl_11,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 12 then coalesce(vlsaldoinicial,0) end) as vl_12,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlsaldoinicial,0)) as vl_total');
     DM.FDQueryFluxoMTotal.SQL.Add('from (');
     DM.FDQueryFluxoMTotal.SQL.Add('select');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoMTotal.SQL.Add('to_char(dtmov,''yyyy'')::integer as ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('to_char(dtmov,''mm'')::integer as mes,');
     DM.FDQueryFluxoMTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer rows between unbounded preceding and current row)');
     DM.FDQueryFluxoMTotal.SQL.Add('-sum(coalesce(vlmov,0)) as vlsaldoinicial,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlentrada,0)) as vlentrada,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlsaida,0)) as vlsaida,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlmov,0)) as vlmov,');
     DM.FDQueryFluxoMTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer rows between unbounded preceding and current row) as vlsaldo');
     DM.FDQueryFluxoMTotal.SQL.Add(' ,caixa_id');
     DM.FDQueryFluxoMTotal.SQL.Add('from sd.vw_fech_caixa');
     DM.FDQueryFluxoMTotal.SQL.Add('group by cliente_id,filial_id,filial,fantasiafilial,ano,mes,caixa_id');
     DM.FDQueryFluxoMTotal.SQL.Add('order by cliente_id,filial_id,filial,fantasiafilial,ano,mes');
     DM.FDQueryFluxoMTotal.SQL.Add(')a');
     DM.FDQueryFluxoMTotal.SQL.Add('where ano = '+FormatDateTime('yyyy',DTPDeM.Date)+' ');

     if CodigoBusca > 0  then DM.FDQueryFluxoMTotal.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoMTotal.SQL.Add('group by filial_id,ano');
     DM.FDQueryFluxoMTotal.SQL.Add('union');
     DM.FDQueryFluxoMTotal.SQL.Add('select');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoMTotal.SQL.Add('ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('2 as ordem,');
     DM.FDQueryFluxoMTotal.SQL.Add('''Total entradas'' as apr,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 1 then coalesce(vlentrada,0) end) as vl_01,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 2 then coalesce(vlentrada,0) end) as vl_02,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 3 then coalesce(vlentrada,0) end) as vl_03,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 4 then coalesce(vlentrada,0) end) as vl_04,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 5 then coalesce(vlentrada,0) end) as vl_05,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 6 then coalesce(vlentrada,0) end) as vl_06,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 7 then coalesce(vlentrada,0) end) as vl_07,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 8 then coalesce(vlentrada,0) end) as vl_08,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 9 then coalesce(vlentrada,0) end) as vl_09,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 10 then coalesce(vlentrada,0) end) as vl_10,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 11 then coalesce(vlentrada,0) end) as vl_11,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 12 then coalesce(vlentrada,0) end) as vl_12,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlentrada,0)) as vl_total');
     DM.FDQueryFluxoMTotal.SQL.Add('from (');
     DM.FDQueryFluxoMTotal.SQL.Add('select');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoMTotal.SQL.Add('to_char(dtmov,''yyyy'')::integer as ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('to_char(dtmov,''mm'')::integer as mes,');
     DM.FDQueryFluxoMTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer rows between unbounded preceding and current row)');
     DM.FDQueryFluxoMTotal.SQL.Add('-sum(coalesce(vlmov,0)) as vlsaldoinicial,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlentrada,0)) as vlentrada,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlsaida,0)) as vlsaida,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlmov,0)) as vlmov,');
     DM.FDQueryFluxoMTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer rows between unbounded preceding and current row) as vlsaldo');
     DM.FDQueryFluxoMTotal.SQL.Add(' ,caixa_id');
     DM.FDQueryFluxoMTotal.SQL.Add('from sd.vw_fech_caixa');
     DM.FDQueryFluxoMTotal.SQL.Add('group by cliente_id,filial_id,filial,fantasiafilial,ano,mes,caixa_id');
     DM.FDQueryFluxoMTotal.SQL.Add('order by cliente_id,filial_id,filial,fantasiafilial,ano,mes');
     DM.FDQueryFluxoMTotal.SQL.Add(')a');
     DM.FDQueryFluxoMTotal.SQL.Add('where ano = '+FormatDateTime('yyyy',DTPDeM.Date)+' ');

     if CodigoBusca > 0  then DM.FDQueryFluxoMTotal.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoMTotal.SQL.Add('group by filial_id,ano');
     DM.FDQueryFluxoMTotal.SQL.Add('union');
     DM.FDQueryFluxoMTotal.SQL.Add('select');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoMTotal.SQL.Add('ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('3 as ordem,');
     DM.FDQueryFluxoMTotal.SQL.Add('''Total sa�das'' as apr,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 1 then coalesce(vlsaida,0) end) as vl_01,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 2 then coalesce(vlsaida,0) end) as vl_02,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 3 then coalesce(vlsaida,0) end) as vl_03,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 4 then coalesce(vlsaida,0) end) as vl_04,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 5 then coalesce(vlsaida,0) end) as vl_05,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 6 then coalesce(vlsaida,0) end) as vl_06,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 7 then coalesce(vlsaida,0) end) as vl_07,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 8 then coalesce(vlsaida,0) end) as vl_08,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 9 then coalesce(vlsaida,0) end) as vl_09,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 10 then coalesce(vlsaida,0) end) as vl_10,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 11 then coalesce(vlsaida,0) end) as vl_11,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 12 then coalesce(vlsaida,0) end) as vl_12,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlsaida,0)) as vl_total');
     DM.FDQueryFluxoMTotal.SQL.Add('from (');
     DM.FDQueryFluxoMTotal.SQL.Add('select');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoMTotal.SQL.Add('to_char(dtmov,''yyyy'')::integer as ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('to_char(dtmov,''mm'')::integer as mes,');
     DM.FDQueryFluxoMTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer rows between unbounded preceding and current row)');
     DM.FDQueryFluxoMTotal.SQL.Add('-sum(coalesce(vlmov,0)) as vlsaldoinicial,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlentrada,0)) as vlentrada,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlsaida,0)) as vlsaida,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlmov,0)) as vlmov,');
     DM.FDQueryFluxoMTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer rows between unbounded preceding and current row) as vlsaldo');
     DM.FDQueryFluxoMTotal.SQL.Add(' ,caixa_id');
     DM.FDQueryFluxoMTotal.SQL.Add('from sd.vw_fech_caixa');
     DM.FDQueryFluxoMTotal.SQL.Add('group by cliente_id,filial_id,filial,fantasiafilial,ano,mes,caixa_id');
     DM.FDQueryFluxoMTotal.SQL.Add('order by cliente_id,filial_id,filial,fantasiafilial,ano,mes');
     DM.FDQueryFluxoMTotal.SQL.Add(')a');
     DM.FDQueryFluxoMTotal.SQL.Add('where ano = '+FormatDateTime('yyyy',DTPDeM.Date)+'');

     if CodigoBusca > 0  then DM.FDQueryFluxoMTotal.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoMTotal.SQL.Add('group by filial_id,ano');
     DM.FDQueryFluxoMTotal.SQL.Add('union');
     DM.FDQueryFluxoMTotal.SQL.Add('select');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoMTotal.SQL.Add('ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('5 as ordem,');
     DM.FDQueryFluxoMTotal.SQL.Add('''Diferen�a'' as apr,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 1 then coalesce(vlmov,0) end) as vl_01,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 2 then coalesce(vlmov,0) end) as vl_02,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 3 then coalesce(vlmov,0) end) as vl_03,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 4 then coalesce(vlmov,0) end) as vl_04,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 5 then coalesce(vlmov,0) end) as vl_05,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 6 then coalesce(vlmov,0) end) as vl_06,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 7 then coalesce(vlmov,0) end) as vl_07,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 8 then coalesce(vlmov,0) end) as vl_08,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 9 then coalesce(vlmov,0) end) as vl_09,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 10 then coalesce(vlmov,0) end) as vl_10,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 11 then coalesce(vlmov,0) end) as vl_11,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 12 then coalesce(vlmov,0) end) as vl_12,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlmov,0)) as vl_total');
     DM.FDQueryFluxoMTotal.SQL.Add('from (');
     DM.FDQueryFluxoMTotal.SQL.Add('select');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoMTotal.SQL.Add('to_char(dtmov,''yyyy'')::integer as ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('to_char(dtmov,''mm'')::integer as mes,');
     DM.FDQueryFluxoMTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer rows between unbounded preceding and current row)');
     DM.FDQueryFluxoMTotal.SQL.Add('-sum(coalesce(vlmov,0)) as vlsaldoinicial,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlentrada,0)) as vlentrada,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlsaida,0)) as vlsaida,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlmov,0)) as vlmov,');
     DM.FDQueryFluxoMTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer rows between unbounded preceding and current row) as vlsaldo');
     DM.FDQueryFluxoMTotal.SQL.Add(' ,caixa_id');
     DM.FDQueryFluxoMTotal.SQL.Add('from sd.vw_fech_caixa');
     DM.FDQueryFluxoMTotal.SQL.Add('group by cliente_id,filial_id,filial,fantasiafilial,ano,mes,caixa_id');
     DM.FDQueryFluxoMTotal.SQL.Add('order by cliente_id,filial_id,filial,fantasiafilial,ano,mes');
     DM.FDQueryFluxoMTotal.SQL.Add(')a');
     DM.FDQueryFluxoMTotal.SQL.Add('where ano = '+FormatDateTime('yyyy',DTPDeM.Date)+'');

     if CodigoBusca > 0  then DM.FDQueryFluxoMTotal.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoMTotal.SQL.Add('group by filial_id,ano');
     DM.FDQueryFluxoMTotal.SQL.Add('union');
     DM.FDQueryFluxoMTotal.SQL.Add('select');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoMTotal.SQL.Add('ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('4 as ordem,');
     DM.FDQueryFluxoMTotal.SQL.Add('''Saldo final'' as apr,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 1 then coalesce(vlsaldo,0) end) as vl_01,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 2 then coalesce(vlsaldo,0) end) as vl_02,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 3 then coalesce(vlsaldo,0) end) as vl_03,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 4 then coalesce(vlsaldo,0) end) as vl_04,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 5 then coalesce(vlsaldo,0) end) as vl_05,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 6 then coalesce(vlsaldo,0) end) as vl_06,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 7 then coalesce(vlsaldo,0) end) as vl_07,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 8 then coalesce(vlsaldo,0) end) as vl_08,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 9 then coalesce(vlsaldo,0) end) as vl_09,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 10 then coalesce(vlsaldo,0) end) as vl_10,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 11 then coalesce(vlsaldo,0) end) as vl_11,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 12 then coalesce(vlsaldo,0) end) as vl_12,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(case when mes = 12 then coalesce(vlsaldo,0) end) as vl_total');
     DM.FDQueryFluxoMTotal.SQL.Add('from (');
     DM.FDQueryFluxoMTotal.SQL.Add('select');
     DM.FDQueryFluxoMTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoMTotal.SQL.Add('to_char(dtmov,''yyyy'')::integer as ano,');
     DM.FDQueryFluxoMTotal.SQL.Add('to_char(dtmov,''mm'')::integer as mes,');
     DM.FDQueryFluxoMTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer rows between unbounded preceding and current row)');
     DM.FDQueryFluxoMTotal.SQL.Add('-sum(coalesce(vlmov,0)) as vlsaldoinicial,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlentrada,0)) as vlentrada,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlsaida,0)) as vlsaida,');
     DM.FDQueryFluxoMTotal.SQL.Add('sum(coalesce(vlmov,0)) as vlmov,');
     DM.FDQueryFluxoMTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer rows between unbounded preceding and current row) as vlsaldo');
     DM.FDQueryFluxoMTotal.SQL.Add(' ,caixa_id');
     DM.FDQueryFluxoMTotal.SQL.Add('from sd.vw_fech_caixa');
     DM.FDQueryFluxoMTotal.SQL.Add('group by cliente_id,filial_id,filial,fantasiafilial,ano,mes,caixa_id');
     DM.FDQueryFluxoMTotal.SQL.Add('order by cliente_id,filial_id,filial,fantasiafilial,ano,mes');
     DM.FDQueryFluxoMTotal.SQL.Add(')a');
     DM.FDQueryFluxoMTotal.SQL.Add('where ano = '+FormatDateTime('yyyy',DTPDeM.Date)+'');

     if CodigoBusca > 0  then DM.FDQueryFluxoMTotal.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoMTotal.SQL.Add('group by filial_id,ano');
     DM.FDQueryFluxoMTotal.SQL.Add(')a');
     DM.FDQueryFluxoMTotal.SQL.Add('order by ano,ordem');
     DM.FDQueryFluxoMTotal.Open;
     DM.FDQueryFluxoMTotal.EnableControls;


end;

procedure TFormPrincipal.selecionarDia;
begin
     DM.FDQueryFluxo.DisableControls;
     DM.FDQueryFluxo.Close;
     DM.FDQueryFluxo.SQL.Clear;
     DM.FDQueryFluxo.SQL.Add('select ');
     DM.FDQueryFluxo.SQL.Add('c.ano, ');
     DM.FDQueryFluxo.SQL.Add('c.mes, ');
     DM.FDQueryFluxo.SQL.Add('case ');
     DM.FDQueryFluxo.SQL.Add('     when '+IntToStr(cbApr.ItemIndex)+'=0 then classifconta ');
     DM.FDQueryFluxo.SQL.Add('     when '+IntToStr(cbApr.ItemIndex)+'=1 then cast(grupoconta_id as varchar) ');
     DM.FDQueryFluxo.SQL.Add('     when '+IntToStr(cbApr.ItemIndex)+'=2 then cast(grupoconta_id as varchar) ');
     DM.FDQueryFluxo.SQL.Add('end as apr_cod, ');
     DM.FDQueryFluxo.SQL.Add('case ');
     DM.FDQueryFluxo.SQL.Add('     when '+IntToStr(cbApr.ItemIndex)+'=0 then classifconta ');
     DM.FDQueryFluxo.SQL.Add('     when '+IntToStr(cbApr.ItemIndex)+'=1 then grupoconta ');
     DM.FDQueryFluxo.SQL.Add('     when '+IntToStr(cbApr.ItemIndex)+'=2 then conta ');
     DM.FDQueryFluxo.SQL.Add('end as apr, ');
     //DM.FDQueryFluxo.SQL.Add('ordem, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 1 then coalesce (c.vlnat,0) end ,0)) as vl_01, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 2 then coalesce (c.vlnat,0) end ,0)) as vl_02, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 3 then coalesce (c.vlnat,0) end ,0)) as vl_03, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 4 then coalesce (c.vlnat,0) end ,0)) as vl_04, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 5 then coalesce (c.vlnat,0) end ,0)) as vl_05, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 6 then coalesce (c.vlnat,0) end ,0)) as vl_06, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 7 then coalesce (c.vlnat,0) end ,0)) as vl_07, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 8 then coalesce (c.vlnat,0) end ,0)) as vl_08, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 9 then coalesce (c.vlnat,0) end ,0)) as vl_09, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 10 then coalesce (c.vlnat,0) end ,0)) as vl_10, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 11 then coalesce (c.vlnat,0) end ,0)) as vl_11, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 12 then coalesce (c.vlnat,0) end ,0)) as vl_12, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 13 then coalesce (c.vlnat,0) end ,0)) as vl_13, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 14 then coalesce (c.vlnat,0) end ,0)) as vl_14, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 15 then coalesce (c.vlnat,0) end ,0)) as vl_15, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 16 then coalesce (c.vlnat,0) end ,0)) as vl_16, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 17 then coalesce (c.vlnat,0) end ,0)) as vl_17, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 18 then coalesce (c.vlnat,0) end ,0)) as vl_18, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 19 then coalesce (c.vlnat,0) end ,0)) as vl_19, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 20 then coalesce (c.vlnat,0) end ,0)) as vl_20, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 21 then coalesce (c.vlnat,0) end ,0)) as vl_21, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 22 then coalesce (c.vlnat,0) end ,0)) as vl_22, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 23 then coalesce (c.vlnat,0) end ,0)) as vl_23, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 24 then coalesce (c.vlnat,0) end ,0)) as vl_24, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 25 then coalesce (c.vlnat,0) end ,0)) as vl_25, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 26 then coalesce (c.vlnat,0) end ,0)) as vl_26, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 27 then coalesce (c.vlnat,0) end ,0)) as vl_27, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 28 then coalesce (c.vlnat,0) end ,0)) as vl_28, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 29 then coalesce (c.vlnat,0) end ,0)) as vl_29, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 30 then coalesce (c.vlnat,0) end ,0)) as vl_30, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(case when c.dia = 31 then coalesce (c.vlnat,0) end ,0)) as vl_31, ');
     DM.FDQueryFluxo.SQL.Add('sum (coalesce(c.vlnat,0)) as vl_total ');
     DM.FDQueryFluxo.SQL.Add('from  ( ');
     DM.FDQueryFluxo.SQL.Add('select ');
     DM.FDQueryFluxo.SQL.Add('	cliente_id, ');
     DM.FDQueryFluxo.SQL.Add('	filial_id, ');
     DM.FDQueryFluxo.SQL.Add('	to_char(dtconc,''yyyy'')::integer as ano, ');
     DM.FDQueryFluxo.SQL.Add('	to_char(dtconc,''mm'')::integer as mes, ');
     DM.FDQueryFluxo.SQL.Add('	to_char(dtconc,''dd'')::integer as dia, ');
     DM.FDQueryFluxo.SQL.Add('	case when natureza = ''CREDITO'' then 1 else 2 end as ordem, ');
     DM.FDQueryFluxo.SQL.Add('	vlnat, ');
     DM.FDQueryFluxo.SQL.Add('	grupoconta_id, ');
     DM.FDQueryFluxo.SQL.Add('	grupoconta, ');
     DM.FDQueryFluxo.SQL.Add('	conta_id, ');
     DM.FDQueryFluxo.SQL.Add('	conta, ');
     DM.FDQueryFluxo.SQL.Add('	tipo, ');
     DM.FDQueryFluxo.SQL.Add('	caixa_id, ');
     DM.FDQueryFluxo.SQL.Add('	case when classifconta not in (''EMPRESTIMO'',''RECEITA_FINANCEIRA'',''RECEITA'',''CUSTO'',''IMPOSTO_VENDA'',''DESPESA_VENDA'',''DESPESA_FIXA'',''DESPESA_EVENTUAL'' ');
     DM.FDQueryFluxo.SQL.Add('	,''FUNCIONARIO'',''DESPESA_FINANCEIRA'',''AMORTIZACAO'',''IPI_ST'',''IRPJ_CSLL'',''IMOBILIZADO'',''VEICULO'',''INVESTIMENTO'') then ''OUTROS'' ');
     DM.FDQueryFluxo.SQL.Add('	else classifconta end as classifconta ');
     DM.FDQueryFluxo.SQL.Add('from sd.vw_cxmv_dados ');
     DM.FDQueryFluxo.SQL.Add(')c ');
     DM.FDQueryFluxo.SQL.Add('where c.ano = '+FormatDateTime('yyyy',DTPDe.Date)+' and c.mes = '+FormatDateTime('MM',DTPDe.Date)+' ');

     if cbTipo.ItemIndex = 0
        then
            DM.FDQueryFluxo.SQL.Add(' and c.tipo = ''CAIXA'' ')
     else
          DM.FDQueryFluxo.SQL.Add(' and c.tipo = ''DOC'' ');

     if CodigoBusca > 0  then DM.FDQueryFluxo.SQL.Add(' and c.caixa_id = '+IntToStr(CodigoBusca)+' ');


     DM.FDQueryFluxo.SQL.Add('group by c.ano,c.mes,apr_cod,apr  ');
     DM.FDQueryFluxo.SQL.Add('order by ano,mes,apr_cod ');

     //Mensagem(DM.FDQueryFluxo.SQL.Text, Informacao);

     DM.FDQueryFluxo.Open;
     DM.FDQueryFluxo.EnableControls;



     DM.FDQueryFluxoTotal.DisableControls;
     DM.FDQueryFluxoTotal.Close;
     DM.FDQueryFluxoTotal.SQL.Clear;
     DM.FDQueryFluxoTotal.SQL.Add('select ');
     DM.FDQueryFluxoTotal.SQL.Add('ano,');
     DM.FDQueryFluxoTotal.SQL.Add('mes,');
     DM.FDQueryFluxoTotal.SQL.Add('ordem,');
     DM.FDQueryFluxoTotal.SQL.Add('cast(apr as varchar),');
     DM.FDQueryFluxoTotal.SQL.Add('vl_01,vl_02,vl_03,vl_04,vl_05,vl_06,vl_07,vl_08,vl_09,vl_10,');
     DM.FDQueryFluxoTotal.SQL.Add('vl_11,vl_12,vl_13,vl_14,vl_15,vl_16,vl_17,vl_18,vl_19,vl_20,');
     DM.FDQueryFluxoTotal.SQL.Add('vl_21,vl_22,vl_23,vl_24,vl_25,vl_26,vl_27,vl_28,vl_29,vl_30,vl_31,vl_total');
     DM.FDQueryFluxoTotal.SQL.Add('from');
     DM.FDQueryFluxoTotal.SQL.Add('(');
     DM.FDQueryFluxoTotal.SQL.Add('select');
     DM.FDQueryFluxoTotal.SQL.Add('ano,');
     DM.FDQueryFluxoTotal.SQL.Add('mes,');
     DM.FDQueryFluxoTotal.SQL.Add('1 as ordem,');
     DM.FDQueryFluxoTotal.SQL.Add('''Saldo inicial'' as apr,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 1 then vlsaldoinicial end) as vl_01,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 2 then vlsaldoinicial end) as vl_02,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 3 then vlsaldoinicial end) as vl_03,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 4 then vlsaldoinicial end) as vl_04,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 5 then vlsaldoinicial end) as vl_05,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 6 then vlsaldoinicial end) as vl_06,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 7 then vlsaldoinicial end) as vl_07,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 8 then vlsaldoinicial end) as vl_08,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 9 then vlsaldoinicial end) as vl_09,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 10 then vlsaldoinicial end) as vl_10,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 11 then vlsaldoinicial end) as vl_11,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 12 then vlsaldoinicial end) as vl_12,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 13 then vlsaldoinicial end) as vl_13,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 14 then vlsaldoinicial end) as vl_14,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 15 then vlsaldoinicial end) as vl_15,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 16 then vlsaldoinicial end) as vl_16,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 17 then vlsaldoinicial end) as vl_17,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 18 then vlsaldoinicial end) as vl_18,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 19 then vlsaldoinicial end) as vl_19,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 20 then vlsaldoinicial end) as vl_20,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 21 then vlsaldoinicial end) as vl_21,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 22 then vlsaldoinicial end) as vl_22,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 23 then vlsaldoinicial end) as vl_23,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 24 then vlsaldoinicial end) as vl_24,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 25 then vlsaldoinicial end) as vl_25,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 26 then vlsaldoinicial end) as vl_26,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 27 then vlsaldoinicial end) as vl_27,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 28 then vlsaldoinicial end) as vl_28,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 29 then vlsaldoinicial end) as vl_29,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 30 then vlsaldoinicial end) as vl_30,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 31 then vlsaldoinicial end) as vl_31,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 1 then vlsaldoinicial end) as vl_total');
     DM.FDQueryFluxoTotal.SQL.Add('from (');
     DM.FDQueryFluxoTotal.SQL.Add('select');
     DM.FDQueryFluxoTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''yyyy'')::integer as ano,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''mm'')::integer as mes,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''dd'')::integer as dia,');
     //DM.FDQueryFluxoTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY cliente_id,filial_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer,to_char(dtmov,''dd'')::integer rows between unbounded preceding and current row)');
     //DM.FDQueryFluxoTotal.SQL.Add('-sum(coalesce(vlmov,0)) as vlsaldoinicial');
     DM.FDQueryFluxoTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer,to_char(dtmov,''dd'')::integer rows between unbounded preceding and current row)-sum(coalesce(vlmov,0)) as vlsaldoinicial ');
     DM.FDQueryFluxoTotal.SQL.Add(',caixa_id ');
     DM.FDQueryFluxoTotal.SQL.Add('from sd.vw_fech_caixa');
     DM.FDQueryFluxoTotal.SQL.Add('group by cliente_id,filial_id,filial,fantasiafilial,ano,mes,dia,caixa_id');
     DM.FDQueryFluxoTotal.SQL.Add('order by cliente_id,filial_id,filial,fantasiafilial,ano,mes,dia');
     DM.FDQueryFluxoTotal.SQL.Add(')a');
     DM.FDQueryFluxoTotal.SQL.Add('where ano = '+FormatDateTime('yyyy',DTPDe.Date)+' and mes = '+FormatDateTime('MM',DTPDe.Date)+'');

     if CodigoBusca > 0  then DM.FDQueryFluxoTotal.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoTotal.SQL.Add('group by ano,mes');
     DM.FDQueryFluxoTotal.SQL.Add('union');
     DM.FDQueryFluxoTotal.SQL.Add('select');
     DM.FDQueryFluxoTotal.SQL.Add('ano,');
     DM.FDQueryFluxoTotal.SQL.Add('mes,');
     DM.FDQueryFluxoTotal.SQL.Add('2 as ordem,');
     DM.FDQueryFluxoTotal.SQL.Add('''Total entradas'' as apr,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 1 then vlentrada end) as vl_01,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 2 then vlentrada end) as vl_02,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 3 then vlentrada end) as vl_03,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 4 then vlentrada end) as vl_04,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 5 then vlentrada end) as vl_05,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 6 then vlentrada end) as vl_06,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 7 then vlentrada end) as vl_07,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 8 then vlentrada end) as vl_08,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 9 then vlentrada end) as vl_09,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 10 then vlentrada end) as vl_10,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 11 then vlentrada end) as vl_11,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 12 then vlentrada end) as vl_12,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 13 then vlentrada end) as vl_13,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 14 then vlentrada end) as vl_14,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 15 then vlentrada end) as vl_15,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 16 then vlentrada end) as vl_16,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 17 then vlentrada end) as vl_17,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 18 then vlentrada end) as vl_18,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 19 then vlentrada end) as vl_19,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 20 then vlentrada end) as vl_20,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 21 then vlentrada end) as vl_21,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 22 then vlentrada end) as vl_22,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 23 then vlentrada end) as vl_23,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 24 then vlentrada end) as vl_24,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 25 then vlentrada end) as vl_25,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 26 then vlentrada end) as vl_26,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 27 then vlentrada end) as vl_27,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 28 then vlentrada end) as vl_28,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 29 then vlentrada end) as vl_29,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 30 then vlentrada end) as vl_30,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 31 then vlentrada end) as vl_31,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(vlentrada) as vl_total');
     DM.FDQueryFluxoTotal.SQL.Add('from (');
     DM.FDQueryFluxoTotal.SQL.Add('select');
     DM.FDQueryFluxoTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''yyyy'')::integer as ano,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''mm'')::integer as mes,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''dd'')::integer as dia,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(coalesce(vlentrada,0)) as vlentrada');
     DM.FDQueryFluxoTotal.SQL.Add(',caixa_id ');
     DM.FDQueryFluxoTotal.SQL.Add('from sd.vw_fech_caixa');
     DM.FDQueryFluxoTotal.SQL.Add('group by cliente_id,filial_id,filial,fantasiafilial,ano,mes,dia,caixa_id');
     DM.FDQueryFluxoTotal.SQL.Add('order by cliente_id,filial_id,filial,fantasiafilial,ano,mes,dia');
     DM.FDQueryFluxoTotal.SQL.Add(')a');
     DM.FDQueryFluxoTotal.SQL.Add('where ano = '+FormatDateTime('yyyy',DTPDe.Date)+' and mes = '+FormatDateTime('MM',DTPDe.Date)+'');

     if CodigoBusca > 0  then DM.FDQueryFluxoTotal.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoTotal.SQL.Add('group by ano,mes');
     DM.FDQueryFluxoTotal.SQL.Add('union');
     DM.FDQueryFluxoTotal.SQL.Add('select');
     DM.FDQueryFluxoTotal.SQL.Add('ano,');
     DM.FDQueryFluxoTotal.SQL.Add('mes,');
     DM.FDQueryFluxoTotal.SQL.Add('3 as ordem,');
     DM.FDQueryFluxoTotal.SQL.Add('''Total sa�das'' as apr,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 1 then vlsaida end) as vl_01,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 2 then vlsaida end) as vl_02,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 3 then vlsaida end) as vl_03,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 4 then vlsaida end) as vl_04,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 5 then vlsaida end) as vl_05,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 6 then vlsaida end) as vl_06,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 7 then vlsaida end) as vl_07,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 8 then vlsaida end) as vl_08,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 9 then vlsaida end) as vl_09,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 10 then vlsaida end) as vl_10,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 11 then vlsaida end) as vl_11,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 12 then vlsaida end) as vl_12,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 13 then vlsaida end) as vl_13,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 14 then vlsaida end) as vl_14,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 15 then vlsaida end) as vl_15,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 16 then vlsaida end) as vl_16,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 17 then vlsaida end) as vl_17,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 18 then vlsaida end) as vl_18,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 19 then vlsaida end) as vl_19,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 20 then vlsaida end) as vl_20,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 21 then vlsaida end) as vl_21,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 22 then vlsaida end) as vl_22,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 23 then vlsaida end) as vl_23,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 24 then vlsaida end) as vl_24,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 25 then vlsaida end) as vl_25,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 26 then vlsaida end) as vl_26,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 27 then vlsaida end) as vl_27,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 28 then vlsaida end) as vl_28,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 29 then vlsaida end) as vl_29,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 30 then vlsaida end) as vl_30,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 31 then vlsaida end) as vl_31,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(vlsaida) as vl_total');
     DM.FDQueryFluxoTotal.SQL.Add('from (');
     DM.FDQueryFluxoTotal.SQL.Add('select');
     DM.FDQueryFluxoTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''yyyy'')::integer as ano,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''mm'')::integer as mes,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''dd'')::integer as dia,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(coalesce(vlsaida,0)) as vlsaida');
     DM.FDQueryFluxoTotal.SQL.Add(',caixa_id ');
     DM.FDQueryFluxoTotal.SQL.Add('from sd.vw_fech_caixa');
     DM.FDQueryFluxoTotal.SQL.Add('group by cliente_id,filial_id,filial,fantasiafilial,ano,mes,dia,caixa_id');
     DM.FDQueryFluxoTotal.SQL.Add('order by cliente_id,filial_id,filial,fantasiafilial,ano,mes,dia');
     DM.FDQueryFluxoTotal.SQL.Add(')a');
     DM.FDQueryFluxoTotal.SQL.Add('where ano = '+FormatDateTime('yyyy',DTPDe.Date)+' and mes = '+FormatDateTime('MM',DTPDe.Date)+'');

     if CodigoBusca > 0  then DM.FDQueryFluxoTotal.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoTotal.SQL.Add('group by ano,mes');
     DM.FDQueryFluxoTotal.SQL.Add('union');
     DM.FDQueryFluxoTotal.SQL.Add('select');
     DM.FDQueryFluxoTotal.SQL.Add('ano,');
     DM.FDQueryFluxoTotal.SQL.Add('mes,');
     DM.FDQueryFluxoTotal.SQL.Add('5 as ordem,');
     DM.FDQueryFluxoTotal.SQL.Add('''Diferen�a'' as apr,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 1 then vlmov end) as vl_1,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 2 then vlmov end) as vl_2,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 3 then vlmov end) as vl_3,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 4 then vlmov end) as vl_4,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 5 then vlmov end) as vl_5,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 6 then vlmov end) as vl_6,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 7 then vlmov end) as vl_7,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 8 then vlmov end) as vl_8,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 9 then vlmov end) as vl_9,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 10 then vlmov end) as vl_10,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 11 then vlmov end) as vl_11,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 12 then vlmov end) as vl_12,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 13 then vlmov end) as vl_13,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 14 then vlmov end) as vl_14,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 15 then vlmov end) as vl_15,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 16 then vlmov end) as vl_16,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 17 then vlmov end) as vl_17,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 18 then vlmov end) as vl_18,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 19 then vlmov end) as vl_19,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 20 then vlmov end) as vl_20,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 21 then vlmov end) as vl_21,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 22 then vlmov end) as vl_22,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 23 then vlmov end) as vl_23,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 24 then vlmov end) as vl_24,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 25 then vlmov end) as vl_25,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 26 then vlmov end) as vl_26,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 27 then vlmov end) as vl_27,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 28 then vlmov end) as vl_28,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 29 then vlmov end) as vl_29,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 30 then vlmov end) as vl_30,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 31 then vlmov end) as vl_31,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(vlmov) as vl_total');
     DM.FDQueryFluxoTotal.SQL.Add('from (');
     DM.FDQueryFluxoTotal.SQL.Add('select');
     DM.FDQueryFluxoTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''yyyy'')::integer as ano,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''mm'')::integer as mes,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''dd'')::integer as dia,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(coalesce(vlmov,0)) as vlmov');
     DM.FDQueryFluxoTotal.SQL.Add(',caixa_id ');
     DM.FDQueryFluxoTotal.SQL.Add('from sd.vw_fech_caixa');
     DM.FDQueryFluxoTotal.SQL.Add('group by cliente_id,filial_id,filial,fantasiafilial,ano,mes,dia,caixa_id');
     DM.FDQueryFluxoTotal.SQL.Add('order by cliente_id,filial_id,filial,fantasiafilial,ano,mes,dia');
     DM.FDQueryFluxoTotal.SQL.Add(')a');
     DM.FDQueryFluxoTotal.SQL.Add('where ano = '+FormatDateTime('yyyy',DTPDe.Date)+' and mes = '+FormatDateTime('MM',DTPDe.Date)+'');

     if CodigoBusca > 0  then DM.FDQueryFluxoTotal.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoTotal.SQL.Add('group by ano,mes');
     DM.FDQueryFluxoTotal.SQL.Add('union');
     DM.FDQueryFluxoTotal.SQL.Add('select');
     DM.FDQueryFluxoTotal.SQL.Add('ano,');
     DM.FDQueryFluxoTotal.SQL.Add('mes,');
     DM.FDQueryFluxoTotal.SQL.Add('4 as ordem,');
     DM.FDQueryFluxoTotal.SQL.Add('''Saldo final'' as apr,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 1 then vlsaldo end) as vl_1,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 2 then vlsaldo end) as vl_2,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 3 then vlsaldo end) as vl_3,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 4 then vlsaldo end) as vl_4,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 5 then vlsaldo end) as vl_5,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 6 then vlsaldo end) as vl_6,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 7 then vlsaldo end) as vl_7,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 8 then vlsaldo end) as vl_8,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 9 then vlsaldo end) as vl_9,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 10 then vlsaldo end) as vl_10,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 11 then vlsaldo end) as vl_11,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 12 then vlsaldo end) as vl_12,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 13 then vlsaldo end) as vl_13,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 14 then vlsaldo end) as vl_14,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 15 then vlsaldo end) as vl_15,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 16 then vlsaldo end) as vl_16,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 17 then vlsaldo end) as vl_17,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 18 then vlsaldo end) as vl_18,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 19 then vlsaldo end) as vl_19,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 20 then vlsaldo end) as vl_20,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 21 then vlsaldo end) as vl_21,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 22 then vlsaldo end) as vl_22,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 23 then vlsaldo end) as vl_23,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 24 then vlsaldo end) as vl_24,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 25 then vlsaldo end) as vl_25,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 26 then vlsaldo end) as vl_26,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 27 then vlsaldo end) as vl_27,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 28 then vlsaldo end) as vl_28,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 29 then vlsaldo end) as vl_29,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 30 then vlsaldo end) as vl_30,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 31 then vlsaldo end) as vl_31,');
     DM.FDQueryFluxoTotal.SQL.Add('sum(case when dia = 31 then vlsaldo end) as vl_total');
     DM.FDQueryFluxoTotal.SQL.Add('from (');
     DM.FDQueryFluxoTotal.SQL.Add('select');
     DM.FDQueryFluxoTotal.SQL.Add('filial_id,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''yyyy'')::integer as ano,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''mm'')::integer as mes,');
     DM.FDQueryFluxoTotal.SQL.Add('to_char(dtmov,''dd'')::integer as dia,');
     //DM.FDQueryFluxoTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY cliente_id,filial_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer,to_char(dtmov,''dd'')::integer rows between unbounded preceding and current row) as vlsaldo');
     DM.FDQueryFluxoTotal.SQL.Add('SUM(sum(coalesce(vlmov,0))) OVER (PARTITION BY caixa_id ORDER BY to_char(dtmov,''yyyy'')::integer,to_char(dtmov,''mm'')::integer,to_char(dtmov,''dd'')::integer rows between unbounded preceding and current row) as vlsaldo ');
     DM.FDQueryFluxoTotal.SQL.Add(',caixa_id ');
     DM.FDQueryFluxoTotal.SQL.Add('from sd.vw_fech_caixa');
     DM.FDQueryFluxoTotal.SQL.Add('group by cliente_id,filial_id,filial,fantasiafilial,ano,mes,dia,caixa_id');
     DM.FDQueryFluxoTotal.SQL.Add('order by cliente_id,filial_id,filial,fantasiafilial,ano,mes,dia');
     DM.FDQueryFluxoTotal.SQL.Add(')a');
     DM.FDQueryFluxoTotal.SQL.Add('where ano = '+FormatDateTime('yyyy',DTPDe.Date)+' and mes = '+FormatDateTime('MM',DTPDe.Date)+'');

     if CodigoBusca > 0  then DM.FDQueryFluxoTotal.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

     DM.FDQueryFluxoTotal.SQL.Add('group by ano,mes');
     DM.FDQueryFluxoTotal.SQL.Add(')a');
     DM.FDQueryFluxoTotal.SQL.Add('order by ano,mes,ordem');
     DM.FDQueryFluxoTotal.Open;
     //Mensagem(DM.FDQueryFluxoTotal.Text, Informacao);
     DM.FDQueryFluxoTotal.EnableControls;
end;

procedure TFormPrincipal.bbCxMClick(Sender: TObject);
begin
     DM.FDQueryBusca.Close;
     DM.FDQueryBusca.SQL.Clear;
     DM.FDQueryBusca.SQL.Add('select cxcod as cod, cast (cxdes as varchar) as des');
     DM.FDQueryBusca.SQL.Add('from cx ');
     DM.FDQueryBusca.SQL.Add('order by cxcod ');
     DM.FDQueryBusca.Open;

     DM.FDQueryBusca.Filtered:=False;

     FormBusca.ShowModal;

     if CodigoBusca >0
        then
            begin
                 EditCxCodM.Text := IntToStr(CodigoBusca);
                 EditCxDesM.Text := DescricaoBusca;
            end;
end;

procedure TFormPrincipal.bbImprimirClick(Sender: TObject);
var
   formato: TFormatoSaida;
begin
     with TRelatorioVisualizador.Create('th_fluxo_caixa_diario') do
     begin
          //Obter par�metros
          addParametro('P_DTANO',FormatDateTime('yyyy',DTPDe.Date));
          addParametro('P_DTMES',FormatDateTime('MM',DTPDe.Date));
          addParametro('P_APR',cbApr.ItemIndex);
          addParametro('P_USU',CodigoFuncionarioLogado);


          //Obter formato
          case cbFormatoDia.ItemIndex of
          0:formato:=tsVisualizacao;
          1:formato:=tsPDF;
          2:formato:=tsXLS;
          end;
          executar(formato);

     end;

end;

procedure TFormPrincipal.bbImprimirMensalClick(Sender: TObject);
var
   formato: TFormatoSaida;
begin
     with TRelatorioVisualizador.Create('th_fluxo_caixa_mensal') do
     begin
          //Obter par�metros
          addParametro('P_DTANO',FormatDateTime('yyyy',DTPDeM.Date));
          addParametro('P_APR',cbAprM.ItemIndex);
          addParametro('P_USU',CodigoFuncionarioLogado);


          //Obter formato
          case cgFormatoMensal.ItemIndex of
          0:formato:=tsVisualizacao;
          1:formato:=tsPDF;
          2:formato:=tsXLS;
          end;
          executar(formato);

     end;
end;

procedure TFormPrincipal.bbCxClick(Sender: TObject);
begin
     DM.FDQueryBusca.Close;
     DM.FDQueryBusca.SQL.Clear;
     DM.FDQueryBusca.SQL.Add('select cxcod as cod, cast (cxdes as varchar) as des');
     DM.FDQueryBusca.SQL.Add('from cx ');
     DM.FDQueryBusca.SQL.Add('order by cxcod ');
     DM.FDQueryBusca.Open;

     DM.FDQueryBusca.Filtered:=False;

     FormBusca.ShowModal;

     if CodigoBusca >0
        then
            begin
                 EditCxCod.Text := IntToStr(CodigoBusca);
                 EditCxDes.Text := DescricaoBusca;
            end;
end;

procedure TFormPrincipal.bbSelecionarClick(Sender: TObject);
begin
     selecionarDia;
end;

procedure TFormPrincipal.bbSelecionarMesClick(Sender: TObject);
begin
     selecionarMes;
end;

procedure TFormPrincipal.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     if ((Sender as TDBGrid).DataSource.Dataset.IsEmpty) then
        Exit;

     if (Column.FieldName = 'vl_01') and (DM.FDQueryFluxoTotalvl_01.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_02') and (DM.FDQueryFluxoTotalvl_02.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_03') and (DM.FDQueryFluxoTotalvl_03.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_04') and (DM.FDQueryFluxoTotalvl_04.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_05') and (DM.FDQueryFluxoTotalvl_05.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_06') and (DM.FDQueryFluxoTotalvl_06.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_07') and (DM.FDQueryFluxoTotalvl_07.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_08') and (DM.FDQueryFluxoTotalvl_08.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_09') and (DM.FDQueryFluxoTotalvl_09.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_10') and (DM.FDQueryFluxoTotalvl_10.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_11') and (DM.FDQueryFluxoTotalvl_11.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_12') and (DM.FDQueryFluxoTotalvl_12.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_13') and (DM.FDQueryFluxoTotalvl_13.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_14') and (DM.FDQueryFluxoTotalvl_14.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_15') and (DM.FDQueryFluxoTotalvl_15.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_16') and (DM.FDQueryFluxoTotalvl_16.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_17') and (DM.FDQueryFluxoTotalvl_17.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_18') and (DM.FDQueryFluxoTotalvl_18.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_19') and (DM.FDQueryFluxoTotalvl_19.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_20') and (DM.FDQueryFluxoTotalvl_20.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_21') and (DM.FDQueryFluxoTotalvl_21.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_22') and (DM.FDQueryFluxoTotalvl_22.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_23') and (DM.FDQueryFluxoTotalvl_23.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_24') and (DM.FDQueryFluxoTotalvl_24.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_25') and (DM.FDQueryFluxoTotalvl_25.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_26') and (DM.FDQueryFluxoTotalvl_26.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_27') and (DM.FDQueryFluxoTotalvl_27.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_28') and (DM.FDQueryFluxoTotalvl_28.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_29') and (DM.FDQueryFluxoTotalvl_29.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_30') and (DM.FDQueryFluxoTotalvl_30.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_31') and (DM.FDQueryFluxoTotalvl_31.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_total') and (DM.FDQueryFluxoTotalvl_total.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
end;

procedure TFormPrincipal.DBGrid2DblClick(Sender: TObject);
begin
     try
        //Classifica��o de Conta
        //Grupo de Contas
        //Contas Gerenciais
        case cbAprM.ItemIndex of
        0:begin
             DM.FDQueryDetalhe.Close;
             DM.FDQueryDetalhe.SQL.Clear;
             DM.FDQueryDetalhe.SQL.Add('select * from ( ');
             DM.FDQueryDetalhe.SQL.Add('select ');
             DM.FDQueryDetalhe.SQL.Add('	cliente_id, ');
             DM.FDQueryDetalhe.SQL.Add('	filial_id, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''yyyy'')::integer as ano, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''mm'')::integer as mes, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''dd'')::integer as dia, ');
             DM.FDQueryDetalhe.SQL.Add('	case when natureza = ''CREDITO'' then 1 else 2 end as ordem, ');
             DM.FDQueryDetalhe.SQL.Add('	vlnat, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta, ');
             DM.FDQueryDetalhe.SQL.Add('	conta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	conta, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro_id, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca_id, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca, ');
             DM.FDQueryDetalhe.SQL.Add('	numdocumento, ');
             DM.FDQueryDetalhe.SQL.Add('	parc, ');
             DM.FDQueryDetalhe.SQL.Add('	observacao, ');
             DM.FDQueryDetalhe.SQL.Add('	tipo_movimento, ');
             DM.FDQueryDetalhe.SQL.Add('	case when classifconta not in (''EMPRESTIMO'',''RECEITA_FINANCEIRA'',''RECEITA'',''CUSTO'',''IMPOSTO_VENDA'',''DESPESA_VENDA'',''DESPESA_FIXA'',''DESPESA_EVENTUAL'' ');
             DM.FDQueryDetalhe.SQL.Add('	,''FUNCIONARIO'',''DESPESA_FINANCEIRA'',''AMORTIZACAO'',''IPI_ST'',''IRPJ_CSLL'',''IMOBILIZADO'',''VEICULO'',''INVESTIMENTO'') then ''OUTROS'' ');
             DM.FDQueryDetalhe.SQL.Add('	else classifconta end as classifconta ');
             DM.FDQueryDetalhe.SQL.Add('from sd.vw_cxmv_dados ');
             DM.FDQueryDetalhe.SQL.Add(') a ');
             DM.FDQueryDetalhe.SQL.Add('where a.ano = :0 ');
             DM.FDQueryDetalhe.SQL.Add('and a.mes = :1 ');
             DM.FDQueryDetalhe.SQL.Add('and a.classifconta = :2 ');
             DM.FDQueryDetalhe.Params[0].AsInteger := DM.FDQueryFluxoMano.AsInteger;
             DM.FDQueryDetalhe.Params[1].AsInteger := DBGrid2.Columns.Grid.SelectedIndex;
             DM.FDQueryDetalhe.Params[2].AsString := DM.FDQueryFluxoMapr.AsString;
             DM.FDQueryDetalhe.Open;
          end;

        1:begin
             DM.FDQueryDetalhe.Close;
             DM.FDQueryDetalhe.SQL.Clear;
             DM.FDQueryDetalhe.SQL.Add('select * from ( ');
             DM.FDQueryDetalhe.SQL.Add('select ');
             DM.FDQueryDetalhe.SQL.Add('	cliente_id, ');
             DM.FDQueryDetalhe.SQL.Add('	filial_id, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''yyyy'')::integer as ano, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''mm'')::integer as mes, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''dd'')::integer as dia, ');
             DM.FDQueryDetalhe.SQL.Add('	case when natureza = ''CREDITO'' then 1 else 2 end as ordem, ');
             DM.FDQueryDetalhe.SQL.Add('	vlnat, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta, ');
             DM.FDQueryDetalhe.SQL.Add('	conta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	conta, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro_id, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca_id, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca, ');
             DM.FDQueryDetalhe.SQL.Add('	numdocumento, ');
             DM.FDQueryDetalhe.SQL.Add('	parc, ');
             DM.FDQueryDetalhe.SQL.Add('	observacao, ');
             DM.FDQueryDetalhe.SQL.Add('	tipo_movimento, ');
             DM.FDQueryDetalhe.SQL.Add('	case when classifconta not in (''EMPRESTIMO'',''RECEITA_FINANCEIRA'',''RECEITA'',''CUSTO'',''IMPOSTO_VENDA'',''DESPESA_VENDA'',''DESPESA_FIXA'',''DESPESA_EVENTUAL'' ');
             DM.FDQueryDetalhe.SQL.Add('	,''FUNCIONARIO'',''DESPESA_FINANCEIRA'',''AMORTIZACAO'',''IPI_ST'',''IRPJ_CSLL'',''IMOBILIZADO'',''VEICULO'',''INVESTIMENTO'') then ''OUTROS'' ');
             DM.FDQueryDetalhe.SQL.Add('	else classifconta end as classifconta ');
             DM.FDQueryDetalhe.SQL.Add('from sd.vw_cxmv_dados ');
             DM.FDQueryDetalhe.SQL.Add(') a ');
             DM.FDQueryDetalhe.SQL.Add('where a.ano = :0 ');
             DM.FDQueryDetalhe.SQL.Add('and a.mes = :1 ');
             DM.FDQueryDetalhe.SQL.Add('and a.grupoconta = :2 ');
             DM.FDQueryDetalhe.Params[0].AsInteger := DM.FDQueryFluxoMano.AsInteger;
             DM.FDQueryDetalhe.Params[1].AsInteger := DBGrid2.Columns.Grid.SelectedIndex;
             DM.FDQueryDetalhe.Params[2].AsString := DM.FDQueryFluxoMapr.AsString;
             DM.FDQueryDetalhe.Open;
          end;

        2:begin
             DM.FDQueryDetalhe.Close;
             DM.FDQueryDetalhe.SQL.Clear;
             DM.FDQueryDetalhe.SQL.Add('select * from ( ');
             DM.FDQueryDetalhe.SQL.Add('select ');
             DM.FDQueryDetalhe.SQL.Add('	cliente_id, ');
             DM.FDQueryDetalhe.SQL.Add('	filial_id, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''yyyy'')::integer as ano, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''mm'')::integer as mes, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''dd'')::integer as dia, ');
             DM.FDQueryDetalhe.SQL.Add('	case when natureza = ''CREDITO'' then 1 else 2 end as ordem, ');
             DM.FDQueryDetalhe.SQL.Add('	vlnat, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta, ');
             DM.FDQueryDetalhe.SQL.Add('	conta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	conta, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro_id, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca_id, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca, ');
             DM.FDQueryDetalhe.SQL.Add('	numdocumento, ');
             DM.FDQueryDetalhe.SQL.Add('	parc, ');
             DM.FDQueryDetalhe.SQL.Add('	observacao, ');
             DM.FDQueryDetalhe.SQL.Add('	tipo_movimento, ');
             DM.FDQueryDetalhe.SQL.Add('	case when classifconta not in (''EMPRESTIMO'',''RECEITA_FINANCEIRA'',''RECEITA'',''CUSTO'',''IMPOSTO_VENDA'',''DESPESA_VENDA'',''DESPESA_FIXA'',''DESPESA_EVENTUAL'' ');
             DM.FDQueryDetalhe.SQL.Add('	,''FUNCIONARIO'',''DESPESA_FINANCEIRA'',''AMORTIZACAO'',''IPI_ST'',''IRPJ_CSLL'',''IMOBILIZADO'',''VEICULO'',''INVESTIMENTO'') then ''OUTROS'' ');
             DM.FDQueryDetalhe.SQL.Add('	else classifconta end as classifconta ');
             DM.FDQueryDetalhe.SQL.Add('from sd.vw_cxmv_dados ');
             DM.FDQueryDetalhe.SQL.Add(') a ');
             DM.FDQueryDetalhe.SQL.Add('where a.ano = :0 ');
             DM.FDQueryDetalhe.SQL.Add('and a.mes = :1 ');
             DM.FDQueryDetalhe.SQL.Add('and a.conta = :2 ');
             DM.FDQueryDetalhe.Params[0].AsInteger := DM.FDQueryFluxoMano.AsInteger;
             DM.FDQueryDetalhe.Params[1].AsInteger := DBGrid2.Columns.Grid.SelectedIndex;
             DM.FDQueryDetalhe.Params[2].AsString := DM.FDQueryFluxoMapr.AsString;
             DM.FDQueryDetalhe.Open;
          end;



        end;



        Application.CreateForm(TFormBusca,FormBusca);
        //Mensagem(DBGridDiario.Columns.Grid.SelectedField.Name,Informacao);
        FormDetalhe.Caption := DBGrid2.Columns[DBGrid2.Columns.Grid.SelectedIndex].Title.Caption+' - '+DM.FDQueryFluxoMapr.AsString;
        FormDetalhe.StaticText1.Caption:=FloatToStr(DM.FDQueryFluxoMvl_01.AsFloat); //Columns.Grid.SelectedField.Name;
        //FormBusca.ShowModal;

     except


     end;
end;

procedure TFormPrincipal.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     if ((Sender as TDBGrid).DataSource.Dataset.IsEmpty) then
        Exit;

     if (Column.FieldName = 'vl_01') and (DM.FDQueryFluxoMvl_01.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_02') and (DM.FDQueryFluxoMvl_02.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_03') and (DM.FDQueryFluxoMvl_03.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_04') and (DM.FDQueryFluxoMvl_04.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_05') and (DM.FDQueryFluxoMvl_05.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_06') and (DM.FDQueryFluxoMvl_06.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_07') and (DM.FDQueryFluxoMvl_07.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_08') and (DM.FDQueryFluxoMvl_08.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_09') and (DM.FDQueryFluxoMvl_09.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_10') and (DM.FDQueryFluxoMvl_10.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_11') and (DM.FDQueryFluxoMvl_11.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_12') and (DM.FDQueryFluxoMvl_12.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_total') and (DM.FDQueryFluxoMvl_total.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
end;

procedure TFormPrincipal.DBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     if ((Sender as TDBGrid).DataSource.Dataset.IsEmpty) then
        Exit;

     if (Column.FieldName = 'vl_01') and (DM.FDQueryFluxoMTotalvl_01.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_02') and (DM.FDQueryFluxoMTotalvl_02.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_03') and (DM.FDQueryFluxoMTotalvl_03.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_04') and (DM.FDQueryFluxoMTotalvl_04.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_05') and (DM.FDQueryFluxoMTotalvl_05.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_06') and (DM.FDQueryFluxoMTotalvl_06.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_07') and (DM.FDQueryFluxoMTotalvl_07.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_08') and (DM.FDQueryFluxoMTotalvl_08.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_09') and (DM.FDQueryFluxoMTotalvl_09.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_10') and (DM.FDQueryFluxoMTotalvl_10.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_11') and (DM.FDQueryFluxoMTotalvl_11.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_12') and (DM.FDQueryFluxoMTotalvl_12.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_total') and (DM.FDQueryFluxoMTotalvl_total.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
end;

procedure TFormPrincipal.DBGridDiarioDblClick(Sender: TObject);
begin
     try
        //Classifica��o de Conta
        //Grupo de Contas
        //Contas Gerenciais
        case cbApr.ItemIndex of
        0:begin
             DM.FDQueryDetalhe.Close;
             DM.FDQueryDetalhe.SQL.Clear;
             DM.FDQueryDetalhe.SQL.Add('select * from ( ');
             DM.FDQueryDetalhe.SQL.Add('select ');
             DM.FDQueryDetalhe.SQL.Add('	cliente_id, ');
             DM.FDQueryDetalhe.SQL.Add('	filial_id, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''yyyy'')::integer as ano, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''mm'')::integer as mes, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''dd'')::integer as dia, ');
             DM.FDQueryDetalhe.SQL.Add('	case when natureza = ''CREDITO'' then 1 else 2 end as ordem, ');
             DM.FDQueryDetalhe.SQL.Add('	vlnat, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta, ');
             DM.FDQueryDetalhe.SQL.Add('	conta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	conta, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro_id, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca_id, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca, ');
             DM.FDQueryDetalhe.SQL.Add('	numdocumento, ');
             DM.FDQueryDetalhe.SQL.Add('	parc, ');
             DM.FDQueryDetalhe.SQL.Add('	observacao, ');
             DM.FDQueryDetalhe.SQL.Add('	tipo_movimento, tipo as tipomov, caixa_id,');
             DM.FDQueryDetalhe.SQL.Add('	case when tipo = ''CAIXA'' then cast(''Caixa'' as varchar) else cast(''Previs�o'' as varchar) end as tipo, ');
             DM.FDQueryDetalhe.SQL.Add('	case when classifconta not in (''EMPRESTIMO'',''RECEITA_FINANCEIRA'',''RECEITA'',''CUSTO'',''IMPOSTO_VENDA'',''DESPESA_VENDA'',''DESPESA_FIXA'',''DESPESA_EVENTUAL'' ');
             DM.FDQueryDetalhe.SQL.Add('	,''FUNCIONARIO'',''DESPESA_FINANCEIRA'',''AMORTIZACAO'',''IPI_ST'',''IRPJ_CSLL'',''IMOBILIZADO'',''VEICULO'',''INVESTIMENTO'') then ''OUTROS'' ');
             DM.FDQueryDetalhe.SQL.Add('	else classifconta end as classifconta ');
             DM.FDQueryDetalhe.SQL.Add('from sd.vw_cxmv_dados ');
             DM.FDQueryDetalhe.SQL.Add(') a ');
             DM.FDQueryDetalhe.SQL.Add('where a.ano = :0 ');
             DM.FDQueryDetalhe.SQL.Add('and a.mes = :1 ');
             DM.FDQueryDetalhe.SQL.Add('and a.dia = :2 ');
             DM.FDQueryDetalhe.SQL.Add('and a.classifconta = :3 ');

             if cbTipo.ItemIndex = 0
                then
                    DM.FDQueryDetalhe.SQL.Add(' and a.tipomov = ''CAIXA'' ')
             else
                 DM.FDQueryDetalhe.SQL.Add(' and a.tipomov = ''DOC'' ');

             if CodigoBusca > 0  then DM.FDQueryDetalhe.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

             DM.FDQueryDetalhe.Params[0].AsInteger := DM.FDQueryFluxoano.AsInteger;
             DM.FDQueryDetalhe.Params[1].AsInteger := DM.FDQueryFluxomes.AsInteger;
             DM.FDQueryDetalhe.Params[2].AsInteger := DBGridDiario.Columns.Grid.SelectedIndex;
             DM.FDQueryDetalhe.Params[3].AsString := DM.FDQueryFluxoapr.AsString;
             DM.FDQueryDetalhe.Open;
          end;

        1:begin
             DM.FDQueryDetalhe.Close;
             DM.FDQueryDetalhe.SQL.Clear;
             DM.FDQueryDetalhe.SQL.Add('select * from ( ');
             DM.FDQueryDetalhe.SQL.Add('select ');
             DM.FDQueryDetalhe.SQL.Add('	cliente_id, ');
             DM.FDQueryDetalhe.SQL.Add('	filial_id, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''yyyy'')::integer as ano, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''mm'')::integer as mes, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''dd'')::integer as dia, ');
             DM.FDQueryDetalhe.SQL.Add('	case when natureza = ''CREDITO'' then 1 else 2 end as ordem, ');
             DM.FDQueryDetalhe.SQL.Add('	vlnat, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta, ');
             DM.FDQueryDetalhe.SQL.Add('	conta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	conta, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro_id, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca_id, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca, ');
             DM.FDQueryDetalhe.SQL.Add('	numdocumento, ');
             DM.FDQueryDetalhe.SQL.Add('	parc, ');
             DM.FDQueryDetalhe.SQL.Add('	observacao, ');
             DM.FDQueryDetalhe.SQL.Add('	tipo_movimento, tipo as tipomov, caixa_id,');
             DM.FDQueryDetalhe.SQL.Add('	case when tipo = ''CAIXA'' then cast(''Caixa'' as varchar) else cast(''Previs�o'' as varchar) end as tipo, ');
             DM.FDQueryDetalhe.SQL.Add('	case when classifconta not in (''EMPRESTIMO'',''RECEITA_FINANCEIRA'',''RECEITA'',''CUSTO'',''IMPOSTO_VENDA'',''DESPESA_VENDA'',''DESPESA_FIXA'',''DESPESA_EVENTUAL'' ');
             DM.FDQueryDetalhe.SQL.Add('	,''FUNCIONARIO'',''DESPESA_FINANCEIRA'',''AMORTIZACAO'',''IPI_ST'',''IRPJ_CSLL'',''IMOBILIZADO'',''VEICULO'',''INVESTIMENTO'') then ''OUTROS'' ');
             DM.FDQueryDetalhe.SQL.Add('	else classifconta end as classifconta ');
             DM.FDQueryDetalhe.SQL.Add('from sd.vw_cxmv_dados ');
             DM.FDQueryDetalhe.SQL.Add(') a ');
             DM.FDQueryDetalhe.SQL.Add('where a.ano = :0 ');
             DM.FDQueryDetalhe.SQL.Add('and a.mes = :1 ');
             DM.FDQueryDetalhe.SQL.Add('and a.dia = :2 ');
             DM.FDQueryDetalhe.SQL.Add('and a.grupoconta = :3 ');

             if cbTipo.ItemIndex = 0
                then
                    DM.FDQueryDetalhe.SQL.Add(' and a.tipomov = ''CAIXA'' ')
             else
                 DM.FDQueryDetalhe.SQL.Add(' and a.tipomov = ''DOC'' ');

             if CodigoBusca > 0  then DM.FDQueryDetalhe.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

             DM.FDQueryDetalhe.Params[0].AsInteger := DM.FDQueryFluxoano.AsInteger;
             DM.FDQueryDetalhe.Params[1].AsInteger := DM.FDQueryFluxomes.AsInteger;
             DM.FDQueryDetalhe.Params[2].AsInteger := DBGridDiario.Columns.Grid.SelectedIndex;
             DM.FDQueryDetalhe.Params[3].AsString := DM.FDQueryFluxoapr.AsString;
             DM.FDQueryDetalhe.Open;
          end;

        2:begin
             DM.FDQueryDetalhe.Close;
             DM.FDQueryDetalhe.SQL.Clear;
             DM.FDQueryDetalhe.SQL.Add('select * from ( ');
             DM.FDQueryDetalhe.SQL.Add('select ');
             DM.FDQueryDetalhe.SQL.Add('	cliente_id, ');
             DM.FDQueryDetalhe.SQL.Add('	filial_id, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''yyyy'')::integer as ano, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''mm'')::integer as mes, ');
             DM.FDQueryDetalhe.SQL.Add('	to_char(dtconc,''dd'')::integer as dia, ');
             DM.FDQueryDetalhe.SQL.Add('	case when natureza = ''CREDITO'' then 1 else 2 end as ordem, ');
             DM.FDQueryDetalhe.SQL.Add('	vlnat, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	grupoconta, ');
             DM.FDQueryDetalhe.SQL.Add('	conta_id, ');
             DM.FDQueryDetalhe.SQL.Add('	conta, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro_id, ');
             DM.FDQueryDetalhe.SQL.Add('	parceiro, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca_id, ');
             DM.FDQueryDetalhe.SQL.Add('	cobranca, ');
             DM.FDQueryDetalhe.SQL.Add('	numdocumento, ');
             DM.FDQueryDetalhe.SQL.Add('	parc, ');
             DM.FDQueryDetalhe.SQL.Add('	observacao, ');
             DM.FDQueryDetalhe.SQL.Add('	tipo_movimento, tipo as tipomov, caixa_id,');
             DM.FDQueryDetalhe.SQL.Add('	case when tipo = ''CAIXA'' then cast(''Caixa'' as varchar) else cast(''Previs�o'' as varchar) end as tipo, ');
             DM.FDQueryDetalhe.SQL.Add('	case when classifconta not in (''EMPRESTIMO'',''RECEITA_FINANCEIRA'',''RECEITA'',''CUSTO'',''IMPOSTO_VENDA'',''DESPESA_VENDA'',''DESPESA_FIXA'',''DESPESA_EVENTUAL'' ');
             DM.FDQueryDetalhe.SQL.Add('	,''FUNCIONARIO'',''DESPESA_FINANCEIRA'',''AMORTIZACAO'',''IPI_ST'',''IRPJ_CSLL'',''IMOBILIZADO'',''VEICULO'',''INVESTIMENTO'') then ''OUTROS'' ');
             DM.FDQueryDetalhe.SQL.Add('	else classifconta end as classifconta ');
             DM.FDQueryDetalhe.SQL.Add('from sd.vw_cxmv_dados ');
             DM.FDQueryDetalhe.SQL.Add(') a ');
             DM.FDQueryDetalhe.SQL.Add('where a.ano = :0 ');
             DM.FDQueryDetalhe.SQL.Add('and a.mes = :1 ');
             DM.FDQueryDetalhe.SQL.Add('and a.dia = :2 ');
             DM.FDQueryDetalhe.SQL.Add('and a.conta = :3 ');

             if cbTipo.ItemIndex = 0
                then
                    DM.FDQueryDetalhe.SQL.Add(' and a.tipomov = ''CAIXA'' ')
             else
                 DM.FDQueryDetalhe.SQL.Add(' and a.tipomov = ''DOC'' ');

             if CodigoBusca > 0  then DM.FDQueryDetalhe.SQL.Add(' and a.caixa_id = '+IntToStr(CodigoBusca)+' ');

             DM.FDQueryDetalhe.Params[0].AsInteger := DM.FDQueryFluxoano.AsInteger;
             DM.FDQueryDetalhe.Params[1].AsInteger := DM.FDQueryFluxomes.AsInteger;
             DM.FDQueryDetalhe.Params[2].AsInteger := DBGridDiario.Columns.Grid.SelectedIndex;
             DM.FDQueryDetalhe.Params[3].AsString := DM.FDQueryFluxoapr.AsString;
             DM.FDQueryDetalhe.Open;
          end;



        end;


        //Mensagem(DM.FDQueryDetalhe.SQL.Text,Informacao);

        Application.CreateForm(TFormDetalhe,FormDetalhe);
        //Mensagem(DBGridDiario.Columns.Grid.SelectedField.Name,Informacao);
        FormDetalhe.Caption := DBGridDiario.Columns[DBGridDiario.Columns.Grid.SelectedIndex].Title.Caption+' - '+DM.FDQueryFluxoapr.AsString;
        FormDetalhe.StaticText1.Caption:=FloatToStr(DM.FDQueryFluxovl_01.AsFloat); //Columns.Grid.SelectedField.Name;
        FormDetalhe.StatusBar1.Panels[1].Text := DBGridDiario.Columns.Grid.SelectedField.Text; //;
        FormDetalhe.ShowModal;

     except


     end;

end;

procedure TFormPrincipal.DBGridDiarioDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var dif_bold : integer;
begin
     if ((Sender as TDBGrid).DataSource.Dataset.IsEmpty) then
        Exit;

     if (Column.FieldName = 'vl_01') and (DM.FDQueryFluxovl_01.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_02') and (DM.FDQueryFluxovl_02.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_03') and (DM.FDQueryFluxovl_03.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_04') and (DM.FDQueryFluxovl_04.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_05') and (DM.FDQueryFluxovl_05.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_06') and (DM.FDQueryFluxovl_06.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_07') and (DM.FDQueryFluxovl_07.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_08') and (DM.FDQueryFluxovl_08.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_09') and (DM.FDQueryFluxovl_09.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_10') and (DM.FDQueryFluxovl_10.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_11') and (DM.FDQueryFluxovl_11.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_12') and (DM.FDQueryFluxovl_12.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_13') and (DM.FDQueryFluxovl_13.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_14') and (DM.FDQueryFluxovl_14.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_15') and (DM.FDQueryFluxovl_15.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_16') and (DM.FDQueryFluxovl_16.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_17') and (DM.FDQueryFluxovl_17.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_18') and (DM.FDQueryFluxovl_18.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_19') and (DM.FDQueryFluxovl_19.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_20') and (DM.FDQueryFluxovl_20.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_21') and (DM.FDQueryFluxovl_21.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_22') and (DM.FDQueryFluxovl_22.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_23') and (DM.FDQueryFluxovl_23.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_24') and (DM.FDQueryFluxovl_24.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_25') and (DM.FDQueryFluxovl_25.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_26') and (DM.FDQueryFluxovl_26.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_27') and (DM.FDQueryFluxovl_27.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_28') and (DM.FDQueryFluxovl_28.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_29') and (DM.FDQueryFluxovl_29.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_30') and (DM.FDQueryFluxovl_30.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_31') and (DM.FDQueryFluxovl_31.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;
     if (Column.FieldName = 'vl_total') and (DM.FDQueryFluxovl_total.AsFloat < 0)
        then
            begin
                 TDBGrid(Sender).Canvas.Brush.Color := $999FFF;
                 TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
            end;


end;

procedure TFormPrincipal.DBGridDiarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     {case Key Of
          VK_LEFT: abort;
          VK_RIGHT:
                   begin
                        DBGrid1.SetFocus;
                        DBGrid1.SelectedIndex := DBGridDiario.SelectedIndex + 1;
                        abort;
                   end;
     end;}
end;

procedure TFormPrincipal.EditCxCodChange(Sender: TObject);
begin
     if EditCxCod.Text = ''
        then
            begin
                 EditCxDes.Text := '';
                 CodigoBusca    := 0;
                 DescricaoBusca := '';
            end;

end;

procedure TFormPrincipal.FormCreate(Sender: TObject);
begin

     //Par�metros do chamar rotina
     Servidor:=ParamStr(1);
     Porta:=ParamStr(2);
     Usuario:=ParamStr(3);
     Senha:=ParamStr(4);
     Banco:=ParamStr(5);
     CodigoEmpresaLogada:=StrToInt(ParamStr(6));
     CodigoFuncionarioLogado:=StrToInt(ParamStr(7));
     UsuarioLogado:=ParamStr(8);
     Outros:=ParamStr(9);

     //Conex�o
     DM.FDConnection1.Connected:=false;
     //DM.FDConnection1.DriverName:='PG';
     //showmessage(ExtractFilePath(Application.ExeName));
     //DM.FDPhysPgDriverLink1.Release;
     //DM.FDPhysPgDriverLink1.VendorHome:='';
     //DM.FDPhysPgDriverLink1.VendorHome:=ExtractFilePath(Application.ExeName)+'DLL';
     //DM.FDPhysPgDriverLink1.Release;
     DM.FDPhysPgDriverLink1.VendorHome:='';;
     DM.FDPhysPgDriverLink1.VendorLib:=ExtractFilePath(Application.ExeName)+'libpq.dll';
     DM.FDPhysPgDriverLink1.Release;


     DM.FDConnection1.Params.Values['server']      :=Servidor;
     DM.FDConnection1.Params.Values['port']        :=Porta;
     DM.FDConnection1.Params.Values['user_name']   :=Usuario;
     DM.FDConnection1.Params.Values['password']    :=Senha;
     DM.FDConnection1.Params.Values['database']    :=Banco;

     DM.FDConnection1.Connected := true;
end;

procedure TFormPrincipal.FormDestroy(Sender: TObject);
begin
     DM.FDConnection1.Close;
end;

procedure TFormPrincipal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key=27
        then
            Close;
end;

procedure TFormPrincipal.FormShow(Sender: TObject);
begin
     PageControl1.ActivePageIndex:=0;

     DTPDe.Date:=StartOfTheMonth(Date);
     DTPDeM.Date:=StartOfTheYear(Date);



end;

end.
