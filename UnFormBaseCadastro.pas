unit UnFormBaseCadastro;

interface

uses
  Forms,UnFormBase,Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls
  , Vcl.Controls, System.Classes;

type
  TFormBaseCadastro = class(TFormBase)
    procedure FormCreate(Sender: TObject);
  private
    fPermiteIncluir,fPermiteAlterar,fPermiteExcluir : Boolean;
  public
    property PermiteIncluir:Boolean read fPermiteIncluir;
    property PermiteAlterar:Boolean read fPermiteAlterar;
    property PermiteExcluir:Boolean read fPermiteExcluir;
  end;

var
  FormBaseCadastro: TFormBaseCadastro;

implementation

{$R *.dfm}

uses EventosForm;

procedure TFormBaseCadastro.FormCreate(Sender: TObject);
begin
  inherited;
  carregarPermissoesDeAcesso(self.fPermiteIncluir,self.fPermiteAlterar, self.fPermiteExcluir);
end;

end.
