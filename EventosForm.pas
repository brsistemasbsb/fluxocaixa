unit EventosForm;

interface

uses SysUtils,DateUtils,variants,Controls, forms,shellapi, windows
     ,classes, DB, Vcl.ComCtrls, StdCtrls, ExtCtrls, Messages,
     FireDAC.Comp.Client;

     procedure carregarPermissoesDeAcesso(Var PermiteIncluir,PermiteAlterar,PermiteExcluir:Boolean;Parametro:String = '');
     procedure tratarHabilitacaoToolBar(Sender:TObject;Form:TForm;PermiteIncluir,PermiteAlterar,PermiteExcluir : Boolean);
     procedure EscreveBarraTarefas(Ds:TDataSource;Form:TForm);
     function buscarRegistro(TextoSql:String; Ds:TDataSource; CampoFoco:TField = nil; limitar:boolean = True):Boolean;
     procedure carregaDescricoesLookUp(TextoSql:String;Parametros:Variant;EditDes:Array of TEdit);
     procedure OnKeyPressDoForm(Sender:TObject; Var Key:char);
     procedure OnKeyDownDoForm(Ds:TDataSource;Key:Word;ID:Integer;Sender:TObject);
     procedure ConfigurarPanelCodigo(Form: TForm;DataSet:TDataSet);
     procedure excluirRegistro(DS: TDataSource; Registro,Item:String);

implementation

uses StrUtils, Eventos, UnL, EventosUtil, EventosKey, UnFormLBusca;



procedure excluirRegistro(DS: TDataSource; Registro,Item:String);  //Confirma ou n�o a exclus�o de um registro
begin
     case Mensagem('tem certeza que deseja excluir '+Item+#13#10+'"'+Registro+'"?',[tmExclusao]) of
     mrYes : DS.DataSet.Delete;
     end;
end;


procedure ConfigurarPanelCodigo(Form: TForm;DataSet:TDataSet);
var l_painelCodigo:TPanel;
begin
     if Form = nil then exit;

     l_painelCodigo := ((Form.FindComponent('PanelCodigo')) as TPanel);
     if l_painelCodigo <> nil
        then
            l_painelCodigo.Caption := ' C�digo => ' + FormatFloat(TNumericField(DataSet.Fields[0]).DisplayFormat,DataSet.Fields[0].asInteger);
end;

procedure carregarPermissoesDeAcesso(Var PermiteIncluir,PermiteAlterar,PermiteExcluir:Boolean;Parametro:String = '');
var
   i:byte;
begin
//      if UsuarioLogado <> 'thunder'
//        then
//            begin
//                 PermiteAlterar:=(DMConf.QMenu.FieldByName('priva').AsString='S');
//                 PermiteIncluir:=(DMConf.QMenu.FieldByName('privi').AsString='S');
//                 PermiteExcluir:=(DMConf.QMenu.FieldByName('prive').AsString='S');
//            end
//        else
            begin
                 PermiteAlterar:=True;
                 PermiteIncluir:=True;
                 PermiteExcluir:=True;
            end;
     i := length(Parametro);
     while (Parametro <> '') do
     begin
          case Parametro[i] of
          'i': PermiteIncluir := false;
          'a': PermiteAlterar := false;
          'e': PermiteExcluir := false;
          end;
          Parametro:=copy(Parametro,1,length(Parametro)-1);
          i := i-1;
     end;
end;

procedure tratarHabilitacaoToolBar(Sender:TObject;Form:TForm;PermiteIncluir,PermiteAlterar,PermiteExcluir : Boolean);
begin
     if Form <> nil
        then
            with TDataSource(Sender) do
            begin
                 if TToolButton(Form.FindComponent('tbNovo'))<>nil
                    then
                        TToolButton(Form.FindComponent('tbNovo')).Enabled     := not (State in [dsInsert,dsEdit])
                                                                                  and PermiteIncluir;
                 if TToolButton(Form.FindComponent('tbAlterar'))<>nil
                    then
                        TToolButton(Form.FindComponent('tbAlterar')).Enabled  := not (State in [dsInsert,dsEdit])
                                                                                  and  (not DataSet.Fields[0].IsNull)
                                                                                  and PermiteAlterar;
                 if TToolButton(Form.FindComponent('tbGravar'))<>nil
                    then
                        TToolButton(Form.FindComponent('tbGravar')).Enabled   := (State in [dsInsert,dsEdit]);

                 if TToolButton(Form.FindComponent('tbExcluir'))<>nil
                    then
                        TToolButton(Form.FindComponent('tbExcluir')).Enabled  := not (State in [dsInsert,dsEdit])
                                                                                  and (not DataSet.Fields[0].IsNull)
                                                                                  and (PermiteExcluir);
                 if TToolButton(Form.FindComponent('tbBuscar')) <> nil
                    then
                        TToolButton(Form.FindComponent('tbBuscar')).Enabled   := not (State in [dsInsert,dsEdit]);

                 if TToolButton(Form.FindComponent('tbImprimir'))<>nil
                    then
                        TToolButton(Form.FindComponent('tbImprimir')).Enabled  := not (State in [dsInsert,dsEdit])
                                                                                   and (not DataSet.Fields[0].IsNull);
                 if TToolButton(Form.FindComponent('tbSair'))<>nil
                    then
                        TToolButton(Form.FindComponent('tbSair')).Enabled     := not (State in [dsInsert,dsEdit]);
                 EscreveBarraTarefas(TDataSource(Sender),Form);
            end;
end;

procedure EscreveBarraTarefas(Ds:TDataSource;Form:TForm);
begin
     //escreve na barra de tarefas
     case Ds.DataSet.State of
     dsBrowse : (Form.FindComponent('StatusBar1') as TStatusBar).Panels[1].Text := 'Consultando';
     dsEdit   : begin
                     (Form.FindComponent('StatusBar1') as TStatusBar).Panels[1].Text := 'Alterando';
                     (Form.FindComponent('StatusBar1') as TStatusBar).Panels[0].Text :=  EmptyStr;
                end;
     dsInsert : begin
                     (Form.FindComponent('StatusBar1') as TStatusBar).Panels[1].Text := 'Incluindo';
                     (Form.FindComponent('StatusBar1') as TStatusBar).Panels[0].Text :=  EmptyStr;
                end;
     end;
end;

function buscarRegistro(TextoSql:String; Ds:TDataSource; CampoFoco:TField = nil; limitar:boolean = True):Boolean;
var
   i: byte;
   w,larg: integer;
begin
     try
        Application.CreateForm(TFormLBusca,FormLBusca);
        DsL := Ds;  // Utilizada no evento FormL.DBGridPress
        if TextoSql[Length(TextoSql)]=';' then Delete(TextoSql,Length(TextoSql),1);
        with FormLBusca.QueryLoc do
        begin
             Close;
             SQL.Clear;
             if (Not(Limitar)) or (Pos(' limit ',TextoSql)>0)
                then
                    Sql.Text:=TextoSql
                else
                    Sql.Text:=TextoSql+' limit 200;';
             Params[0].Value := '%';
             EventosUtil.AbrirQuery(FormLBusca.QueryLoc);
             w := 0;
             For i := 0 to FieldCount - 1 do
             begin
                  FormLBusca.DBGridB.Columns.Add.FieldName := Fields[i].FieldName;
                  FormLBusca.DBGridB.Columns[i].Alignment := taLeftJustify;
                  FormLBusca.DBGridB.Columns[i].Title.Caption := BuscaDisplayLabel(Ds,Fields[i].FieldName);
                  larg:=BuscaDisplayWidth(Ds,Fields[i].FieldName);
                  if larg >0
                     then
                         FormLBusca.DBGridB.Columns[i].Width:=larg * 7  //7 p/ transformar em pixels a medida, ou seja, n�o � a mesma do displaywidth do campo
                     else
                         FormLBusca.DBGridB.Columns[i].Width:=RetornaWidthDefault(Fields[i].DataType)*7;
                  if (i = (FieldCount - 1)) and ((FormLBusca.DBGridB.Width - 22) > (w+FormLBusca.DBGridB.Columns[i].Width)) //22 � o tamanho da barra de rolagem.
                     then
                         FormLBusca.DBGridB.Columns[i].Width := (FormLBusca.DBGridB.Width - 22)- w;

                  w:=w+FormLBusca.DBGridB.Columns[i].Width;
             end;
        end;
        FormLBusca.ShowModal;
        if FormLBusca.ModalResult = mrOk
           then
               Result:=true
           else
               Result:=false;
        if CampoFoco <> nil then CampoFoco.FocusControl;
     finally
       FormLBusca.Release;
       FormLBusca := nil;
     end;
end;

procedure carregaDescricoesLookUp(TextoSql:String;Parametros:Variant;EditDes:Array of TEdit);
          //Atualiza os LookUps quando se rola a tabela
var
   i,QtdeCampos:byte;
   pesquisar:Boolean;
   Dados:Variant;

   procedure LimparEdits;
   var e,QtdeEdits:byte;
   begin
        QtdeEdits :=High(EditDes);
        for e:=0 to QtdeEdits do
        begin
            EditDes[e].Clear;
        end;
   end;

begin
     pesquisar:=true;
     //Verifica se � necess�rio pesquisar.
     if VarIsArray(Parametros)
        then
            begin
                 QtdeCampos :=VarArrayHighBound(Parametros,1);
                 for i:=0 to QtdeCampos do
                 begin
                      if ValorNull(Parametros[i])
                         then
                             begin
                                  LimparEdits;
                                  pesquisar:=false;
                                  Break;
                             end;
                 end;
            end
        else
            if ValorNull(Parametros)
               then
                   begin
                        LimparEdits;
                        pesquisar:=false;
                   end;

     if pesquisar
        then
            begin
                 Dados:=EncontraValor(TextoSql,Parametros);
                 if VarIsArray(Dados)
                    then
                        begin
                             QtdeCampos := VarArrayHighBound(Dados,1);
                             for i:=0 to QtdeCampos do
                             begin
                                  if ValorNull(Dados[i])
                                     then
                                         EditDes[i].Clear
                                     else
                                         EditDes[i].Text:=FormataValorEmString(Dados[i]);
                             end;
                        end
                    else
                        if ValorNull(Dados)
                           then
                               EditDes[0].Clear
                           else
                               EditDes[0].Text:=FormataValorEmString(Dados);
            end;
end;

procedure OnKeyPressDoForm(Sender:TObject; Var Key:char);
begin
     //Aceita somente caracteres v�lidos (existentes no teclado) tirando acentos
     OnKeyPressDosTextos(Key);

     if Key = #13
        then
            if (Sender as TForm).ActiveControl <> nil
               then
                   if ((((Sender as TForm).ActiveControl).ClassName) <> 'TMemo') and
                      ((((Sender as TForm).ActiveControl).ClassName) <> 'TDBMemo') and
                      ((((Sender as TForm).ActiveControl).ClassName) <> 'TDBGrid')
                      then
                          begin
                               (Sender as TForm).PERFORM(WM_NEXTDLGCTL,0,0);

                               Key:=#0;
                          end;
end;

procedure OnKeyDownDoForm(Ds:TDataSource;Key:Word;ID:Integer;Sender:TObject);
var
   StateTabela : TDataSetState;
   abaAtual:integer;
begin
  case key of
  VK_F1:Application.HelpContext(0);  //Application.HelpContext(ID);
  VK_ESCAPE:
    begin
      if Ds <> nil
        then
          begin
            StateTabela:=TFDQuery(Ds.DataSet).State;
            if (StateTabela in [dsInsert,dsEdit])
              then
                begin
                  if Mensagem('Deseja realmente desistir da opera��o?',Confirmacao) = mrYES
                     then
                         begin
                              TFDQuery(Ds.DataSet).Cancel;
                              if (StateTabela = dsEdit) and (Assigned(Ds.DataSet.AfterScroll))
                                 then
                                     TFDQuery(Ds.DataSet).AfterScroll(TFDQuery(Ds.DataSet));
                         end;
                end
              else
                (Sender as TForm).Close;
          end
        else
          (Sender as TForm).Close;
    end;
  end;
end;



end.
