unit EventosKey;

interface

uses FireDAC.Comp.Client,FireDAC.Comp.BatchMove.Text,SysUtils,DateUtils,variants,Controls, forms,shellapi, windows
     ,classes, DB;

     procedure OnKeyPressDoEditMoeda(Var Key:Char;Texto:String;PoseNeg:boolean = false;QdoDBGrid:boolean = false);
     procedure OnKeyPressDosTextos(Var Key:Char);
     procedure OnKeyPressDasLetras(Var Key:Char);  //Aceita somente Letras
     function Localizavirg(num:String):boolean;

implementation

uses StrUtils;


function Localizavirg(num:String):boolean;
var
   i:byte;
begin
     Result:=false;
     i:=1;
     while i<=Length(num) do
     begin
     if num[i] = ','
        then
            begin
                 Result:=true;
                 i := Length(num);
            end;
            inc(i);
     end;
end;

procedure OnKeyPressDosTextos(Var Key:Char); //Aceita somente os caracteres do teclado e tira os acentos dos textos
begin
     Case Key of
     '�','�','�','�','�','�','�','�': Key :='A';
     '�','�','�','�','�','�'        : Key :='E';
     '�','�','�','�','�','�'        : Key :='I';
     '�','�','�','�','�','�','�','�': Key :='O';
     '�','�','�','�','�','�','�','�': Key :='U';
     '�','�'                        : Key :='C';
     end;

     if Not (Key in [#3,#22,#26,#13,#10,#9,#8,'A'..'Z','a'..'z','0'..'9','"','!','@','#',
                     '�','�','�','�','�','�','�','�','�','�','�','�','�','�',
                     '�','�','�','�','�','�','�','�','�','�','�','�','�','�',
                     '�','�','�','�','�','�','�','�','�','�','�','�','$','%',
                     '&','*','(',')','-','_','=','+','�','<','>','[',']','{','}',
                     '?','/','\',',','|','.',';',' ',':'])
           then
               Key:=#0
           else
               if Key in ['a'..'z']
                  then
                      Key:=UpCase(Key);
end;

procedure OnKeyPressDoEditMoeda(Var Key:Char;Texto:String;PoseNeg:boolean = false;QdoDBGrid:boolean = false);
var
   Teclas:Set of Char;
begin
     if PoseNeg
        then
            Teclas:=['0'..'9',#8,'-',',','.',#13]
        else
            Teclas:=['0'..'9',#8,',','.',#13];
     if not(Key in Teclas)
        then
            Key :=#0
        else
            begin
                 if Key in [',','.']
                    then
                        Key := FormatSettings.DecimalSeparator;
                 if not QdoDBGrid
                    then
                        if Key=','
                           then
                               if Localizavirg(Texto)
                                  then
                                      Key:=#0;
            end;
end;

procedure OnKeyPressDasLetras(Var Key:Char);  //Aceita somente Letras
begin
     if not (Key in ['A'..'Z','a'..'z',#8,#13])
        then
            Key := #0;
end;

end.
