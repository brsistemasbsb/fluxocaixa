unit UnFormBase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, StdCtrls, Vcl.ExtCtrls;

type
  TFormBase = class(TForm)
    StatusBar1: TStatusBar;
    PanelTitulo: TPanel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public

  protected
           procedure MostraErro(Sender: TObject; E: Exception);
  end;

var
   FormBase: TFormBase;

implementation

uses Eventos;

{$R *.dfm}

procedure TFormBase.MostraErro(Sender: TObject; E: Exception);
begiN
     Mensagem(E.Message+#13#10+'Contate Suporte t�cnico.',Advertencia);
end;

procedure TFormBase.FormKeyPress(Sender: TObject; var Key: Char);
begin
     OnKeyPressDoForm(Sender,Key)
end;

procedure TFormBase.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     case Key Of
          VK_F1     : Application.HelpContext(54);
          VK_ESCAPE : Close
     end;
end;

end.

