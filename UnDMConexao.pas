// 1.14.06.24 - 24/06/2014 - Raul salva a p�tria com o backup do
unit UnDMConexao;

interface

uses
  Windows, Messages, SysUtils, comctrls, dbgrids, Classes, Graphics, Controls,
  Forms, Dialogs, Db, Menus, stdctrls, extctrls, buttons, dbctrls,
  ImgList,Math, Variants, ShellApi,
  InvokeRegistry, AppEvnts,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Comp.Client,
  FireDAC.Phys.PG, FireDAC.Phys.PGDef,Eventos, FireDAC.VCLUI.Wait;

type
  TDMConexao = class(TDataModule)
    dbzae: TFDConnection;
    FDPhysPgDriverLink1: TFDPhysPgDriverLink;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  end;

var
   DMConexao: TDMConexao;

implementation

uses UnLeitorParametroSubPrograma;
  
{$R *.DFM}
procedure TDMConexao.DataModuleCreate(Sender: TObject);
var lLeitorParametroSubPrograma:TLeitorParametroSubPrograma;
begin
     if dbZAE.Connected then dbZAE.Connected:=False; //N�o pode deixar aberto, por via das d�vidas fecho na marra
     //dbZAE.Connected:=true;
     {
     ShortDateFormat:='dd/MM/yyyy';
     DateSeparator:='/';
     ShortTimeFormat:='hh:nn:ss';
     TimeSeparator:=':';
     DecimalSeparator:=',';
     ThousandSeparator:='.';
     }
     try
        lLeitorParametroSubPrograma := TLeitorParametroSubPrograma.create;

        Eventos.EmpresaLogada := lLeitorParametroSubPrograma.EmpresaLogada;
        FuncionarioLogado := lLeitorParametroSubPrograma.FuncionarioLogado;
        UsuarioLogado := lLeitorParametroSubPrograma.UsuarioLogado;

        FDPhysPgDriverLink1.VendorLib := lLeitorParametroSubPrograma.VendorLib;

        with dbzae do
        begin

             Params.Clear;
             Params.Values['DriverID']  := FDPhysPgDriverLink1.DriverID;
             Params.Values['Database']  := lLeitorParametroSubPrograma.BaseDeDados;
             Params.Values['User_name'] := lLeitorParametroSubPrograma.Usuario;
             Params.Values['Password']  := lLeitorParametroSubPrograma.Senha;
             Params.Values['Server']    := lLeitorParametroSubPrograma.IPServidor;
             Params.Values['Port']      := IntToStr(lLeitorParametroSubPrograma.Porta);
             Connected := True;
        end;

     except
           on e:Exception do Mensagem(e.Message,Advertencia);
     end;
end;

procedure TDMConexao.DataModuleDestroy(Sender: TObject);
begin
     dbZAE.Connected := False;
end;

end.
