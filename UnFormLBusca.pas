unit UnFormLBusca;

interface

uses
  Windows, Messages, SysUtils, ExtCtrls, DB, ComCtrls, Grids, DBGrids,
  Classes, Controls, Graphics, Forms, Dialogs,
  dbctrls,
  Provider, DBClient, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFormLBusca = class(TForm)
    Panel1: TPanel;
    DBGridB: TDBGrid;
    DsLoc: TDataSource;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    QueryLoc: TFDQuery;
    procedure DBGridBDblClick(Sender: TObject);
    procedure DBGridBTitleClick(Column: TColumn);
    procedure DBGridBKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGridBColEnter(Sender: TObject);
    procedure DBGridBDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Timer1Timer(Sender: TObject);
  private
         Decrescente:boolean;
         ColAntDBGridBL :integer;
    procedure AlteraSQL(Column:TColumn);
  public
        campo,condicaoCampo:String;
        corDeDestaque:TColor;
        usaDestaque:boolean;
        buscaNovamenteOnShow:boolean;
        procedure TrocaTextoDaSQL(p_campo:WideString);
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  end;

var
  FormLBusca: TFormLBusca;
  Escolheu:Boolean;
  TipoCampo:Integer;
  DsL: TDataSource;

implementation

Uses UnDMConexao, EventosString, Eventos;

{$R *.DFM}

procedure TFormLBusca.CreateParams(var Params: TCreateParams);
begin
     inherited CreateParams(Params);
     Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
end;

procedure TFormLBusca.AlteraSQL(Column:TColumn);
var Ordem:String;
    tipo_do_campo:TFieldType;
    camponil:boolean;

   procedure TrocaTextoSQL(Texto: String);
      var
         TextoAux,S1,S2,S3,S4: String;

      procedure ApagaUpper;
      begin
           Delete(S1,System.Pos('UPPER(',S1),6);
      end;

      Procedure RetornaCampo;  //Se o campo for nomeado (ex.: pardes as Descri��o)
      var aux:String;     // esta fun��o retorna o nome do campo que se encontra antes do comando as)
          p,z,ParentesesA,ParentesesF:Integer;
          campo:String;
      begin
          p:=Pos(' AS '+Texto,Textoaux);
          if p>0
             then
                 begin
                      aux:=Copy(Textoaux,1,p-1);
                      z:=Length(aux);
                      campo:=EmptyStr;
                      While aux[z]=' ' do  //para o caso de mais de um espaco.
                      begin
                         dec(z);
                      end;

                      ParentesesA:=0;ParentesesF:=0;
                      While ((aux[z]<>' ') and (aux[z]<>',')) or (ParentesesA<>ParentesesF) do
                      begin
                         campo:=aux[z]+campo;
                         if aux[z] = '('
                            then ParentesesA:=ParentesesA+1
                            else if aux[z] = ')' then ParentesesF:=ParentesesF+1;
                         dec(z);
                      end;
                      Texto:=campo;
                 end;
      end;

      procedure TrataOrdem;
      var ob,des:Integer;
      begin
           ob:=retornaPosUltimo('ORDER BY',TextoAux);
           des:=retornaPosUltimo(' DESC ',copy(TextoAux,ob,Length(TextoAux)-ob));
           if des>0
              then
                  Ordem:=EmptyStr
              else
                  if self.Decrescente
                     then
                         Ordem:=' DESC '
                     else
                         Ordem:=EmptyStr;
      end;

   begin
        with QueryLoc do
        begin
             Close;
             TextoAux := AnsiUpperCase(SQL.Text);
             TrataOrdem;
             RetornaCampo;
             S1 := ApagaPalavra(copy(TextoAux,1,pos('LIKE',TextoAux)-2),'u');
             if pos('UPPER(',S1) > 0
                then
                    ApagaUpper;
             S2 := copy(TextoAux,retornaPosUltimo('LIKE',TextoAux),retornaPosUltimo('ORDER BY',TextoAux)+8 - retornaPosUltimo('LIKE',TextoAux));
             S3 := ApagaPalavra(copy(TextoAux,retornaPosUltimo('ORDER BY',TextoAux)+9,length(TextoAux)),'p');
             S4 := S1 +'UPPER(cast('+ Texto +' as varchar(254))) ' + S2 + ' ' + Texto + Ordem +S3;
             SQL.Clear;
             SQL.Text := S4;
        end;
   end;

begin
     if Column.FieldName<>''
        then
            begin
                 tipo_do_campo:=Column.Field.DataType;
                 camponil:=Column.Field=Nil;
                 TrocaTextoSQL(AnsiUpperCase(Column.FieldName));
                 if not camponil
                    then
                        begin
                             case tipo_do_campo of
                             ftSmallint,ftInteger,ftWord,ftFloat,ftCurrency,ftLargeInt,
                             ftBytes,ftAutoInc                                         : TipoCampo := 0;
                             ftString,ftBlob,ftMemo,ftFmtMemo,ftFixedChar,ftWideString : TipoCampo := 1;
                             end;
                        end;
                 ColAntDBGridBL := Column.Index;
            end;
end;

procedure TFormLBusca.DBGridBDblClick(Sender: TObject);
var lCont:integer;
begin
  if not DsLoc.DataSet.IsEmpty
    then
        begin
          TFDQuery(DsL.DataSet).Close;
          for lCont := 0 to TFDQuery(DsL.DataSet).ParamCount - 1 do
          begin
              TFDQuery(DsL.DataSet).Params[lCont].Value := DsLoc.DataSet.Fields[lCont].Value;
          end;
          TFDQuery(DsL.DataSet).Open;
          FormLBusca.ModalResult:=mrOk;
        end
end;

procedure TFormLBusca.DBGridBTitleClick(Column: TColumn);

procedure OnTitleClickDoDBGridB(Column:TColumn;FormLoc:TForm);
var Ordem:String;

   procedure TrocaTextoSQL(Texto: String);
      var
         TextoAux,S1,S2,S3,S4: String;

      procedure ApagaUpper;
      begin
           Delete(S1,pos('UPPER(',S1),6);
      end;

      Procedure RetornaCampo;  //Se o campo for nomeado (ex.: pardes as Descri��o)
      var aux:WideString;     // esta fun��o retorna o nome do campo que se encontra antes do comando as)
          p,z,ParentesesA,ParentesesF:Integer;
          campo:String;
      begin
          p:=Pos(' AS '+Texto,Textoaux);
          if p>0
             then
                 begin
                      aux:=Copy(Textoaux,1,p-1);
                      z:=Length(aux);
                      campo:=EmptyStr;
                      While aux[z]=' ' do  //para o caso de mais de um espaco.
                      begin
                         dec(z);
                      end;

                      ParentesesA:=0;ParentesesF:=0;
                      While ((aux[z]<>' ') and (aux[z]<>',')) or (ParentesesA<>ParentesesF) do
                      begin
                         campo:=aux[z]+campo;
                         if aux[z] = '('
                            then ParentesesA:=ParentesesA+1
                            else if aux[z] = ')' then ParentesesF:=ParentesesF+1;
                         dec(z);
                      end;
                      Texto:=campo;
                 end;
      end;

      procedure TrataOrdem;
      var ob,des:Integer;
          posicaoVirgula:integer;
          str_order_by:string;
      begin
           {ob:=Pos('ORDER BY',TextoAux);
           des:=pos(' DESC ',copy(TextoAux,ob,Length(TextoAux)-ob));}
           ob  := retornaPosUltimo('ORDER BY',TextoAux);
           str_order_by := copy(TextoAux,ob,Length(TextoAux)-ob);
           posicaoVirgula := retornaPosUltimo(' DESC ',str_order_by);
           if posicaoVirgula > 0
              then
                  des := retornaPosUltimo(' DESC ',copy(str_order_by,1,posicaoVirgula))
              else
                  des := retornaPosUltimo(' DESC ',str_order_by);
           if des>0
              then
                  Ordem:=EmptyStr
              else
                  if ColAntDBGridBL = Column.index
                     then
                         Ordem:=' DESC '
                     else
                         Ordem:=EmptyStr;
      end;

   begin
        with QueryLoc do
        begin
             Close;
             TextoAux := AnsiUpperCase(SQL.Text);
             TrataOrdem;
             RetornaCampo;
             //S1 := ApagaPalavra(copy(TextoAux,1,pos('LIKE',TextoAux)-2),'u');
             S1 := ApagaPalavra(copy(TextoAux,1,retornaPosUltimo('LIKE',TextoAux)-2),'u');
             if pos('UPPER(',S1) > 0
                then
                    ApagaUpper;
             //S2 := copy(TextoAux,pos('LIKE',TextoAux),pos('ORDER BY',TextoAux)+8 - pos('LIKE',TextoAux));
             S2 := copy(TextoAux,retornaPosUltimo('LIKE',TextoAux),retornaPosUltimo('ORDER BY',TextoAux)+8 - retornaPosUltimo('LIKE',TextoAux));
             //S3 := ApagaPalavra(copy(TextoAux,pos('ORDER BY',TextoAux)+9,length(TextoAux)),'p');

             S3 := ApagaPalavra(copy(TextoAux,retornaPosUltimo('ORDER BY',TextoAux)+9,length(TextoAux)),'p');

             // retira o DESC
             if retornaPosUltimo('DESC',S3) > 0
                then
                    S3 := ApagaPalavra(S3,'p');                    
             S4 := S1 +'UPPER(cast('+ Texto +' as varchar(254))) ' + S2 + ' ' + Texto + Ordem +S3;
             SQL.Clear;
             SQL.Text := S4;
             //mensagem(S4,Informacao);

             Params[0].Value := '%';
             Open;
        end;
   end;
begin
     if Column.FieldName<>''
        then
            begin
                 TrocaTextoSQL(AnsiUpperCase(Column.FieldName));
                 (FormLoc.FindComponent('StatusBar1') as TStatusBar).Panels[0].Text := 'Busca por '+AnsiUpperCase(Column.Title.Caption) + ':';
                  (FormLoc.FindComponent('StatusBar1') as TStatusBar).Panels[1].Text := '';
                 if Column.Field<>Nil
                    then
                        begin
                             (FormLoc.FindComponent('StatusBar1') as TStatusBar).Panels[1].Width := Column.Field.DisplayWidth * 14;
                             ColAntDBGridBL := Column.Index;
                             case Column.Field.DataType of
                             ftSmallint,ftInteger,ftWord,ftFloat,ftCurrency,ftLargeInt,
                             ftBytes,ftAutoInc                                         : TipoCampo := 0;
                             ftString,ftBlob,ftMemo,ftFmtMemo,ftFixedChar,ftWideString : TipoCampo := 1;
                             end;
                        end;
            end;
end;


begin
     OnTitleClickDoDBGridB(Column,FormLBusca);
     Column.Field.FocusControl;
end;

procedure TFormLBusca.DBGridBKeyPress(Sender: TObject; var Key: Char);
Var aux:String;
begin
     case TipoCampo of
     0: if Key in ['0'..'9','A'..'Z','a'..'z',',','.',#8,#13,#27]    //Campo num�rico
           then
               begin
                    if Key = ','
                       then
                           Key:='.';
               end
           else
               begin
                    Key:=#0;
                    Exit;
               end;
     1: if not (Key in ['A'..'Z','a'..'z','0'..'9','.',',','/','\','_','-','&','"',#8,#13,#27,#32]) //String
           then
               begin
                    Key:=#0;
                    Exit;
               end;
     end;

     if not (Key in [#8,#13])
        then
            begin
                 if (Key = #32) and (StatusBar1.Panels[1].Text=EmptyStr)
                    then Key:=#0  //Para n�o incluir espa�o no in�cio do texto a ser procurado.
                    else StatusBar1.Panels[1].Text:=concat(StatusBar1.Panels[1].Text,uppercase(Key));
            end
        else
            if key=#8
               then
                   StatusBar1.Panels[1].Text:=copy( StatusBar1.Panels[1].Text,1,(length(StatusBar1.Panels[1].Text)-1));

     if not (key in [#13,#27])
        then
            begin
                 with QueryLoc do
                 begin
                      DisableControls;
                      aux:=StatusBar1.Panels[1].Text+'%';
                      aux:=StringReplace(aux,' ','%',[rfReplaceAll]);
                      if (DBGridB.Columns[DBGridB.SelectedIndex].Field.DataType in [ftString,ftBlob,ftMemo,ftFmtMemo,ftFixedChar,ftWideString])
                          and
                          (DBGridB.Columns[DBGridB.SelectedIndex].FieldName<>'mundes')
                         then
                             aux:='%'+aux;
                      if DBGridB.SelectedIndex <> ColAntDBGridBL
                         then
                             AlteraSQL(DBGridB.Columns[DBGridB.SelectedIndex]);
                      if Active then Close;
                      Params[0].Value:=aux;
                      Open;
                      EnableControls;
                 end;
            end;
     case key of
     #13: begin
               Key:=#0;
               DBGridB.OnDblClick(nil);
          end;
     #27: begin
               Key:=#0;
               DsLoc.DataSet.Close;
               Close;
          end;
     end;
end;

procedure TFormLBusca.FormShow(Sender: TObject);
begin
     if buscaNovamenteOnShow then OnShowDoFormL(DBGridB);
end;

procedure TFormLBusca.FormCreate(Sender: TObject);
begin
     Escolheu:=False;
     usaDestaque:=false;
     buscaNovamenteOnShow:=true;
     SetWindowLong(
                Application.Handle,
                GWL_EXSTYLE,
                GetWindowLong(Application.Handle,GWL_EXSTYLE)
                and
                WS_EX_APPWINDOW
                and
                WS_EX_TOOLWINDOW);
end;

procedure TFormLBusca.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if Not Escolheu
        then
            QueryLoc.Close;
     self.Decrescente := False;
end;

procedure TFormLBusca.DBGridBColEnter(Sender: TObject);
var Coluna:TColumn;
begin
     Coluna:=DBGridB.Columns[DBGridB.SelectedIndex];
     StatusBar1.Panels[0].Text := 'Busca por '+AnsiUpperCase(Coluna.Title.Caption) + ':';
     StatusBar1.Panels[1].Text := '';
     StatusBar1.Panels[1].Width := Coluna.Field.DisplayWidth * 14;
     case Coluna.Field.DataType of
     ftSmallint,ftInteger,ftWord,ftFloat,ftCurrency,ftLargeInt,
     ftBytes,ftAutoInc                                         : TipoCampo := 0;
     ftString,ftBlob,ftMemo,ftFmtMemo,ftFixedChar,ftWideString : TipoCampo := 1;
     end;
end;

procedure TFormLBusca.DBGridBDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     if (usaDestaque) and (DsLoc.DataSet.FieldByName(campo).AsString = condicaoCampo)
        then
            begin
                 DBGridB.Canvas.Font.Color:=corDeDestaque;
                 DBGridB.Canvas.FillRect(Rect);
                 if Column.Field.DataType in [ftFloat,ftInteger,ftWord,ftSmallint,ftCurrency,ftLargeInt]
                    then
                        DBGridB.Canvas.TextOut(Rect.Right-Canvas.TextWidth(FormatFloat(TNumericField(Column.Field).EditFormat,Column.Field.AsFloat))-2,Rect.Top,FormatFloat(TNumericField(Column.Field).EditFormat,Column.Field.AsFloat))
                    else
                        DBGridB.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);
            end;
end;

procedure TFormLBusca.Timer1Timer(Sender: TObject);
begin
     FlashWindow(self.Handle,true);
end;

procedure TFormLBusca.TrocaTextoDaSQL(p_campo:WideString);
var
   TextoAux,S1,S2,S3,S4: String;
   Ordem:String;

      procedure ApagaUpper;
      begin
           Delete(S1,pos('UPPER(',S1),6);
      end;

      Procedure RetornaCampo;  //Se o campo for nomeado (ex.: pardes as Descri��o)
      var aux:WideString;     // esta fun��o retorna o nome do campo que se encontra antes do comando as)
          p,z,ParentesesA,ParentesesF:Integer;
          campo:String;
      begin
          p:=Pos(' AS '+UpperCase(p_campo),Textoaux);
          if p>0
             then
                 begin
                      aux:=Copy(Textoaux,1,p-1);
                      z:=Length(aux);
                      campo:=EmptyStr;
                      While aux[z]=' ' do  //para o caso de mais de um espaco.
                      begin
                         dec(z);
                      end;

                      ParentesesA:=0;ParentesesF:=0;
                      While ((aux[z]<>' ') and (aux[z]<>',')) or (ParentesesA<>ParentesesF) do
                      begin
                         campo:=aux[z]+campo;
                         if aux[z] = '('
                            then ParentesesA:=ParentesesA+1
                            else if aux[z] = ')' then ParentesesF:=ParentesesF+1;
                         dec(z);
                      end;
                      p_campo := campo;
                 end;
      end;

      procedure TrataOrdem;
      var ob,des:Integer;
      begin
           {ob:=Pos('ORDER BY',TextoAux);
           des:=pos(' DESC ',copy(TextoAux,ob,Length(TextoAux)-ob));}
           ob:=retornaPosUltimo('ORDER BY',TextoAux);
           des:=retornaPosUltimo(' DESC ',copy(TextoAux,ob,Length(TextoAux)-ob));
           Ordem:=EmptyStr
      end;

begin
     with QueryLoc do
     begin
          Close;
          TextoAux := AnsiUpperCase(SQL.Text);
          TrataOrdem;
          RetornaCampo;
          //S1 := ApagaPalavra(copy(TextoAux,1,pos('LIKE',TextoAux)-2),'u');
          S1 := ApagaPalavra(copy(TextoAux,1,retornaPosUltimo('LIKE',TextoAux)-2),'u');
          if pos('UPPER(',S1) > 0
             then
                 ApagaUpper;
          //S2 := copy(TextoAux,pos('LIKE',TextoAux),pos('ORDER BY',TextoAux)+8 - pos('LIKE',TextoAux));
          S2 := copy(TextoAux,retornaPosUltimo('LIKE',TextoAux),retornaPosUltimo('ORDER BY',TextoAux)+8 - retornaPosUltimo('LIKE',TextoAux));
          //S3 := ApagaPalavra(copy(TextoAux,pos('ORDER BY',TextoAux)+9,length(TextoAux)),'p');

          S3 := ApagaPalavra(copy(TextoAux,retornaPosUltimo('ORDER BY',TextoAux)+9,length(TextoAux)),'p');

          // retira o DESC
          if retornaPosUltimo('DESC',S3) > 0
             then
                 S3 := ApagaPalavra(S3,'p');
          S4 := S1 +'UPPER(cast('+ p_campo +' as varchar(254))) ' + S2 + ' ' + p_campo + Ordem +S3;
          SQL.Clear;
          SQL.Text := S4;
          //mensagem(S4,Informacao);

          //Params[0].Value := '%';
          //Open;
     end;
end;

end.
