unit EventosJasper;

interface

uses SysUtils,DateUtils,variants,Eventos,Controls, forms,shellapi, windows;

procedure abrirJasper(rel,parametros:string;formato_saida:string = 'VIS';valida_parametros:string = 'N');

implementation

uses StrUtils, UnDMConexao;

procedure abrirJasper(rel,parametros:string;formato_saida:string = 'VIS';valida_parametros:string = 'N');
var comando_jasper:string;
begin

     try
        comando_jasper := ' /k java -jar '+ExtractFileDir(Application.ExeName)+'\jasperviewer.jar '+
                          ' '+DMConexao.dbzae.ConnectionName+
                          ' '+ExtractFileDir(Application.ExeName)+'\relatorios\'+rel+
                          ' '+formato_saida+
                          ' '+valida_parametros;

        if parametros <> EmptyStr
           then
               comando_jasper := comando_jasper + ' '+parametros;
        if (UsuarioLogado = 'thunder')
           then
               ShellExecute(0, nil, 'cmd.exe', Pchar(comando_jasper), nil, SW_show)
           else
               ShellExecute(0, nil, 'cmd.exe', Pchar(comando_jasper), nil, SW_hide);
               
     except
           on e : Exception do
           begin
                Mensagem(e.Message,Advertencia);
           end;
     end
end;


end.
