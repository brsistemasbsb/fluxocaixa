unit UnControleTransacao;


{
  essa classe permite q seja criada um aninhado de transa��es no meio do codigo,
  mas na pratica garante que apenas o objeto q iniciou a transa��o seja
  capaz de comitar ou voltar a transa��o

   isso � feito atrav�s da variavel objControleTransacao
}
interface

uses FireDAC.Comp.Client;

type
   TControleTransacao = class

   public
         class procedure starTransaction(p_object:TObject);
         class procedure commit(p_object:TObject);
         class procedure rollback(p_object:TObject);
         class procedure setBaseDados(p_baseDados:TFDConnection);
         class function getBaseDados:TFDConnection;
   end;

implementation

var
   objControleTransacao:TObject;
   baseDados:TFDConnection;

class procedure TControleTransacao.setBaseDados(p_baseDados:TFDConnection);
begin
     baseDados := p_baseDados;
end;

class function TControleTransacao.getBaseDados:TFDConnection;
begin
     result := baseDados;
end;

class procedure TControleTransacao.starTransaction(p_object:TObject);
begin
     if objControleTransacao = nil
        then
            if not baseDados.InTransaction
               then
                   begin
                        objControleTransacao := p_object;
                        baseDados.StartTransaction;
                   end;
end;

class procedure TControleTransacao.commit(p_object:TObject);
begin
     // so permite commitar a transa��o se o objeto q chamou essa
     // fun��o for o mesmo que iniciou a transa��o

     if objControleTransacao = p_object
        then
            begin
                 objControleTransacao := nil;
                 baseDados.Commit;
            end;
end;

class procedure TControleTransacao.rollback(p_object:TObject);
begin
     // so permite dar rollback na transa��o se o objeto q chamou essa
     // fun��o for o mesmo que iniciou a transa��o

     if objControleTransacao = p_object
        then
            begin
                 objControleTransacao := nil;
                 baseDados.Rollback;
            end;
end;

end.
