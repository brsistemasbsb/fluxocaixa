unit UnMensSenha;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ImgList, Variants;

type
  TFormMensSenha = class(TForm)
    bbOk: TBitBtn;
    ImgAdv: TImage;
    bbCancel: TBitBtn;
    Memo1: TMemo;
    EditSenha: TEdit;
    labelSenha: TLabel;
    Timer1: TTimer;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EditSenhaEnter(Sender: TObject);
    procedure EditSenhaExit(Sender: TObject);
    procedure EditSenhaKeyPress(Sender: TObject; var Key: Char);
    procedure Timer1Timer(Sender: TObject);
    procedure bbCancelClick(Sender: TObject);
    procedure bbOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    botaoFoco:TBitBtn;
    SQl:String;
  protected
    procedure CreateParams(var Params: TCreateParams); override;

  end;

var
  FormMensSenha: TFormMensSenha;


implementation

uses Eventos;

{$R *.DFM}

procedure TFormMensSenha.CreateParams(var Params: TCreateParams);
begin
     inherited CreateParams(Params);
     Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
end;

procedure TFormMensSenha.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if Key = #27
        then
            Close;
     if (Key = #13)
        then
            begin
                 if bbOk.Visible
                    then bbOk.Click;
            end;
end;

procedure TFormMensSenha.FormShow(Sender: TObject);
begin
     if Memo1.Lines.Count>7
        then
            Memo1.ScrollBars:=ssVertical
        else
            Memo1.ScrollBars:=ssNone;
     EditSenha.SetFocus;
end;

procedure TFormMensSenha.FormCreate(Sender: TObject);
begin
     SetWindowLong(Application.Handle, GWL_EXSTYLE,GetWindowLong(Application.Handle,GWL_EXSTYLE) and WS_EX_APPWINDOW and WS_EX_TOOLWINDOW);
end;

procedure TFormMensSenha.EditSenhaEnter(Sender: TObject);
begin
     OnEnterDoDBEdit(sender);
end;

procedure TFormMensSenha.EditSenhaExit(Sender: TObject);
begin
     OnExitDoDBEdit(sender);
end;

procedure TFormMensSenha.EditSenhaKeyPress(Sender: TObject; var Key: Char);
begin
     if key = #13
        then
            bbOk.OnClick(bbOk);
            //ModalResult:=mryes;
end;

procedure TFormMensSenha.Timer1Timer(Sender: TObject);
begin
     FlashWindow(self.Handle,true);
end;

procedure TFormMensSenha.bbCancelClick(Sender: TObject);
begin
     ModalResult:=mrNo;
end;

procedure TFormMensSenha.bbOkClick(Sender: TObject);
begin
     if sql <> emptystr
        then
            begin
                if Encontrou(sql,EditSenha.Text)
                   then
                       ModalResult:=mrYes
                   else
                       ModalResult:=mrNo;
            end
        else
        //ModalResult:=mrYes;
end;

end.
