unit UnLeitorParametroSubPrograma;

interface

uses System.sysUtils,Forms;

type
    TLeitorParametroSubPrograma = class
    private
           fIPServidor:string;
           fPorta:integer;
           fUsuario:string;
           fSenha:string;
           fBaseDeDados:string;
           fEmpresaLogada:integer;
           fFuncionarioLogado:integer;
           fUsuarioLogado:string;
           function getVendorLib:string;
           function getDiretorioLib:string;
    public
          property IPServidor:string read fIPServidor;
          property Porta:integer read fPorta;
          property Usuario:string read fUsuario;
          property Senha:string read fSenha;
          property BaseDeDados:string read fBaseDeDados;
          property EmpresaLogada:integer read fEmpresaLogada;
          property FuncionarioLogado:integer read fFuncionarioLogado;
          property UsuarioLogado:string read fUsuarioLogado;
          property VendorLib:string read getVendorLib;

          constructor create;
    end;

implementation

constructor TLeitorParametroSubPrograma.create;
begin
     self.fIPServidor         := paramstr(1);
     self.fPorta              := StrToIntDef(paramstr(2),5432);
     self.fUsuario            := paramstr(3);
     self.fSenha              := paramstr(4);
     self.fBaseDeDados        := paramstr(5);
     self.fEmpresaLogada      := StrToIntDef(paramstr(6),0);
     self.fFuncionarioLogado  := StrToIntDef(paramstr(7),0);
     self.fUsuarioLogado      := paramstr(8);

     if (self.IPServidor = EmptyStr)  then raise Exception.Create('Informe o IP nos par�metros');
     if (self.Usuario = EmptyStr)     then raise Exception.Create('Informe o Usu�rio nos par�metros');
     if (self.Senha = EmptyStr)       then raise Exception.Create('Informe a Senha nos par�metros');
     if (self.BaseDeDados = EmptyStr) then raise Exception.Create('Informe a Base de Dados nos par�metros');
     if (self.EmpresaLogada = 0)      then raise Exception.Create('Informe a empresa nos par�metros');
     if (self.FuncionarioLogado = 0)  then raise Exception.Create('Informe o funcion�rio nos par�metros');

end;


function TLeitorParametroSubPrograma.getDiretorioLib:string;
begin
     result := ExtractFileDir(Application.ExeName)+'\lib\10\';
end;

function TLeitorParametroSubPrograma.getVendorLib:string;
begin
     result := getDiretorioLib+'libpq.dll';
end;

end.
