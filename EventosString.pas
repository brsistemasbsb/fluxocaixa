unit EventosString;

interface

uses SysUtils;
{In�cio Se��o de Declara��o de Tipos}

    function retornaPosUltimo(textoProcurado,textoAux:string):integer;
    function ApagaPalavra(iTexto: WideString; posicao: char):WideString;
    function ApenasUm(Texto:String;caracter:char):String;
    function limpaString(texto:String):String;

   {Fim Se��o de Declara��o de Vari�veis}


implementation

function retornaPosUltimo(textoProcurado,textoAux:string):integer;
var posini,posfinal:integer;
begin
     posfinal:=0;
     posini:=pos(textoProcurado,textoAux);
     while posini>0 do
     begin
          if posfinal > 0
             then
                 posfinal:=posfinal+posini+length(textoProcurado)
             else
                 posfinal:=posfinal+posini;
          textoAux := copy(TextoAux,posini+length(textoProcurado),length(TextoAux));
          posini:=pos(textoProcurado,textoAux);
     end;
     result := posfinal;
end;

function ApagaPalavra(iTexto: WideString; posicao: char):WideString;
Var ParentesesA,ParentesesF,i:Integer;
begin
     ParentesesA:=0; ParentesesF:=0;
     if posicao = 'u' //Ultima
        then
            begin
                 While iTexto[length(iTexto)]=' ' do  //para o caso de mais de um espaco.
                 begin
                    Delete(iTexto,length(iTexto),1);
                 end;

                 i:=length(iTexto);
                 While ((iTexto[i] <> ' ') and (iTexto[i] <> '(')) or (ParentesesA<>ParentesesF) do
                 begin
                      if iTexto[i] = '('
                         then ParentesesA:=ParentesesA+1
                         else if iTexto[i] = ')' then ParentesesF:=ParentesesF+1;
                      Delete(iTexto,i,1);
                      i:=length(iTexto);
                 end;
            end
        else
            begin
                 if iTexto <> EmptyStr
                    then
                        begin
                             While iTexto[1]=' ' do  //para o caso de mais de um espaco.
                             begin
                                Delete(iTexto,1,1);
                             end;

                             While ((iTexto <> '') and (iTexto[1] <> ' ') and (iTexto[1] <> ',')) or (ParentesesA<>ParentesesF) do
                             begin
                                  if iTexto[1] = '('
                                     then ParentesesA:=ParentesesA+1
                                     else if iTexto[1] = ')' then ParentesesF:=ParentesesF+1;
                                  Delete(iTexto,1,1);
                             end;
                        end;
            end;
     result := iTexto;
end;

function ApenasUm(Texto:String;caracter:char):String; //N�o permite a inser��o de um mesmo caracter paralelamente
var i:integer;
begin
   i:=1;
   while i<=(Length(Texto)-1) do
   begin
       if (Texto[i] = caracter)and(Texto[i] = Texto[i+1])
          then
             delete(Texto,i,1)
          else
             inc(i);
   end;
   Result:=Texto;
end;

function limpaString(texto:String):String;
var c:Char;
    i:Integer;
begin
     Result:=EmptyStr;
     for i:=1 to Length(texto) do
     begin
          c:=texto[i];
          Case c of
           '�','�','�','�'                : c :='a';
           '�','�','�','�'                : c :='A';
           '�','�','�'                    : c :='e';
           '�','�','�'                    : c :='E';
           '�','�','�'                    : c :='i';
           '�','�','�'                    : c :='I';
           '�','�','�','�'                : c :='o';
           '�','�','�','�'                : c :='O';
           '�','�','�'                    : c :='u';
           '�','�','�'                    : c :='U';
           '�'                            : c :='c';
           '�'                            : c :='C';
           '�'                            : c :='n';
           '�'                            : c :='N';
           '�'                            : begin
                                                Result:=Result+'o';       
                                                C :='.';
                                            end;
           '�'                            : begin
                                                Result:=Result+'a';
                                                C :='.';
                                            end;
           '%'                            : c:=#0;
           '.'                            : c:=#32;
          end;
          if Not (c in ['A'..'Z','a'..'z','0'..'9'])
             then
                 c:=#32;

          if not (c = #32)
             then
                 Result:=Result+c;
     end;
end;


end.
