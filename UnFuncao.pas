unit UnFuncao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, jpeg, Buttons, ComCtrls, UnMens, DB,
  UnDM,MaskUtils, Math, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type

  TTipoMensagem  = (tmExclusao,tmGravacao,tmAdvertencia,tmInformacao,tmConfirmacao,tmResAC,tmResAEC,tmErro);
  TTipoMensagens = set of TTipoMensagem;


function Arredondar(pValor: Currency; pCasasDecimais:Integer=2;pTipo: String='R'): double;
procedure ExecutaQuery(TextoSql:WideString;Parametro:Variant);
function EncontraValor(TextoSql:WideString;Parametro:Variant):Variant;
function Encontrou(TextoSql:WideString;Parametro:Variant):boolean;
function Mensagem(Texto:WideString;TipoMensagem: TTipoMensagens):Integer;
function sql_OpenQuery(Query: TFDQuery): boolean;
function LPad(value:string; tamanho:integer; caractere:char): string;
function RPad(value:string; tamanho:integer; caractere:char): string;
function ValidarCNPJCPF(Nr_CGC:String;Formata:Boolean):String;
function RetirarPonto(S: String):String;



const

     Exclusao    = [tmExclusao];
     Gravacao    = [tmGravacao];
     Advertencia = [tmAdvertencia];
     Informacao  = [tmInformacao];
     Confirmacao = [tmConfirmacao];
     ResAC       = [tmResAC];
     ResAEC      = [tmResAEC];
     Erro        = [tmErro];

var
   CodigoLojaLogada: Integer;
   CodigoPDVLogado: Integer;
   NumeroPDVLogado: String;
   CodigoOperadLogado: Integer;
   CodigoOpnfNF: Integer;
   CodigoOpnfDoc: Integer;
   RegTrib: String;


   OperadLogado: String;
   CodigoFiscalLogado: Integer;

implementation

function Arredondar(pValor: Currency; pCasasDecimais:Integer=2;pTipo: String='R'): double;
var
   fator : Extended;
begin
     //if pTipo='R'
        //then

     fator := Power(10,pCasasDecimais);
     pValor := (fator * pValor);
     if pTipo='T'
        then
            pValor := Trunc(pValor)
        else
            pValor := Round(pValor);

     Result := (pValor * (1/fator));

end;


procedure ExecutaQuery(TextoSql:WideString;Parametro:Variant);
var
  i,QtdePar:byte;
begin
 with TFDQuery.Create(Application) do
 begin
   try
      Connection:=DM.FDConnection1;
      sql.text:=TextoSql;
      if VarIsArray(Parametro)
         then
             begin
                  QtdePar:=VarArrayHighBound(Parametro,1);
                  for i:=0 to QtdePar do
                      Params[i].value:=Parametro[i]
             end
         else
             if Params.Count > 0
                then
                    Params[0].Value:=Parametro;

      ExecSQl;
   Finally
      Free;
   end;
 end;
end;

function EncontraValor(TextoSql:WideString;Parametro:Variant):Variant;  //Procura por um valor de acordo com os par�metros passados
var
  QueryValor:TFDQuery;
  resultado:variant;
  i,QtdePar,QtdeCampos:integer;

  procedure RetornaValores;
  begin
      With QueryValor do
      begin
         QtdeCampos := Fields.Count-1;
         if QtdeCampos > 0
            then
                begin
                     Resultado := VarArrayCreate([0,QtdeCampos],varVariant);
                     i:=0;
                     While i<=QtdeCampos do
                     begin
                          if Fields[i].IsNull
                             then
                                 Case (Fields[i].DataType) of
                                      ftString,ftBlob,ftMemo,ftFmtMemo,ftFixedChar,ftWideString : Resultado[i]:= '';
                                      ftSmallint,ftInteger,ftWord,ftFloat,ftCurrency,ftLargeInt,
                                      ftBytes,ftAutoInc                                         : Resultado[i]:= 0;
                                      ftBoolean                                                 : Resultado[i]:= False;
                                      ftDate                                                    : Resultado[i]:= StrTodate('01/01/1500');
                                      ftTime                                                    : Resultado[i]:= StrToTime('00:00:00');
                                      ftDateTime                                                : Resultado[i]:= StrToDateTime('01/01/1500 00:00');
                                 end
                             else
                                 Resultado[i] := Fields[i].Value;
                          inc(i);
                     end;
                     Result:=Resultado;
                end
            else
                begin
                     if Fields[0].IsNull
                        then
                            begin
                                 Case (Fields[0].DataType) of
                                      ftString,ftBlob,ftMemo,ftFmtMemo,ftFixedChar,ftWideString : Result:='';
                                      ftSmallint,ftInteger,ftWord,ftFloat,ftCurrency,ftLargeInt,
                                      ftBytes,ftAutoInc                                         : Result:=0;
                                      ftBoolean                                                 : Result:=False;
                                      ftDate                                                    : Result:= StrTodate('01/01/1500');
                                      ftTime                                                    : Result:= StrToTime('00:00:00');
                                      ftDateTime                                                : Result:= StrToDateTime('01/01/1500 00:00');
                                 end;
                            end
                        else
                            Result:=Fields[0].Value;
                end;
      end;
  end;

begin
     try
        QueryValor := TFDQuery.Create(Application);
        QueryValor.Connection:=DM.FDConnection1;
        QueryValor.sql.text:=TextoSql;
        if VarIsArray(Parametro)

           then
               begin
                    QtdePar:=VarArrayHighBound(Parametro,1);
                    for i:=0 to QtdePar do
                        QueryValor.Params[i].value:=Parametro[i]
               end
           else
               if QueryValor.Params.Count > 0 //Qdo a fun��o n�o tem nenhum par�metro, o mesmo vir� como EmptyStr
                  then
                      QueryValor.Params[0].Value:=Parametro;

        sql_OpenQuery(QueryValor);
         QueryValor.First;
        RetornaValores;
     Finally
        QueryValor.Free;
     end;
end;

function Encontrou(TextoSql:WideString;Parametro:Variant):boolean;  //Verifica se existe um determinado valor na Tabela de acordo com o texto passado como par�metro
var
   QtdePar,i:byte;
begin
  with TFDQuery.Create(Application) do
  begin
    try
       Connection:=DM.FDConnection1;
       sql.text:=TextoSql;
       if VarIsArray(Parametro)
          then
              begin
                   QtdePar:=VarArrayHighBound(Parametro,1);
                   for i:=0 to QtdePar do
                       Params[i].value:=Parametro[i]
              end
          else
              if Params.Count > 0
                 then
                     Params[0].Value:=Parametro;
       Open;
       if IsEmpty
         then Result:=false
         else Result:=true;
    Finally
       Free;
    end;
  end;
end;

function Mensagem(Texto:WideString;TipoMensagem: TTipoMensagens):Integer;  //Configura o tipo de mensagem que aparecer� para o usu�rio
begin
     try
        Application.CreateForm(TFormMens,FormMens);
        //FormMens.Memo1.Text := AnsiUpperCase(UsuarioLogado)+#13#10+ Texto;
        FormMens.Memo1.Text := Texto;

        if tmExclusao in TipoMensagem
           then
               begin
                    FormMens.bbSim.Left := FormMens.bbCancel.Left;
                    FormMens.bbNao.Left := FormMens.bbOK.Left;
                    FormMens.bbSim.Visible := true;
                    FormMens.bbNao.Visible := true;
                    FormMens.ImgExc.Visible := true;
                    FormMens.Caption := 'Mensagem Confirma Exclus�o';
                    FormMens.botaoFoco:=FormMens.bbNao;
               end;
        if tmGravacao in TipoMensagem
           then
               begin
                    FormMens.bbSim.Left := FormMens.bbNao.Left;
                    FormMens.bbNao.Left := FormMens.bbCancel.Left;
                    FormMens.bbCancel.Left := FormMens.bbOK.Left;
                    FormMens.bbSim.Visible := true;
                    FormMens.bbNao.Visible := true;
                    FormMens.bbCancel.Visible := true;
                    FormMens.ImgGrav.Visible := true;
                    FormMens.Caption := 'Mensagem Confirma Grava��o';
                    FormMens.botaoFoco:=FormMens.bbCancel;
               end;

        if tmAdvertencia in TipoMensagem
           then
               begin
                    FormMens.bbOK.Visible := true;
                    FormMens.ImgAdv.Visible := true;
                    FormMens.Caption := 'Mensagem de Advert�ncia';
                    FormMens.botaoFoco:=FormMens.bbOk;
               end;

        if tmInformacao in TipoMensagem
           then
               begin
                    FormMens.bbOK.Visible := true;
                    FormMens.ImgInfo.Visible := true;
                    FormMens.Caption := 'Mensagem de Informa��o';
                    FormMens.botaoFoco:=FormMens.bbOk;
               end;

        if tmConfirmacao in TipoMensagem
           then
               begin
                    FormMens.bbSim.Left := FormMens.bbCancel.Left;
                    FormMens.bbNao.Left := FormMens.bbOK.Left;
                    FormMens.bbSim.Visible := true;
                    FormMens.bbNao.Visible := true;
                    FormMens.ImgConf.Visible := true;
                    FormMens.Caption := 'Mensagem de Confirma��o';
                    FormMens.botaoFoco:=FormMens.bbNao;
               end;
        if tmResAC in TipoMensagem
           then
               begin
                    FormMens.bbSim.Left := FormMens.bbCancel.Left;
                    FormMens.bbCancel.Left := FormMens.bbOK.Left;
                    FormMens.bbSim.Visible := true;
                    FormMens.bbCancel.Visible := true;
                    FormMens.ImgResAEC.Visible := true;
                    FormMens.bbSim.Caption := '&Alterar';
                    FormMens.bbCancel.Caption := '&Cancelar';
                    FormMens.Caption := 'Aten��o';
                    FormMens.botaoFoco:=FormMens.bbCancel;
               end;
        if tmResAEC in TipoMensagem
           then
               begin
                    FormMens.bbSim.Left := FormMens.bbNao.Left;
                    FormMens.bbNao.Left := FormMens.bbCancel.Left;
                    FormMens.bbCancel.Left := FormMens.bbOK.Left;
                    FormMens.bbSim.Visible := true;
                    FormMens.bbNao.Visible := true;
                    FormMens.bbCancel.Visible := true;
                    FormMens.ImgResAEC.Visible := true;
                    FormMens.bbSim.Caption := '&Alterar';
                    FormMens.bbNao.Caption := '&Excluir';
                    FormMens.bbCancel.Caption := '&Cancelar';
                    FormMens.Caption := 'Aten��o';
                    FormMens.botaoFoco:=FormMens.bbSim;
               end;
        FormMens.ShowModal;
        result := FormMens.ModalResult;
     finally

        FormMens.Release;
        FormMens := nil;
     end;
end;

function sql_OpenQuery(Query: TFDQuery): boolean;
var Cur: TCursor;
begin
     Cur := Screen.Cursor;
     result:=true;
     with Query do
     begin
          if Active then Exit;
          try
             Screen.Cursor := crHourGlass;
             try
                Open;
             except
                   on E: Exception do
                   begin
                        Screen.Cursor := crDefault;
                        result:=false;
                        raise exception.create('Erro na abertura da tabela' + #13#13 + E.Message + #13#13 + Text);
                   end;
             end
          finally
                 Screen.Cursor := Cur;
          end;
     end;
end;

function LPad(value:string; tamanho:integer; caractere:char): string;
var i: integer;
begin
     Result := value;
     if (Length(value) > tamanho)
        then
            exit;
        for i := 1 to (tamanho - Length(value)) do
        Result := caractere + Result;
end;

function RPad(value:string; tamanho:integer; caractere:char): string;
var i: integer;
begin
     Result := value;
     if (Length(value) > tamanho)
        then
            exit;
     for i := 1 to (tamanho - Length(value)) do
         Result := Result + caractere;
end;

function ValidarCNPJCPF(Nr_CGC:String;Formata:Boolean):String;
var
  Digito1,Digito2: String;
  S,Cont,Digito,Soma: Integer;
begin
  //Nr_Cgc:=Numeros(Nr_Cgc);
  Nr_Cgc:=Nr_Cgc;

  if Length(Nr_Cgc)=11 then begin
    {Primeiro D�gito }
    Cont:=1;
    Soma:=0;
    for S:=9 Downto 1 do begin
      Inc(Cont);
      Soma:=Soma + StrToInt(Nr_Cgc[S]) * Cont;
    end;
    Soma:=Soma * 10;
    Digito1:=IntToStr(Soma Mod 11);
    if StrToInt(Digito1)>=10 then
      Digito1:='0';
    // Segundo D�gito
    Cont:=1;
    Soma:=0;
    for S:=10 Downto 1 do begin
      Inc(Cont);
      Soma:=Soma + StrToInt(Nr_Cgc[S]) * Cont;
    end;
    Soma:=Soma * 10;
    Digito2:=IntToStr(Soma Mod 11);
    if StrToInt(Digito2)>=10 then
      Digito2:='0';
  end else if Length(Nr_Cgc) = 14 then begin
    Soma:=5 * StrToInt(Nr_Cgc[1]) + 4 * StrToInt(Nr_Cgc[2]) + 3 * StrToInt(Nr_Cgc[3])+
          2 * StrToInt(Nr_Cgc[4]) + 9 * StrToInt(Nr_Cgc[5]) + 8 * StrToInt(Nr_Cgc[6])+
          7 * StrToInt(Nr_Cgc[7]) + 6 * StrToInt(Nr_Cgc[8]) + 5 * StrToInt(Nr_Cgc[9])+
          4 * StrToInt(Nr_Cgc[10])+ 3 * StrToInt(Nr_Cgc[11])+ 2 * StrToInt(Nr_Cgc[12]);
    Digito:=Soma Mod 11;
    if Digito>1 then
      Digito:=11-Digito
    else
      Digito:=0;
    Digito1:=IntToStr(Digito);

    Soma:=6 * StrToInt(Nr_Cgc[1]) + 5 * StrToInt(Nr_Cgc[2]) + 4 * StrToInt(Nr_Cgc[3])+
          3 * StrToInt(Nr_Cgc[4]) + 2 * StrToInt(Nr_Cgc[5]) + 9 * StrToInt(Nr_Cgc[6])+
          8 * StrToInt(Nr_Cgc[7]) + 7 * StrToInt(Nr_Cgc[8]) + 6 * StrToInt(Nr_Cgc[9])+
          5 * StrToInt(Nr_Cgc[10])+ 4 * StrToInt(Nr_Cgc[11])+ 3 * StrToInt(Nr_Cgc[12])+
          2 * StrToInt(Digito1);
    Digito:=Soma Mod 11;
    if Digito>1 then
      Digito:=11-Digito
    else
      Digito:=0;
    Digito2:=IntToStr(Digito);
  end else begin
    Result:='';
    Exit;
  end;

  if Copy(Nr_Cgc,Length(Nr_Cgc)-1,2) <> Digito1+Digito2 then
    Result:=''
  else if Formata then begin
    if Length(Nr_Cgc)=11 then
      Result:=FormatMaskText('999.999.999/99;0;',Nr_Cgc)
    else
      Result:=FormatMaskText('99.999.999/9999-99;0;',Nr_Cgc);
  end else
    Result:=Nr_Cgc;

end;

function RetirarPonto(S: String):String;
begin
     Result:=StringReplace(S, '.', '', [rfReplaceAll, rfIgnoreCase])
end;


end.
