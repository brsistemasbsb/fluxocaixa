unit Eventos;

interface

uses
  Windows, Messages, SysUtils, comctrls, dbgrids, Classes, Graphics, Controls, Forms, Dialogs,
  Db, dbctrls, stdctrls, Menus, Buttons, checklst, Mask, TypInfo, ShellAPI,Commctrl,
  Variants, Printers, extCtrls, Math, Registry
  ,grids,
  ACBrNFe, ACBrBase,ACBrETQ,ACBrETQClass, AcbrDevice,MSHTML,OleCtrls,SHDocVw,ActiveX,ExtActns
  ,FileCtrl,FireDAC.Comp.Client;

{In�cio Se��o de Declara��o de Tipos}
type
    TIntegerArray = array of integer;
    TTipoMensagem  = (tmExclusao,tmGravacao,tmAdvertencia,tmInformacao,tmConfirmacao,tmResAC,tmResAEC,tmErro);
    TTipoMensagens = set of TTipoMensagem;
    RefForm=^TipoForm;
    TipoForm=
             record
                   Form:TForm;
                   nome_do_form:String;
                   Ativo:boolean;
                   Prox:RefForm;
             end;

    DOpNF = record
             Status,Tipo,Especie,Rateio,natureza:String[2];
             TipoEmissao:String[1];
             GeraConta,GT,CFOP,ConsistePed,CalcCusto,PISCOFINS:Boolean;
             Codigo,Conta,centro_custo,GTCodE,GTCodI,CFOPCodE,CFOPCodI,CFOPCodINContrICMS:Integer;
          end;
    
const

     Exclusao    = [tmExclusao];
     Gravacao    = [tmGravacao];
     Advertencia = [tmAdvertencia];
     Informacao  = [tmInformacao];
     Confirmacao = [tmConfirmacao];
     ResAC       = [tmResAC];
     ResAEC      = [tmResAEC];
     Erro        = [tmErro];

    function Mensagem(Texto:WideString;TipoMensagem: TTipoMensagens):Integer;
    function MensagemSenha(Texto:WideString;Sql:String= ''):Integer;
    procedure OnEnterDoDBEdit(Sender:TObject);
    procedure OnExitDoDBEdit(Sender:TObject);
    function EncontraValor(TextoSql:WideString;Parametro:Variant):Variant;
    function Encontrou(TextoSql:WideString;Parametro:Variant):boolean;
    function sql_OpenQuery(Query: TFDQuery): boolean;
    procedure ExecutaQuery(TextoSql:WideString;Parametro:Variant);
    function TextoPuro(texto:String;NaoPerc:Boolean=False;NaoPonto:boolean = false):String;
    function ListaAlinhEsq(texto:WideString;Limite:Integer;AcabaEnter:Boolean=True;LinhaVazia:Boolean=True):TStringList;
    function SomenteNumeros(Documento:String):String;
    function Mod11de9a2(Valor:String):String;
    function RetornaDataHora:TDateTime;
    procedure OnPaintDoForm(form:tform = nil);
    function CarregaHint(Form:Tform):variant;
    procedure FORM_desalocarForm(var lista:RefForm;form:tform);
    procedure OnKeyPressDoForm(Sender:TObject; Var Key:char);
    function ApagaEspacos(texto:String):String;
    procedure RetornaDiretorio(Sender:TObject);
    procedure OnShowDoForm(Sender:TObject; Ds: TDataSource);
    procedure OnKeyPressDosNumeros(Var Key:Char);
    procedure OnDBGridEnterLook(Campo:TField);
    function OnDBGridExitLook(DBGridL:TDBGrid;Descricoes:array of TField;TextoSql,TextoSqlL:WideString;DispEsqDir:Boolean):Boolean;
    function ConverterValoresString(Dados:variant):variant;
    function OnDBGridLook(DBGridL:TDBGrid;Descricoes:array of TField;TextoSqlL:WideString;Proximo:Boolean;DispEsqDir:Boolean):Boolean;
    function FormataValorEmString(valor:Variant):String;
    procedure OnShowDoFormL(DBGridB:TDBGrid);
    function BuscaDisplayLabel(Ds:TDataSource;campo: string):string;
    function BuscaDisplayWidth(Ds:TDataSource;campo: string):Integer;
    function RetornaWidthDefault(TipoDoCampo:TFieldType):Integer;
    procedure OnKeyPressDoDBGridLook(DBGridL:TDBGrid;Key:Char);
    function ValorNull(valor:Variant):boolean;
    function NumeroSemMascara(Valor:String):Double; 

   {Fim Se��o de Declara��o de Vari�veis}
var
   FuncionarioLogado,EmpresaLogada:integer;
   UsuarioLogado:String;
   IPServidor,BancoDados:String;
   NaoPassouOnEnter,BotaoAtivo:boolean;
   ConteudoDBEditAntLook : String;  //Guarda o �ltimo valor do campo lookup
   Lista_de_Form:RefForm;
   ColAntDBGridBL: Integer;  // Vari�vel usada no FormL2.DBGridB2 para armazenar o �ltimo �ndice da coluna em que foi feita a pesquisa
   

implementation

uses UnMens, UnMensSenha, UnL, UnDMConexao;

function Apagaespacos(texto:String):String;
var i:integer;
begin
   i:=1;
   While i<=Length(texto) do
   begin
      if texto[i] = ' '
         then delete(texto,i,1)
         else inc(i);
   end;
   Result:=texto;
end;


function RetornaDataHora:TDateTime;  //Retorna a data e hora do sistema operacional onde se encontra o Banco de Dados
begin
     with TFDQuery.Create(Application) do
     begin
          ConnectionName := DMConexao.dbzae.ConnectionName;
          Close;
          Sql.Text:='select now() as data;';
          Open;
          Result:=FieldByName('data').AsDateTime;
          Free;
     end;
end;

function Mod11de9a2(Valor:String):String;
var
   Peso,i:byte;
   soma,resto:integer;
begin
     Peso:=2;
     soma:=0;
     Valor:=trim(Valor);
     for i:=length(Valor) Downto 1  do
     begin
          if Peso>9
             then
                 Peso:=2;
          soma:=soma+StrToInt(Valor[i])*Peso;
          Inc(Peso);
     end;

     //soma:=soma*10;
     if (soma >= 11)
        then
            begin
                 resto:=(soma mod 11);
                 if (resto > 1)
                    then
                        Result:= IntToStr(11 - resto)
                    else
                        begin
                             if (resto = 1) or (resto = 0)
                                then
                                    Result:= '0';
                        end;
            end
        else
            Result := IntToStr(11 - soma);

     //result:=IntToStr(resto);
end;

function SomenteNumeros(Documento:String):String;  //Retorna apenas n�meros de uma determinada string
var i:integer;
    aux:String;
begin
    aux:=EmptyStr;
    if Documento<>EmptyStr
       then
           begin
                i:=Length(Documento);
                while i>0 do
                begin
                     if Documento[i] in ['0'..'9']
                        then aux:=Documento[i]+aux;
                     i:=i-1;
                end;
           end;
    Result:=aux;
end;

procedure ExecutaQuery(TextoSql:WideString;Parametro:Variant);
var
   i,QtdePar:byte;
begin
  with TFDQuery.Create(Application) do
  begin
    try
       ConnectionName := DMConexao.dbzae.ConnectionName;
       sql.text:=TextoSql;
       if VarIsArray(Parametro)
          then
              begin
                   QtdePar:=VarArrayHighBound(Parametro,1);
                   for i:=0 to QtdePar do
                       Params[i].value:=Parametro[i]
              end
          else
              if Params.Count > 0
                 then
                     Params[0].Value:=Parametro;

       ExecSQl;
    Finally
       Free;
    end;
  end;
end;

function sql_OpenQuery(Query: TFDQuery): boolean;
var Cur: TCursor;
begin
     Cur := Screen.Cursor;
     result:=true;
     with Query do
     begin
          if Active then Exit;
          try
             Screen.Cursor := crHourGlass;
             try
                Open;
             except
                   on E: Exception do
                   begin
                        Screen.Cursor := crDefault;
                        result:=false;
                        raise exception.create('Erro na abertura da tabela' + #13#13 + E.Message + #13#13 + Text);
                   end;
             end
          finally
                 Screen.Cursor := Cur;
          end;
     end;
end;

function Encontrou(TextoSql:WideString;Parametro:Variant):boolean;  //Verifica se existe um determinado valor na Tabela de acordo com o texto passado como par�metro
var
   QtdePar,i:byte;
begin
  with TFDQuery.Create(nil) do
  begin
    try
       ConnectionName := DMConexao.dbzae.ConnectionName;
       sql.text:=TextoSql;
       if VarIsArray(Parametro)
          then
              begin
                   QtdePar:=VarArrayHighBound(Parametro,1);
                   for i:=0 to QtdePar do
                       Params[i].value:=Parametro[i]
              end
          else
              if Params.Count > 0
                 then
                     Params[0].Value:=Parametro;
       Open;
       if IsEmpty
         then Result:=false
         else Result:=true;
    Finally
       Free;
    end;
  end;
end;

function EncontraValor(TextoSql:WideString;Parametro:Variant):Variant;  //Procura por um valor de acordo com os par�metros passados
var
  QueryValor:TFDQuery;
  resultado:variant;
  i,QtdePar,QtdeCampos:integer;

  procedure RetornaValores;
  begin
      With QueryValor do
      begin
         QtdeCampos := Fields.Count-1;
         if QtdeCampos > 0
            then
                begin
                     Resultado := VarArrayCreate([0,QtdeCampos],varVariant);
                     i:=0;
                     While i<=QtdeCampos do
                     begin
                          if Fields[i].IsNull
                             then
                                 Case (Fields[i].DataType) of
                                      ftString,ftBlob,ftMemo,ftFmtMemo,ftFixedChar,ftWideString : Resultado[i]:= '';
                                      ftSmallint,ftInteger,ftWord,ftFloat,ftCurrency,ftLargeInt,
                                      ftBytes,ftAutoInc                                         : Resultado[i]:= 0;
                                      ftBoolean                                                 : Resultado[i]:= False;
                                      ftDate                                                    : Resultado[i]:= StrTodate('01/01/1500');
                                      ftTime                                                    : Resultado[i]:= StrToTime('00:00:00');
                                      ftDateTime                                                : Resultado[i]:= StrToDateTime('01/01/1500 00:00');
                                 end
                             else
                                 Resultado[i] := Fields[i].Value;
                          inc(i);
                     end;
                     Result:=Resultado;
                end
            else
                begin
                     if Fields[0].IsNull
                        then
                            begin
                                 Case (Fields[0].DataType) of
                                      ftString,ftBlob,ftMemo,ftFmtMemo,ftFixedChar,ftWideString : Result:='';
                                      ftSmallint,ftInteger,ftWord,ftFloat,ftCurrency,ftLargeInt,
                                      ftBytes,ftAutoInc                                         : Result:=0;
                                      ftBoolean                                                 : Result:=False;
                                      ftDate                                                    : Result:= StrTodate('01/01/1500');
                                      ftTime                                                    : Result:= StrToTime('00:00:00');
                                      ftDateTime                                                : Result:= StrToDateTime('01/01/1500 00:00');
                                 end;
                            end
                        else
                            Result:=Fields[0].Value;
                end;
      end;
  end;

begin
     try
        QueryValor := TFDQuery.Create(nil);
        QueryValor.ConnectionName := DMConexao.dbzae.ConnectionName;
        QueryValor.sql.text:=TextoSql;
        if VarIsArray(Parametro)
           then
               begin
                    QtdePar:=VarArrayHighBound(Parametro,1);
                    for i:=0 to QtdePar do
                        QueryValor.Params[i].value:=Parametro[i]
               end
           else
               if QueryValor.Params.Count > 0 //Qdo a fun��o n�o tem nenhum par�metro, o mesmo vir� como EmptyStr
                  then
                      QueryValor.Params[0].Value:=Parametro;

        sql_OpenQuery(QueryValor);
         QueryValor.First;
        RetornaValores;
     Finally
        QueryValor.Free;
     end;
end;


function Mensagem(Texto:WideString;TipoMensagem: TTipoMensagens):Integer;  //Configura o tipo de mensagem que aparecer� para o usu�rio
begin
     try
        Application.CreateForm(TFormMens,FormMens);
        //FormMens.Memo1.Text := AnsiUpperCase(UsuarioLogado)+#13#10+ Texto;
        FormMens.Memo1.Text := Texto;

        if tmExclusao in TipoMensagem
           then
               begin
                    FormMens.bbSim.Left := FormMens.bbCancel.Left;
                    FormMens.bbNao.Left := FormMens.bbOK.Left;
                    FormMens.bbSim.Visible := true;
                    FormMens.bbNao.Visible := true;
                    FormMens.ImgExc.Visible := true;
                    FormMens.Caption := 'Mensagem Confirma Exclus�o';
                    FormMens.botaoFoco:=FormMens.bbNao;
               end;
        if tmGravacao in TipoMensagem
           then
               begin
                    FormMens.bbSim.Left := FormMens.bbNao.Left;
                    FormMens.bbNao.Left := FormMens.bbCancel.Left;
                    FormMens.bbCancel.Left := FormMens.bbOK.Left;
                    FormMens.bbSim.Visible := true;
                    FormMens.bbNao.Visible := true;
                    FormMens.bbCancel.Visible := true;
                    FormMens.ImgGrav.Visible := true;
                    FormMens.Caption := 'Mensagem Confirma Grava��o';
                    FormMens.botaoFoco:=FormMens.bbCancel;
               end;

        if tmAdvertencia in TipoMensagem
           then
               begin
                    FormMens.bbOK.Visible := true;
                    FormMens.ImgAdv.Visible := true;
                    FormMens.Caption := 'Mensagem de Advert�ncia';
                    FormMens.botaoFoco:=FormMens.bbOk;
               end;

        if tmInformacao in TipoMensagem
           then
               begin
                    FormMens.bbOK.Visible := true;
                    FormMens.ImgInfo.Visible := true;
                    FormMens.Caption := 'Mensagem de Informa��o';
                    FormMens.botaoFoco:=FormMens.bbOk;
               end;

        if tmConfirmacao in TipoMensagem
           then
               begin
                    FormMens.bbSim.Left := FormMens.bbCancel.Left;
                    FormMens.bbNao.Left := FormMens.bbOK.Left;
                    FormMens.bbSim.Visible := true;
                    FormMens.bbNao.Visible := true;
                    FormMens.ImgConf.Visible := true;
                    FormMens.Caption := 'Mensagem de Confirma��o';
                    FormMens.botaoFoco:=FormMens.bbNao;
               end;
        if tmResAC in TipoMensagem
           then
               begin
                    FormMens.bbSim.Left := FormMens.bbCancel.Left;
                    FormMens.bbCancel.Left := FormMens.bbOK.Left;
                    FormMens.bbSim.Visible := true;
                    FormMens.bbCancel.Visible := true;
                    FormMens.ImgResAEC.Visible := true;
                    FormMens.bbSim.Caption := '&Alterar';
                    FormMens.bbCancel.Caption := '&Cancelar';
                    FormMens.Caption := 'Aten��o';
                    FormMens.botaoFoco:=FormMens.bbCancel;
               end;
        if tmResAEC in TipoMensagem
           then
               begin
                    FormMens.bbSim.Left := FormMens.bbNao.Left;
                    FormMens.bbNao.Left := FormMens.bbCancel.Left;
                    FormMens.bbCancel.Left := FormMens.bbOK.Left;
                    FormMens.bbSim.Visible := true;
                    FormMens.bbNao.Visible := true;
                    FormMens.bbCancel.Visible := true;
                    FormMens.ImgResAEC.Visible := true;
                    FormMens.bbSim.Caption := '&Alterar';
                    FormMens.bbNao.Caption := '&Excluir';
                    FormMens.bbCancel.Caption := '&Cancelar';
                    FormMens.Caption := 'Aten��o';
                    FormMens.botaoFoco:=FormMens.bbSim;
               end;
        FormMens.ShowModal;
        result := FormMens.ModalResult;
     finally

        FormMens.Release;
        FormMens := nil;
     end;
end;

//Fun��o que requisita  a senha de um usu�rio com base na sql
function MensagemSenha(Texto:WideString;Sql:String = ''):Integer;
begin
     {
     try
        Application.CreateForm(TFormMensSenha,FormMensSenha);
        //FormMensSenha.Memo1.Text := AnsiUpperCase(UsuarioLogado)+#13#10+ Texto;
        FormMensSenha.Memo1.Text := '';
        if sql  = ''
           then
               sql:='select usuempparcod,usufunparcod from usu where usufunparcod = '+IntToStr(FuncionarioLogado)+' and ususenha=md5(:0) limit 1';
        FormMensSenha.SQl := sql;

        FormMensSenha.ShowModal;
        if (FormMensSenha.ModalResult <> mryes)
           then
               Mensagem('Senha Incorreta!',Advertencia);
        result := FormMensSenha.ModalResult;
     finally
        FormMensSenha.Release;
        FormMensSenha := nil;
     end;
     ^}

     result := 1;
end;

procedure OnEnterDoDBEdit(Sender:TObject);  //Coloca cor quando entra no objeto
begin
     if (Sender is TDBEdit)
        then
            with (Sender as TDBEdit) do
            begin
                 color:=$00C08000;
                 font.color:=clwhite;
                 font.style:=[fsbold];
            end
        else
            if (Sender is TDBComboBox)
               then
                   with (Sender as TDBComboBox) do
                   begin
                        color:=$00C08000;
                        font.color:=clwhite;
                        font.style:=[fsbold];
                   end
               else
                   if (Sender is TEdit)
                      then
                          with (Sender as TEdit) do
                          begin
                               color:=$00C08000;
                               font.color:=clwhite;
                               font.style:=[fsbold];
                          end
                      else
                          if (Sender is TMaskEdit)
                             then
                                 with (Sender as TMaskEdit) do
                                 begin
                                      color:=$00C08000;
                                      font.color:=clwhite;
                                      font.style:=[fsbold];
                                 end
                             else
                                 if (Sender is TMemo)
                                    then
                                        with (Sender as TMemo) do
                                        begin
                                             color:=$00C08000;
                                             font.color:=clwhite;
                                             font.style:=[fsbold];
                                        end
                                    else
                                      if (Sender is TDBMemo)
                                      then
                                          with (Sender as TDBMemo) do
                                          begin
                                               color:=$00C08000;
                                               font.color:=clwhite;
                                               font.style:=[fsbold];
                                          end

                                      else
                                          if (Sender is TDateTimePicker)
                                             then
                                                 with (Sender as TDateTimePicker) do
                                                 begin
                                                      color:=$00C08000;
                                                      font.color:=clwhite;
                                                      font.style:=[fsbold];
                                                 end
                                             else
                                                 if (Sender is TColumn)
                                                    then
                                                        with (Sender as TColumn) do
                                                        begin
                                                             Color :=$00C08000;
                                                             font.color:=clwhite;
                                                             font.style:=[fsbold];
                                                        end
end;

procedure OnExitDoDBEdit(Sender:TObject);  //Retira cor quando sai do objeto
begin
     if (Sender is TDBEdit)
        then
            with (Sender as TDBEdit) do
            begin
                 color:=clwhite;
                 font.color:=clblack;
                 font.style:=[];
            end
        else

            if (Sender is TDBComboBox)
               then
                   with (Sender as TDBComboBox) do
                   begin
                        color:=clwhite;
                        font.color:=clblack;
                        font.style:=[];
                   end
               else
                   if (Sender is TEdit)
                      then
                          begin
                               if (Sender as TEdit).Enabled
                                  then
                                      with (Sender as TEdit) do
                                      begin
                                          color:=clwhite;
                                          font.color:=clblack;
                                          font.style:=[];
                                     end;
                            end
                      else
                          if (Sender is TMaskEdit)
                             then
                                 begin
                                      if (Sender as TMaskEdit).Enabled
                                         then
                                             with (Sender as TMaskEdit) do
                                             begin
                                                  color:=clwhite;
                                                  font.color:=clblack;
                                                  font.style:=[];
                                             end;
                                 end
                             else
                                 if (Sender is TMemo)
                                    then
                                        with (Sender as TMemo) do
                                        begin
                                             color:=clwhite;
                                             font.color:=clblack;
                                             font.style:=[];
                                        end
                                    else
                                      if (Sender is TDBMemo)
                                        then
                                            with (Sender as TDBMemo) do
                                            begin
                                                 color:=clwhite;
                                                 font.color:=clblack;
                                                 font.style:=[];
                                            end
                                        else
                                            if (Sender is TDateTimePicker)
                                               then
                                                   with (Sender as TDateTimePicker) do
                                                   begin
                                                        color:=clwhite;
                                                        font.color:=clblack;
                                                        font.style:=[];
                                                   end
end;

function TextoPuro(texto:String;NaoPerc:Boolean=False;NaoPonto:boolean = false):String;
var c:Char;
    i:Integer;
begin
     Result:=EmptyStr;
     for i:=1 to Length(texto) do
     begin
          c:=texto[i];
          Case c of
           '�','�','�','�'                : c :='a';
           '�','�','�','�'                : c :='A';
           '�','�','�'                    : c :='e';
           '�','�','�'                    : c :='E';
           '�','�','�'                    : c :='i';
           '�','�','�'                    : c :='I';
           '�','�','�','�'                : c :='o';
           '�','�','�','�'                : c :='O';
           '�','�','�'                    : c :='u';
           '�','�','�'                    : c :='U';
           '�'                            : c :='c';
           '�'                            : c :='C';
           '�'                            : c :='n';
           '�'                            : c :='N';
           '�'                            : begin
                                                Result:=Result+'o';       
                                                C :='.';
                                            end;
           '�'                            : begin
                                                Result:=Result+'a';
                                                C :='.';
                                            end;
           '%'                            : if NaoPerc then c:=#0;
           '.'                            : if NaoPonto then c:=#32;
          end;
          if Not (c in [#3,#22,#26,#13,#10,#9,#8,'A'..'Z','a'..'z','0'..'9','"','!','@','#',
                       '�','�','�','�','�','�','�','�','�','�','�','�','�','�',
                       '�','�','�','�','�','�','�','�','�','�','�','�','�','�',
                       '�','�','�','�','�','�','�','�','�','�','�','�','$','%',
                       '&','*','(',')','-','_','=','+','<','>','[',']','{','}',
                       '?','/','\',',','|','.',';',' ',':'])
             then
                 c:=#32;
          Result:=Result+c;
     end;
end;

function ListaAlinhEsq(texto:WideString;Limite:Integer;AcabaEnter:Boolean=True;LinhaVazia:Boolean=True):TStringList;
Var palavra,Linha:WideString;
    i,z:integer;
    Lista:TStringList;
    iEnter:Boolean;

    procedure AdicionaNaLinha;
    begin
         if Linha<>EmptyWideStr
            then
                Linha:=Linha+palavra
            else
                Linha:=palavra;
         palavra:=EmptyWideStr;
    end;

    procedure IncluiNaLista;
    begin
         Linha:=Trim(Linha);
         if ((Linha<>EmptyWideStr)and(Linha<>' ')) or LinhaVazia
            then
                Lista.Add(Linha);
         Linha:=EmptyWideStr;
    end;

    procedure ObtemPalavra;
    begin
         iEnter:=False;
         if (texto[i]=#$D) and Not(AcabaEnter)
            then
                iEnter:=True
            else
                if (texto[i]<>#$A) then palavra:=palavra+texto[i];
         i:=i+1;
         if Not iEnter
            then
                begin
                     While (texto[i]<>' ') and (i<=z) do
                     begin
                          if (texto[i]=#$D)
                             then
                                 begin
                                      iEnter:=True;
                                      i:=i+1;
                                      Break;
                                 end
                             else
                                 if (texto[i]<>#$A) then palavra:=palavra+texto[i];
                          i:=i+1;
                     end;
                end;
    end;

begin
     if AcabaEnter then texto:=StringReplace(texto,#13#10,' ',[rfReplaceall]); //Elimina quebras de linhas quando se para um texto de um StringList.
     Linha:=EmptyWideStr;
     i:=1; z:=Length(texto);
     Lista:=TStringList.Create;
     while (i<=z) and (z>0) do
     begin
         ObtemPalavra;
         if (Length(Linha)+Length(palavra) >= Limite) or iEnter
            then
                begin
                     if (Length(Linha)+Length(palavra) <= Limite)
                        then
                           begin
                                AdicionaNaLinha;
                                IncluiNaLista;
                           end
                        else
                           begin
                                IncluiNaLista;
                                AdicionaNaLinha;
                           end;
                end
            else
                AdicionaNaLinha;
     end;
     if Linha<>EmptyWideStr then IncluiNaLista;
     Result:=Lista;
end;

procedure OnPaintDoForm(form:tform = nil);
begin
end;

function CarregaHint(Form:Tform):variant;
begin
end;

procedure FORM_desalocarForm(var lista:RefForm;form:tform);
begin
end;

procedure OnKeyPressDoForm(Sender:TObject; Var Key:char);
begin
end;

procedure RetornaDiretorio(Sender:TObject);
var
   Dir:String;
begin
     Application.ProcessMessages;
     Dir:='';
     if SelectDirectory('Diret�rios','',Dir)
        then begin
               if Sender is TDBEdit
                  then begin
                        TDBEdit(Sender).Field.AsString:=Dir;
                        TDBEdit(Sender).SetFocus;
                       end;

               if Sender is TEdit
                  then begin
                        TEdit(Sender).Text:=Dir;
                        TEdit(Sender).SetFocus;
                       end;
              end;
     SetForegroundWindow(Application.Handle); //tenta evitar que a applica��o perca o foco quando o usu�rio escolher o diret�rio.
end;

procedure OnShowDoForm(Sender:TObject; Ds: TDataSource);  //Abre o Form com a Tabela Vazia, colocando o nome do Usu�rio logado na Barra de�T�tulos
var
   i: byte;
begin
     CarregaHint(TForm(sender));
     //TForm(sender).left:=0;
     //TForm(sender).Top:=0;
     if Ds <> Nil
        then
            begin
                 with TFDQuery(Ds.DataSet) do
                 begin
                      if Active then Close;
                      i := 0;
                      while i<Params.Count do
                      begin
                           Params[i].Value := 0;
                           inc(i);
                      end;
                      Open;
                 end;
            end;
    if Pos(' - [',(Sender as TForm).Caption)<=0
       then
           (Sender as TForm).Caption := (Sender as TForm).Caption + ' - ['+AnsiUpperCase(UsuarioLogado)+'] - ['+EncontraValor('select parfan from par where parcod =:0',EmpresaLogada)+']';

    if (((Sender as TForm).FindComponent('StatusBar1')) as TStatusBar) <> nil
       then
           (((Sender as TForm).FindComponent('StatusBar1')) as TStatusBar).Panels[0].Text:=EmptyStr;
end;

procedure OnKeyPressDosNumeros(Var Key:Char);  //Aceita somente N�meros
begin
     if not(Key in ['0'..'9',#8,#13,','])
        then
            Key :=#0;
end;

procedure OnDBGridEnterLook(Campo:TField);  //Alimenta vari�vel ConteudoDBEditAntLook
begin
     if NaoPassouOnEnter // Evita que a vari�vel ConteudoDBEditAntLook receba valores �nv�lidos
        then
            begin
                 ConteudoDBEditAntLook := Campo.AsString;
                 NaoPassouOnEnter := false;
            end;
end;

function OnDBGridExitLook(DBGridL:TDBGrid;Descricoes:array of TField;TextoSql,TextoSqlL:WideString;DispEsqDir:Boolean):Boolean;
var
   i: byte;
   Des:Variant;
begin
     if Not(DBGridL.SelectedField.IsNull)
        then
            begin
                 Result:=true;
                 Des:=EncontraValor(TextoSql,DBGridL.SelectedField.Value);
                 Des:=ConverterValoresString(Des);
                 if VarIsArray(Des)
                   then
                       begin
                            //Tratamento usado p/ abrir o FormL quando a vari�vel Des trazer
                            //valor num�rico vazio, porque a Sql n�o encontrou o valor
                            if (Des[0]=EmptyStr)or(Des[0]='0')or(Des[0]='0,00')or(Des[0]='00:00')or(Des[0]='01/01/1500 00:00:00')
                               then
                                   begin
                                        Result:=OnDBGridLook(DBGridL,Descricoes,TextoSqlL,false,DispEsqDir);
                                        if Not(Result) then Abort; //N�o deixar a coluna do DBGrid perder o foco.
                                   end
                               else
                                   begin
                                        if Not(DBGridL.DataSource.State in [dsEdit,dsInsert])
                                           then DBGridL.DataSource.Edit;
                                        for i:=0 to High(Descricoes) do
                                            Descricoes[i].AsString := Des[i];
                                        NaoPassouOnEnter := true;  //flag para executar o evento OnEnterLookUp
                                   end;
                       end
                   else
                       begin
                            if (Des=EmptyStr)or(Des='0')or(Des='0,00')or(Des='01/01/1500')or(Des='00:00')or(Des='01/01/1500 00:00')
                               then
                                   begin
                                        Result:=OnDBGridLook(DBGridL,Descricoes,TextoSqlL,false,DispEsqDir);
                                        if Not(Result) then Abort; //N�o deixar a coluna do DBGrid perder o foco.
                                   end
                               else
                                   begin
                                        if Not(DBGridL.DataSource.State in [dsEdit,dsInsert])
                                           then DBGridL.DataSource.Edit;
                                        if Descricoes[0]<>Nil
                                           then
                                               Descricoes[0].AsString:=Des;
                                        NaoPassouOnEnter := true;  //flag para executar o evento OnEnterLookUp
                                   end;
                       end;
            end
        else
            begin
                 Result:=false;
                 if Not(DBGridL.DataSource.State in [dsEdit,dsInsert])
                    then DBGridL.DataSource.Edit;
                 if Descricoes[0]<>Nil
                    then
                        for i:=0 to High(Descricoes) do
                        Descricoes[i].AsString := EmptyStr;
                 NaoPassouOnEnter := true;
            end;
end;

function ConverterValoresString(Dados:variant):variant;
var i:integer;
    aux:variant;
begin
    if VarIsArray(Dados)
       then
           begin
                i:=0;
                aux:=VarArrayCreate([0,VarArrayHighBound(Dados,1)],varVariant);
                while i<=VarArrayHighBound(Dados,1) do
                begin
                    aux[i]:=FormataValorEmString(Dados[i]);
                    inc(i);
                end;
           end
       else
           aux:=FormataValorEmString(Dados);
    Result:=aux;
end;

function OnDBGridLook(DBGridL:TDBGrid;Descricoes:array of TField;TextoSqlL:WideString;Proximo:Boolean;DispEsqDir:Boolean):Boolean;
var
   i: byte;
   Anterior: String;
   w,larg: Integer;
begin
     try
        if Not(DBGridL.ReadOnly)
           then
               begin
                    Result:=true;
                    Application.CreateForm(TFormL,FormL);
                    with TFDQuery(FormL.DsLoc.DataSet) do
                    begin
                         Close;
                         Sql.Text:=TextoSqlL;
                         Params[0].Value:='%';
                         Open;
                         if IsEmpty
                            then
                                begin
                                     Mensagem('Listagem vazia!',Informacao);
                                     DBGridL.SelectedField.Clear;
                                     if Descricoes[0]<>Nil
                                        then
                                            for i:=0 to High(Descricoes) do
                                            Descricoes[i].Clear;
                                     Result:=false;
                                     Exit;
                                end
                            else
                                begin
                                     w:=0;
                                     for i := 0 to FieldCount - 1 do
                                     begin
                                          FormL.DBGridB.Columns.Add.FieldName := Fields[i].FieldName;
                                          FormL.DBGridB.Columns[i].Title.Caption := BuscaDisplayLabel(DBGridL.DataSource,Fields[i].FieldName);

                                          larg:=BuscaDisplayWidth(DBGridL.DataSource,Fields[i].FieldName);
                                          if larg >0
                                             then
                                                 FormL.DBGridB.Columns[i].Width:=larg * 7  //7 p/ transformar em pixels a medida, ou seja, n�o � a mesma do displaywidth do campo
                                             else
                                                 FormL.DBGridB.Columns[i].Width:=RetornaWidthDefault(Fields[i].DataType)*7;

                                          if (i = (FieldCount - 1)) and ((FormL.DBGridB.Width - 22) > (w+FormL.DBGridB.Columns[i].Width)) //22 � o tamanho da barra de rolagem.
                                             then
                                                FormL.DBGridB.Columns[i].Width := (FormL.DBGridB.Width - 22)- w;

                                          w:=w+FormL.DBGridB.Columns[i].Width;
                                     end;

                                     FormL.ShowModal;
                                     if (Active)
                                        then     //Usu�rio pressionou tecla ENTER no FormL
                                            begin
                                                 Anterior:=DBGridL.SelectedField.AsString;

                                                 if Not(DBGridL.DataSource.State in [dsEdit,dsInsert])
                                                    then
                                                        DBGridL.DataSource.DataSet.Edit;

                                                 if DispEsqDir //Posicao dos campos Codigo e Descricao no FormL
                                                    then
                                                        begin
                                                             if DBGridL.SelectedField <> nil
                                                                then
                                                                    DBGridL.SelectedField.Value := Fields[0].Value;
                                                             if Descricoes[0] <> nil
                                                                then
                                                                    for i:=0 to High(Descricoes) do
                                                                    begin
                                                                        Descricoes[i].Value:=Fields[i + 1].Value;
                                                                    end;
                                                        end
                                                    else
                                                        begin
                                                             if DBGridL.SelectedField <> nil
                                                                then
                                                                    DBGridL.SelectedField.Value := Fields[FieldCount-2].Value;
                                                             if Descricoes[0] <> nil
                                                                then
                                                                    for i:=0 to High(Descricoes) do
                                                                        Descricoes[i].Value := Fields[FieldCount-1].Value;
                                                        end;

                                                 if Anterior <> DBGridL.SelectedField.AsString
                                                    then
                                                        Result:=true
                                                    else
                                                        Result:=false;

                                                 NaoPassouOnEnter := true;  //Permite executar o evento OnEnterLookUp

                                                 //Quando � permitido avan�ar depois de uma confirma��o do Lookup.
                                                 if Proximo
                                                    then
                                                        OnKeyPressDoDBGridLook(DBGridL,#13); //Focar a pr�xima coluna.
                                            end
                                        else
                                            //Usu�rio pressionou tecla ESC no FormL
                                            begin
                                                 Result:=false;
                                            end;
                                end;
                    end;
               end;
     finally
       BotaoAtivo:=false;
       if FormL <> nil
          then
              begin
                   FormL.Release;
                   FormL:=nil
              end;
     end;
end;

function FormataValorEmString(valor:Variant):String;
begin
     //Mensagem(IntToStr(VarType(valor)),informacao);
     Result:=EmptyStr;
     Case VarType(valor) of
       varNull: Result:=EmptyStr;
       varEmpty,varOleStr,varStrArg,varString,varUString : Result:=valor;
       varSmallint,varInteger,varShortInt,varByte,
       varWord,varLongWord,varInt64                   : Result:=FormatFloat('#0',valor);
       varSingle,varDouble,varCurrency                : Result:=FormatFloat('#,##0.00',valor);
       varDate                                        : if Not(ValorNull(valor))
                                                           then Result:=DateTimeToStr(valor)
                                                           else Result:=FormatDateTime('dd/mm/yyyy hh:mm:ss',valor);
       varBoolean                                     : if Valor then Result:='Verdadeiro' else Result:='Falso';
     end;
end;

procedure OnShowDoFormL(DBGridB:TDBGrid);
begin
     if (ColAntDBGridBL >= DBGridB.Columns.Count)
        then
            ColAntDBGridBL := 0;
     DBGridB.OnTitleClick(DBGridB.Columns[ColAntDBGridBL]);
end;

function BuscaDisplayLabel(Ds:TDataSource;campo: string):string;
var CampoTField : TField;
    a : char;
begin
     if Ds<>Nil
        then
            CampoTField := Ds.DataSet.FindField(campo)
        else
            CampoTField := Nil;
     if CampoTField <> Nil
        then
            result := CampoTField.DisplayLabel
        else
            begin
                 a:=campo[1];
                 delete(campo,1,1);
                 Insert(uppercase(a),campo,1);
                 campo:=StringReplace(campo,'_',' ',[rfReplaceAll]);
                 result := campo;
            end;
end;

function BuscaDisplayWidth(Ds:TDataSource;campo: string):Integer;
var
   CampoTField : TField;
begin
     if Ds<>Nil
        then
            CampoTField := Ds.DataSet.FindField(campo)
        else
            CampoTField := Nil;
     if CampoTField <> Nil
        then
            result := CampoTField.DisplayWidth
        else
            Result := 0;
end;

function RetornaWidthDefault(TipoDoCampo:TFieldType):Integer;
begin
    Case TipoDoCampo of
         ftString,ftBlob,ftMemo,ftFmtMemo,ftFixedChar,ftWideString : Result:= 35;
         ftSmallint,ftInteger,ftWord,ftFloat,ftCurrency,ftLargeInt,
         ftBytes,ftAutoInc                                         : Result:= 15;
         ftBoolean                                                 : Result:= 5;
         ftDate                                                    : Result:= 15;
         ftTime                                                    : Result:= 8;
         ftDateTime                                                : Result:= 22;
    else
        Result:=10;
    end
end;

procedure OnKeyPressDoDBGridLook(DBGridL:TDBGrid;Key:Char); //Transfere o foco para a pr�xima coluna do DBGRID.

     function IndiceFoco:Integer;
     var i:integer;
     begin
         i:= DBGridL.SelectedIndex;
         while i < DBGridL.Columns.Count do
         begin
              i:=i+1;
              if (i)<DBGridL.Columns.Count
                 then
                     begin
                          if DBGridL.Fields[i] <> nil
                             then
                                 begin
                                      if Not(DBGridL.Fields[i].ReadOnly)
                                         then
                                             Break;
                                 end
                             else
                                 begin
                                      i := i - 1;
                                      Break;
                                 end
                     end
                 else
                     begin
                          i:=0;
                          if DBGridL.DataSource.State in [dsInsert,dsEdit]
                             then
                                 begin
                                      DBGridL.DataSource.DataSet.Post;
                                      DBGridL.DataSource.DataSet.Next;
                                 end;
                          Break;
                     end;
         end;
         if i<0 then i:=0;
         if DBGridL.Fields[i] <> nil
            then
                begin
                     if DBGridL.Fields[i].ReadOnly  //Encontrar a pr�xima coluna dispon�vel
                        then
                            begin
                                 while (i < DBGridL.Columns.Count) and (DBGridL.Fields[i].ReadOnly) do
                                 begin
                                      i:=i+1;
                                 end;
                            end;
                     if (i > DBGridL.Columns.Count) then i:=0;
                end;
         Result := i;
     end;

begin
     if (Key = #13) and (DBGridL.DataSource.DataSet.Active)
        then
            DBGridL.Fields[IndiceFoco].FocusControl;
end;

function ValorNull(valor:Variant):boolean; //Fun��o que retorna true se o valor passado coincide com o valor padrao nulo da fun��o encontravalor;
begin
     Result:=false;
     Case VarType(valor) of
       varNull,varEmpty                            : Result:=true;
       varOleStr,varStrArg,varString               : if valor = EmptyStr then Result:=true;
       varSmallint,varInteger,varShortInt,varByte,
       varWord,varLongWord,varInt64,varSingle,
       varDouble,varCurrency                       : if valor = 0 then Result:=true;
       varDate                                     : if (FormatDateTime('dd/mm/yyyy hh:mm:ss',valor) = '01/01/1500 00:00:00')or(FormatDateTime('dd/mm/yyyy hh:mm:ss',valor) = '30/12/1899 00:00:00')
                                                        then Result:=true;
     end;
end;

function NumeroSemMascara(Valor:String):double;
var i:integer;
    aux: String;
begin
    aux:=EmptyStr;
    i:=Length(Valor);
    while i>0 do
    begin
        if Valor[i] in ['0'..'9',',','-']
           then aux:=Valor[i]+aux;
        i:=i-1;
    end;
    if aux<>EmptyStr
       then
           Result:=StrToFloatDef(aux,0)
       else
           Result:=0;
end;


end.
