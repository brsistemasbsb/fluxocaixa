unit UnMens;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Buttons,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFormMens = class(TForm)
    ImgExc: TImage;
    ImgGrav: TImage;
    ImgAdv: TImage;
    ImgInfo: TImage;
    ImgConf: TImage;
    ImgResAEC: TImage;
    bbSim: TBitBtn;
    bbNao: TBitBtn;
    bbOk: TBitBtn;
    bbCancel: TBitBtn;
    Memo1: TMemo;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    botaoFoco:TBitBtn;

  protected
    procedure CreateParams(var Params: TCreateParams); override;
  end;

var
  FormMens: TFormMens;

implementation

{$R *.dfm}

procedure TFormMens.CreateParams(var Params: TCreateParams);
begin
     inherited CreateParams(Params);
     Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
end;

procedure TFormMens.FormCreate(Sender: TObject);
begin
     SetWindowLong(Application.Handle, GWL_EXSTYLE,GetWindowLong(Application.Handle,GWL_EXSTYLE) and WS_EX_APPWINDOW and WS_EX_TOOLWINDOW);
end;

procedure TFormMens.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if Key = #27
        then
            Close;
     if (Key = #13) and (ActiveControl=Memo1)
        then
            begin
                 if bbSim.Visible
                    then bbSim.Click
                    else
                        if bbOk.Visible
                           then bbOk.Click;
            end;

end;

procedure TFormMens.FormShow(Sender: TObject);
begin
     if Memo1.Lines.Count>7
        then
            Memo1.ScrollBars:=ssVertical
        else
            Memo1.ScrollBars:=ssNone;
     if botaoFoco <> nil
        then
            botaoFoco.SetFocus;
end;

procedure TFormMens.Timer1Timer(Sender: TObject);
begin
     FlashWindow(self.Handle,true);
end;

end.
