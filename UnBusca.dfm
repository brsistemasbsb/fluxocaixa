object FormBusca: TFormBusca
  Left = 0
  Top = 0
  ClientHeight = 484
  ClientWidth = 774
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 774
    Height = 463
    Align = alClient
    Caption = 'Selecione a coluna na qual deseja buscar'
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 2
      Top = 15
      Width = 770
      Height = 446
      Align = alClient
      DataSource = DM.DsBusca
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'cod'
          Title.Caption = 'C'#243'd'
          Width = 49
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'des'
          Title.Caption = '...'
          Width = 686
          Visible = True
        end>
    end
  end
  object EditBusca: TEdit
    Left = 0
    Top = 463
    Width = 774
    Height = 21
    Align = alBottom
    Color = clScrollBar
    ReadOnly = True
    TabOrder = 1
  end
end
