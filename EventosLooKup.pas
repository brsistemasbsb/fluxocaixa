// 15.05.05.01 - corre��o nos eventos
// 15.06.03.01 - corre��o nos eventos
// 15.10.16.01 - corre��o nos eventos
// 15.12.09.01 - corre��o nos eventos
// 16.03.02.01 - 03/02/2016 - jonathan - corrigindo criador de sql

unit EventosLooKup;

interface

uses SysUtils,DateUtils,variants,Eventos,Controls, forms,shellapi, windows
     ,classes, DB, StdCtrls, DBGrids, Dialogs, Buttons , DBCtrls, Messages, Graphics,
     FireDAC.Comp.Client;

type

  TELookUpSQL = function (Objeto:TObject;Evento: byte): WideString of object;
  TCriadorSQL = class
  private
         fTabela:string;
         fCampoCod:string;
         fCampoDes:String;
         fSQL1:string;
         fSQL2:string;
         procedure criarSQLs;
  public
        constructor Create(p_Tabela:string;p_CampoCod:string;p_CampoDes:String);overload;
        constructor Create(p_sql:string);overload;
        function ELookUP(Objeto:TObject; Evento: byte): WideString;
  end;

  TLookupDestaque = class
  private
         fCampo:string;
         fCondicaoCampo:String;
         fCorDeDestaque:TColor;
  end;
  TCriadorLookup = class
  private
         fEditCodigo:TObject;
         EditDes:array of TEdit;
         EditDesAux:array of TEdit;
         fbotaoBusca:TBitBtn;
         fDataSet:TDataSet;
         fSQL:TELookUpSQL;
         fSomenteNumeros:boolean;
         fPermiteMultiSelecao:boolean;
         fBuscaUnica:boolean; // no on exit ele sempre procura apenas um valor
         fDestaque:TLookupDestaque;
         ConteudoDBEditAntLook:string;
         //fSQLVariosRegistros:WideString;

         fOnEnterAtual    : TnotifyEvent;
         fOnChangeAtual   : TnotifyEvent;
         fOnExitAtual     : TnotifyEvent;
         fOnKeyPressAtual : TKeyPressEvent;
         fOnClickAtual    : TnotifyEvent;
         procedure fOnEnterEditCod(Sender: TObject);
         procedure fOnChangeEditCod(Sender: TObject);
         procedure fOnExitEditCod(Sender: TObject);
         procedure fOnKeyPressEditCod(Sender: TObject; var Key: Char);
         procedure fOnClickBotao(Sender: TObject);

         procedure carregaEventosEditCodigo;
         procedure carregaEventosBotao;
         procedure setEditDes(const p_editdes:array of TEdit);


         property DataSet :TDataSet read fDataSet write fDataSet;
         property SQL :TELookUpSQL read fSQL write fSQL;
         function getEditCodigoTexto:string;
         function OnClickDoBotaoLookFiltrado(Codigo:array of TObject;EditDes:array of TEdit;TextoSqlL:String):Boolean;

  public

        property BotaoBusca :TBitBtn read fbotaoBusca write fbotaoBusca;
        property EditCodigo:TObject read fEditCodigo write fEditCodigo;
        property EditCodigoTexto:String read getEditCodigoTexto;
        procedure setEditDesAux(const p_editdesaux:array of TEdit);
        constructor Create(
                    p_EditCodigo:TObject;
                    p_EditDes:array of TEdit;
                    p_botaoBusca:TBitBtn;
                    p_SQL:TELookUpSQL);reintroduce;overload;

        constructor Create(
                    p_EditCodigo:TObject;
                    p_EditDes:array of TEdit;
                    p_botaoBusca:TBitBtn;
                    p_dataSet:TDataSet;
                    p_SQL:TELookUpSQL);overload;

        function criarLookUp:TCriadorLookup;
        function selecionar(Sender: TObject):boolean;
        property PermiteSomenteNumeros:boolean read fSomenteNumeros write fSomenteNumeros;
        property BuscaUnica : Boolean read fBuscaUnica write fBuscaUnica;
        property PermiteMultiSelecao : Boolean read fPermiteMultiSelecao write fPermiteMultiSelecao;
        procedure setDestaque(p_Campo,p_CondicaoCampo:String;p_corDeDestaque:TColor);
        function obtemValorVarios:string;
        function houveAlteracao:boolean;
  end;


  procedure OnEnterLookUp(Sender:TObject;EditDes:array of TEdit);
  procedure TratarConteudodoEditDes(EditDes:array of TEdit;acao:char);
  procedure OnChangeLookUp(Sender:TObject;EditDes:array of TEdit);
  procedure OnKeyPressDoDBEditLook(Sender:TObject;Var Key:Char;DataSet:TDataSet;bb:TBitBtn);
  function OnClickDoBotaoLook(Ds:TDataSource;Codigo:array of TObject;EditDes:array of TEdit;TextoSqlL:String;Proximo:Boolean;DispEsqDir:Boolean; limitar:boolean = True):Boolean;

var
    ConteudoEditDes : array of String;  //Tratando EditsDes ao mesmo tempo

implementation

uses UnL, Math, StrUtils, EventosString, EventosNumero;

procedure OnKeyPressDoDBEditLook(Sender:TObject;Var Key:Char;DataSet:TDataSet;bb:TBitBtn);
begin
     if Key = #32  //barra de espa�os
        then
            begin
                 if (Sender is TDBEdit)
                    then
                        begin
                             if (Sender as TDBEdit).ReadOnly = false
                                then
                                    if (DataSet.State in [dsInsert,dsEdit])
                                       then
                                           bb.OnClick(bb);
                        end
                    else
                        if (Sender as TEdit).ReadOnly = false
                           then
                               bb.OnClick(bb);
                 key := #0;//Nao adicionar espaco no campo quando tipo string e quando se tecla enter ou esc no Forml
            end;
end;

procedure OnChangeLookUp(Sender:TObject;EditDes:array of TEdit);

  procedure ApagaDes;
  var
     i:byte;
  begin
       if EditDes[0].Text <> EmptyStr
          then
              for i:=0 to high(EditDes) do
              begin
                   EditDes[i].Text := EmptyStr;
              end;
  end;

begin
     if (Sender is TDBEdit)
        then
            begin
                 //mensagem(ValorSemZerosAEsq(TDBEdit(Sender).DataSource.DataSet.FieldByName(TDBEdit(Sender).DataField).AsString)+#13#10+
                 //ValorSemZerosAEsq(TDBEdit(Sender).Text),Informacao);
                 if ValorSemZerosAEsq(TDBEdit(Sender).DataSource.DataSet.FieldByName(TDBEdit(Sender).DataField).AsString) <> ValorSemZerosAEsq(TDBEdit(Sender).Text)
                    then
                        ApagaDes;
            end
        else
            ApagaDes;
end;

procedure TratarConteudodoEditDes(EditDes:array of TEdit;acao:char);

   procedure LimparConteudo;
   var i:integer;
   begin
        for i:=0 to High (EditDes) do
            ConteudoEditDes[i]:=EmptyStr
   end;

   procedure ReceberConteudo;
   var i:integer;
   begin
        for i:=0 to High (EditDes) do
            ConteudoEditDes[i]:=EditDes[i].Text;
   end;

   procedure PassarConteudo;
   var i:integer;
   begin
        for i:=0 to High (EditDes) do
            EditDes[i].Text:=ConteudoEditDes[i];
   end;

begin
     if ConteudoDBEditAntLook = EmptyStr
        then
            acao:='l';

   //Criando o array caso n�o exista ou esteja com tamanhos diferentes.
   if (ConteudoEditDes = nil) or (High(ConteudoEditDes)<>High(EditDes) )
      then
         SetLength(ConteudoEditDes,High(EditDes)+1);

   case acao of
   'l':LimparConteudo;
   'r':ReceberConteudo;
   'p':begin
          PassarConteudo;
          ConteudoEditDes:=nil;
       end;
   end;
end;


procedure OnEnterLookUp(Sender:TObject;EditDes:array of TEdit);  //Guarda os valores dos lookups (c�digo e descri��o) para ecomizar consulta no banco de dados
begin
     OnExitDoDBEdit(Sender);
     if NaoPassouOnEnter // Evitar que o evento OnEnterLookUp seja executado mais de uma vez para o mesmo objeto, evitando que a vari�vel ConteudoDBEditAntLook receba valores �nv�lidos
        then
            begin
                 ConteudoDBEditAntLook := ValorSemZerosAEsq((Sender as TCustomEdit).Text); //codigo
                 if EditDes[0] <> nil
                    then
                        begin
                             if ConteudoDBEditAntLook = EmptyStr
                                then
                                    TratarConteudodoEditDes(EditDes,'l')
                                else
                                    TratarConteudodoEditDes(EditDes,'r') //recebe descri��o (�es) dos EditsDes
                        end;
                 NaoPassouOnEnter := false;
            end;
end;


//// public

constructor TCriadorLookup.Create(
                    p_EditCodigo:TObject;
                    p_EditDes:array of TEdit;
                    p_botaoBusca:TBitBtn;
                    p_dataSet:TDataSet;
                    p_SQL:TELookUpSQL);
begin
     self.EditCodigo := p_EditCodigo ;
     self.setEditDes(p_EditDes);
     self.BotaoBusca := p_botaoBusca;
     if (p_EditCodigo is TDBEdit)
        then
            self.DataSet    := (p_EditCodigo as TDBEdit).DataSource.DataSet;

     self.SQL        := p_SQL;
     self.PermiteSomenteNumeros := true;
     self.fBuscaUnica:= true;
     self.fPermiteMultiSelecao := false;
     carregaEventosEditCodigo;
     carregaEventosBotao;
end;


constructor TCriadorLookup.Create(
            p_EditCodigo:TObject;
            p_EditDes:array of TEdit;
            p_botaoBusca:TBitBtn;
            p_SQL:TELookUpSQL);
begin
     self.Create(p_EditCodigo,p_EditDes,p_botaoBusca,nil,p_SQL);
     //carregaEventosEditCodigo;
     //carregaEventosBotao;
end;

function TCriadorLookup.criarLookUp:TCriadorLookup;
begin
     result := self;
end;

procedure TCriadorLookup.setEditDesAux(const p_editdesaux:array of TEdit);
begin
     SetLength(EditDesAux, Length(p_editdesaux));
     Move(p_editdesaux[Low(p_editdesaux)], EditDesAux[Low(EditDesAux)], SizeOf(p_editdesaux));
end;

procedure TCriadorLookup.setEditDes(const p_editdes:array of TEdit);
begin
     SetLength(EditDes, Length(p_editdes));
     Move(p_editdes[Low(p_editdes)], EditDes[Low(EditDes)], SizeOf(p_editdes));
end;


//// private

function TCriadorLookup.getEditCodigoTexto:string;
var resultado : string;
begin
     resultado := EmptyStr;
     if (EditCodigo is TEdit)
        then
            resultado := (EditCodigo as TEdit).Text;

     if (EditCodigo is TDBEdit)
        then
            resultado := (EditCodigo as TDBEdit).Text;

     result := resultado;
end;

procedure TCriadorLookup.carregaEventosEditCodigo;
begin
     // on enter
     if (EditCodigo is TEdit)
        then
            begin
                 if assigned((EditCodigo as TEdit).OnEnter)
                    then
                        fOnEnterAtual := (EditCodigo as TEdit).OnEnter
                    else
                        (EditCodigo as TEdit).OnEnter := fOnEnterEditCod;

                 // on change
                 if assigned((EditCodigo as TEdit).OnChange)
                    then
                        fOnChangeAtual := (EditCodigo as TEdit).OnChange
                    else
                        (EditCodigo as TEdit).OnChange := fOnChangeEditCod;

                 // on exit
                 if assigned((EditCodigo as TEdit).OnExit)
                    then
                        fOnExitAtual := (EditCodigo as TEdit).OnExit
                    else
                        (EditCodigo as TEdit).OnExit := fOnExitEditCod;

                 // on keypress
                 if assigned((EditCodigo as TEdit).OnKeyPress)
                    then
                        fOnKeyPressAtual := (EditCodigo as TEdit).OnKeyPress
                    else
                        (EditCodigo as TEdit).OnKeyPress := fOnKeyPressEditCod;
            end
        else
            begin
                 if assigned((EditCodigo as TDBEdit).OnEnter)
                    then
                        fOnEnterAtual := (EditCodigo as TDBEdit).OnEnter
                    else
                        (EditCodigo as TDBEdit).OnEnter := fOnEnterEditCod;

                 // on change
                 if assigned((EditCodigo as TDBEdit).OnChange)
                    then
                        fOnChangeAtual := (EditCodigo as TDBEdit).OnChange
                    else
                        (EditCodigo as TDBEdit).OnChange := fOnChangeEditCod;

                 // on exit
                 if assigned((EditCodigo as TDBEdit).OnExit)
                    then
                        fOnExitAtual := (EditCodigo as TDBEdit).OnExit
                    else
                        (EditCodigo as TDBEdit).OnExit := fOnExitEditCod;

                 // on keypress
                 if assigned((EditCodigo as TDBEdit).OnKeyPress)
                    then
                        fOnKeyPressAtual := (EditCodigo as TDBEdit).OnKeyPress
                    else
                        (EditCodigo as TDBEdit).OnKeyPress := fOnKeyPressEditCod;
         end;

end;

procedure TCriadorLookup.carregaEventosBotao;
begin
     // on click
     if assigned(BotaoBusca.OnClick)
        then
            fOnClickAtual := BotaoBusca.OnClick
        else
            self.BotaoBusca.OnClick := fOnClickBotao;
end;

procedure TCriadorLookup.fOnEnterEditCod(Sender: TObject);
begin
     if assigned(fOnEnterAtual)
        then
            fOnEnterAtual(sender);
     OnEnterLookUp(sender,EditDes);
     OnEnterDoDBEdit(sender);
     if (EditCodigo is TEdit)
        then
            self.ConteudoDBEditAntLook := TEdit(EditCodigo).Text
        else
            self.ConteudoDBEditAntLook := TDBEdit(EditCodigo).text;
end;

procedure TCriadorLookup.fOnChangeEditCod(Sender: TObject);
begin
     if assigned(fOnChangeAtual)
        then
            fOnChangeAtual(sender);

     OnChangeLookUp(Sender,EditDes);
     self.ConteudoDBEditAntLook := EmptyStr;
     if length(EditDesAux) > 0
        then
            OnChangeLookUp(Sender,EditDesAux);
end;

procedure TCriadorLookup.fOnExitEditCod(Sender: TObject);
begin
     try
        OnExitDoDBEdit(sender);
        if TCustomEdit(Sender).Text <> EmptyStr
           then
               begin
                    // chama o onclick do botao ... mas passa o edit como parametro
                    fOnClickBotao(sender);
                    //selecionar(self.EditCodigo);
               end
     except
           on e:exception do
           begin
                if (EditCodigo is TEdit) then (EditCodigo as TEdit).Clear;
                if (EditCodigo is TDBEdit) then (EditCodigo as TDBEdit).Clear;
                raise;
           end;
     end
end;

procedure TCriadorLookup.fOnKeyPressEditCod(Sender: TObject; var Key: Char);
begin
     if assigned(fOnKeyPressAtual)
        then
            fOnKeyPressAtual(sender,key);
     OnKeyPressDoDBEditLook(Sender,Key,DataSet,BotaoBusca);
     if PermiteSomenteNumeros
        then
            OnKeyPressDosNumeros(Key);
end;

procedure TCriadorLookup.fOnClickBotao(Sender: TObject);
begin
     if assigned(fOnClickAtual)
        then
            fOnClickAtual(Sender)
        else
            selecionar(Sender);
end;

function TCriadorLookup.selecionar(Sender: TObject):boolean;

        function OnClickDoBotaoLook2(Ds:TDataSource;Codigo:array of TObject;EditDes:array of TEdit;TextoSqlL:String;Proximo:Boolean;DispEsqDir:Boolean; limitar:boolean = True):Boolean;
        var
           i: byte;
           Anterior: array of String;
           w,larg: Integer;
        begin
             try
               Result:=true;
               BotaoAtivo:=true;
               {if elookup_ultima_sql = TextoSqlL
                  then
                      begin
                      end;}
               Application.CreateForm(TFormL,FormL);

               if self.fDestaque <> nil
                  then
                      begin
                           FormL.campo          := self.fDestaque.fCampo;
                           FormL.condicaoCampo  := self.fDestaque.fCondicaoCampo;
                           FormL.corDeDestaque  := self.fDestaque.fCorDeDestaque;
                           FormL.usaDestaque    := true;
                      end;

               if TextoSqlL[Length(TextoSqlL)]=';' then Delete(TextoSqlL,Length(TextoSqlL),1);
               with TFDQuery(FormL.DsLoc.DataSet) do
               begin
                    Close;
                    SQL.Clear;
                    if (Not(Limitar)) or (Pos(' limit ',TextoSqlL)>0)
                       then
                           Sql.Text:=TextoSqlL
                       else
                           Sql.Text:=TextoSqlL+' limit 50;';
                    Params[0].Value:='%';
                    //Mensagem(sql.Text,Informacao);
                    sql_openQuery(forml.queryloc);
                    if IsEmpty
                       then                   begin
                                Mensagem('Listagem vazia!',Informacao);
                                if (Codigo[0] is TEdit)
                                   then
                                       begin
                                            for i:=0 to High(Codigo) do
                                                TEdit(Codigo[i]).Text:=EmptyStr;
                                            for i:=0 to High(EditDes) do
                                                TEdit(EditDes[i]).Text:=EmptyStr;
                                            if TEdit(Codigo[0]).Visible
                                              then
                                                  TEdit(Codigo[0]).SetFocus;
                                       end
                                   else
                                       begin
                                            if (Codigo[0] is TDBEDit)
                                               then
                                                   begin
                                                        for i:=0 to High(Codigo) do
                                                            TDBEdit(Codigo[i]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[i]).DataField).Value:=Null;
                                                        for i:=0 to High(EditDes) do
                                                            TEdit(EditDes[i]).Text:=EmptyStr;
                                                        TDBEdit(Codigo[0]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[0]).DataField).FocusControl;
                                                   end;
                                       end;
                                Result:=false;
                                Exit;
                           end
                       else
                           begin
                                w:=0;
                                for i := 0 to FieldCount - 1 do
                                begin
                                     FormL.DBGridB.Columns.Add.FieldName := Fields[i].FieldName;
                                     FormL.DBGridB.Columns[i].Title.Caption := BuscaDisplayLabel(Ds,Fields[i].FieldName);

                                     larg:=BuscaDisplayWidth(Ds,Fields[i].FieldName);
                                     if larg >0
                                        then
                                            FormL.DBGridB.Columns[i].Width:=larg * 7  //7 p/ transformar em pixels a medida, ou seja, n�o � a mesma do displaywidth do campo
                                        else
                                            if (Fields[i].size > 0) and (Fields[i].size < 30)
                                               then
                                                   FormL.DBGridB.Columns[i].Width:=Fields[i].size * 7
                                               else
                                                   FormL.DBGridB.Columns[i].Width:=RetornaWidthDefault(Fields[i].DataType)*7;

                                     if (i = (FieldCount - 1)) and ((FormL.DBGridB.Width - 22) > (w+FormL.DBGridB.Columns[i].Width)) //22 � o tamanho da barra de rolagem.
                                        then
                                           FormL.DBGridB.Columns[i].Width := (FormL.DBGridB.Width - 22)- w;

                                     w:=w+FormL.DBGridB.Columns[i].Width;
                                end;

                                FormL.ShowModal;
                                if (Active)
                                   then     //Usu�rio pressionou tecla ENTER no FormL
                                       begin
                                            SetLength(Anterior,High(Codigo)+1);
                                            for i:=0 to High(Codigo) do
                                                Anterior[i]:=TCustomEdit(Codigo[i]).Text;
                                            if DispEsqDir //Posicao dos campos Codigo e Descricao no FormL
                                               then
                                                   begin
                                                        if Codigo[0] <> nil
                                                           then
                                                               for i:=0 to High(Codigo) do
                                                               begin
                                                                    if (Codigo[i] is TDBEdit)
                                                                       then
                                                                           TDBEdit(Codigo[i]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[i]).DataField).AsString := FormataValorEmString(Fields[i].Value)
                                                                       else
                                                                           TEdit(Codigo[i]).Text := FormataValorEmString(Fields[i].Value);
                                                               end;
                                                        if EditDes[0] <> nil
                                                           then
                                                               for i:=0 to High(EditDes) do
                                                                   EditDes[i].Text:=FormataValorEmString(Fields[i + (High(Codigo)+1)].Value);
                                                   end
                                               else
                                                   begin
                                                        if Codigo[0] <> nil
                                                           then
                                                               for i:=0 to High(Codigo) do
                                                               begin
                                                                    if (Codigo[i] is TDBEdit)
                                                                       then
                                                                           TDBEdit(Codigo[i]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[i]).DataField).AsString := FormataValorEmString(Fields[FieldCount-High(Codigo)-2+i].Value)
                                                                       else
                                                                           TEdit(Codigo[i]).Text := FormataValorEmString(Fields[FieldCount-High(Codigo)-2+i].Value);
                                                               end;
                                                        if EditDes[0] <> nil
                                                           then
                                                               for i:=0 to High(EditDes) do
                                                                   EditDes[i].Text:=FormataValorEmString(Fields[i+FieldCount-High(EditDes)-1].Value);
                                                   end;
                                            for i:=0 to High(Codigo) do
                                            begin
                                                 if Anterior[i] <> TCustomEdit(Codigo[i]).Text
                                                    then
                                                        begin
                                                             Result:=true;
                                                             Break;
                                                        end
                                                    else
                                                        Result:=false;
                                            end;
                                            NaoPassouOnEnter := true;  //Permite executar o evento OnEnterLookUp

                                            //Quando � permitido avan�ar depois de uma confirma��o do Lookup.
                                            if Proximo
                                               then
                                                   begin
                                                        if TCustomEdit(Codigo[0]).Owner is TFrame
                                                           then
                                                               TForm(TFrame(TCustomEdit(Codigo[0]).Owner).Owner).Perform(WM_NEXTDLGCTL,0,0)
                                                           else
                                                               (TForm(TCustomEdit(Codigo[0]).Owner)).Perform(WM_NEXTDLGCTL,0,0);
                                                   end;
                                       end
                                   else
                                       //Usu�rio pressionou tecla ESC no FormL
                                       begin
                                            Result:=false;
                                            if TCustomEdit(Codigo[0]).Visible
                                               then
                                                   TCustomEdit(Codigo[0]).SetFocus;
                                       end;
                           end;
               end;
             finally
               BotaoAtivo:=false;
               FormL.Release;
               FormL:=nil
             end;
        end;


        function OnExitAntLook2(Ds:TDataSource;Codigo:TObject;Descricao:array of TEdit;TextoSql,TextoSqlL:WideString;bbLook:TBitBtn;DispEsqDir:Boolean;limitar:Boolean=true):Boolean;
        var
           i: byte;
           Des:Variant;
        begin
             if BotaoAtivo         //Devido a possibilidade de se teclar ENTER e chamar o evento
                then               //Onclick o Evento onexit e seus eventos dependentes ocorreria
                    Result:=false  //desnecessariamente j� que no bot�o eles tamb�m s�o chamados.
                else
                    if (TCustomEdit(Codigo).Text <> '')
                       then
                           begin
                                Result:=true;
                                //Des:=EncontraValor(TextoSql,TCustomEdit(Codigo).Text);
                                Des:=EncontraValor(TextoSql,StrToInt(TCustomEdit(Codigo).Text));
                                Des:=ConverterValoresString(Des);
                                if VarIsArray(Des)
                                   then
                                       begin
                                            //Tratamento usado p/ abrir o FormL quando a vari�vel Des trazer
                                            //valor num�rico vazio, porque a Sql n�o encontrou o valor
                                            if (Des[0]=EmptyStr)or(Des[0]='0')or(Des[0]='0,00')or(Des[0]='00:00')or(Des[0]='01/01/1500 00:00:00')
                                               then
                                                   begin
                                                        {if self.fDestaque <> nil
                                                           then
                                                               Result := OnClickDoBotaoLookDestaque(Ds,Codigo,Descricao,TextoSqlL,((TForm(TCustomEdit(Codigo).Owner).ActiveControl) = bbLook),DispEsqDir,fDestaque.fCampo,fDestaque.fCondicaoCampo,fDestaque.fCorDeDestaque,limitar)
                                                           else}
                                                               Result := OnClickDoBotaoLook(Ds,Codigo,Descricao,TextoSqlL,((TForm(TCustomEdit(Codigo).Owner).ActiveControl) = bbLook),DispEsqDir,limitar)
                                                   end
                                               else
                                                   begin
                                                        for i:=0 to High(Descricao) do
                                                            TCustomEdit(Descricao[i]).Text := Des[i];
                                                        NaoPassouOnEnter := true;  //flag para executar o evento OnEnterLookUp
                                                   end;
                                       end
                                   else
                                       begin
                                            //Tratamento usado p/ abrir o FormL quando a vari�vel Des trazer
                                            //valor num�rico 0, porque a Sql n�o encontrou o valor
                                            if (Des=EmptyStr)or(Des='0')or(Des='0,00')or(Des='01/01/1500')or(Des='00:00')or(Des='01/01/1500 00:00')
                                               then
                                                   Result:=OnClickDoBotaoLook2(Ds,Codigo,Descricao,TextoSqlL,((TForm(TCustomEdit(Codigo).Owner).ActiveControl) = bbLook),DispEsqDir,limitar)
                                               else
                                                   begin
                                                        Descricao[0].Text:=Des;
                                                        NaoPassouOnEnter := true;  //flag para executar o evento OnEnterLookUp
                                                   end;
                                       end;
                           end
                       else
                           begin
                                Result:=false;
                                for i:=0 to High(Descricao) do
                                    TCustomEdit(Descricao[i]).Text := EmptyStr;
                                NaoPassouOnEnter := true;
                           end;
        end;

begin
     result := false;
     // se o evento foi chamado pelo botao
     if (sender is TBitBtn)
        then
            begin
                 {if self.fPermiteMultiSelecao
                    then
                        result := OnClickDoBotaoLookAvancado(nil,[EditCodigo],EditDes,SQL(EditCodigo,2),true,true,false)
                    else }
                        begin
                             {if self.fDestaque <> nil
                                then
                                    Result := OnClickDoBotaoLookDestaque(nil,[EditCodigo],EditDes,SQL(EditCodigo,2),true,true,fDestaque.fCampo,fDestaque.fCondicaoCampo,fDestaque.fCorDeDestaque)
                                else}
                                    result := OnClickDoBotaoLook(nil,[EditCodigo],EditDes,SQL(EditCodigo,2),true,true);

                        end;
            end
        else
            begin
                 OnExitDoDBEdit(Sender);
                 if TCustomEdit(Sender).Text <> EmptyStr //(DataSet = nil) or ((DataSet <> nil) and (DataSet.State in [dsInsert,dsEdit]))
                    then
                        if (self.ConteudoDBEditAntLook <> ValorSemZerosAEsq(TCustomEdit(Sender).Text))
                           or ((length(EditDes) > 0) and (EditDes[0].Text = EmptyStr))
                           then
                               begin
                                    if fBuscaUnica
                                       then
                                           begin
                                                {
                                                if self.fPermiteMultiSelecao
                                                   then
                                                       result := OnExitAntLookAvancado(nil,Sender,EditDes,SQL(EditCodigo,1),SQL(EditCodigo,2),BotaoBusca,true,false)
                                                   else }
                                                       result := OnExitAntLook2(nil,Sender,EditDes,SQL(EditCodigo,1),SQL(EditCodigo,2),BotaoBusca,true)
                                           end
                                       else
                                           result := OnClickDoBotaoLookFiltrado(sender,EditDes,SQL(EditCodigo,2))
                               end
                           else
                               begin
                                    TratarConteudoDoEditDes(EditDes,'p');
                                    //TratarConteudoDoEditDes(EditDesAux,'p');
                                    NaoPassouOnEnter := true;
                               end
                    else
                        NaoPassouOnEnter:=true;
            end;
end;
function TCriadorLookup.houveAlteracao:boolean;
begin
     result := (self.ConteudoDBEditAntLook <> ValorSemZerosAEsq(TCustomEdit(fEditCodigo).Text));
end;

procedure TCriadorLookup.setDestaque(p_Campo,p_CondicaoCampo:String;p_corDeDestaque:TColor);
var l_destaque:TLookupDestaque;
begin
     l_destaque := TLookupDestaque.Create;
     l_destaque.fCampo := p_Campo;
     l_destaque.fCondicaoCampo := p_CondicaoCampo;
     l_destaque.fCorDeDestaque := p_corDeDestaque;
     self.fDestaque := l_destaque;
end;

function TCriadorLookup.obtemValorVarios:string;
var lResultado:String;
    lTextoEditCodigo:String;
begin
     if (EditCodigo is TEdit)
        then
            lTextoEditCodigo := TEdit(fEditCodigo).Text
        else
            lTextoEditCodigo := TDBEdit(fEditCodigo).Text;

     lResultado := IfThen(lTextoEditCodigo = 'V�RIOS',EditDes[0].Text,lTextoEditCodigo);
     if (lResultado <> EmptyStr)
        then
            begin
                 lResultado := StringReplace(lResultado,',',''',''',[rfReplaceAll]);
                 lResultado := ''''+lResultado+'''';
            end;
     result := lResultado;
end;

function TCriadorLookup.OnClickDoBotaoLookFiltrado(Codigo:array of TObject;EditDes:array of TEdit;TextoSqlL:String):Boolean;
var
   i: byte;
   Anterior: array of String;
   w,larg: Integer;
   DispEsqDir : boolean;
   valor_parametro:string;

     function retornaPrimeiroCampo:string;
     var posAux:integer;
         sqlaux:string;
     begin
          sqlaux := TextoSqlL;
          //  procurando o 'select'
          posAux:=pos('select',sqlaux);
          if posAux > 0
             then
                 begin
                      sqlaux := Trim(copy(sqlaux,posAux+6,Length(sqlaux)));
                      posAux := pos(',',sqlaux);
                      if posAux > 0
                         then
                             begin
                                  sqlaux := Trim(copy(sqlaux,0,posAux-1));
                                  posAux := pos(' as ',sqlaux);
                                  if posAux > 0
                                     then
                                         sqlaux := Trim(copy(sqlaux,0,posAux-1));

                                  result := sqlaux;
                             end;
                 end;
     end;
begin
     if (Codigo[0] is TEdit)
        then
            valor_parametro := TEdit(Codigo[0]).Text;

     if (Codigo[0] is TDBEdit)
        then
            valor_parametro := TDBEdit(Codigo[0]).Text;

     if (valor_parametro <> EmptyStr) and (not BotaoAtivo)
        then
            try

               begin
                    DispEsqDir := false;
                    Result:=true;
                    BotaoAtivo:=true;

                    application.CreateForm(TFormL,FormL);
                    if TextoSqlL[Length(TextoSqlL)]=';' then Delete(TextoSqlL,Length(TextoSqlL),1);
                    with TFDQuery(FormL.DsLoc.DataSet) do
                    begin
                        Close;
                        SQL.Clear;
                        if (Pos(' limit ',TextoSqlL)>0)
                           then
                               Sql.Text:=TextoSqlL
                           else
                               Sql.Text:=TextoSqlL+' limit 50;';

                        FormL.TrocaTextoDaSQL(retornaPrimeiroCampo);
            
                        Params[0].Value := valor_parametro;
                        //Mensagem(sql.Text,Informacao);
                        sql_openQuery(forml.queryloc);
                        if IsEmpty
                           then
                               begin
                                    close;
                                    Params[0].Value := '%';
                                    open;
                               end;
                               
                        if IsEmpty
                           then
                               begin
                                    Mensagem('Listagem vazia!',Informacao);
                                    if (Codigo[0] is TEdit)
                                       then
                                           begin
                                                for i:=0 to High(Codigo) do
                                                    TEdit(Codigo[i]).Text:=EmptyStr;
                                                for i:=0 to High(EditDes) do
                                                    TEdit(EditDes[i]).Text:=EmptyStr;
                                                if TEdit(Codigo[0]).Visible
                                                  then
                                                      TEdit(Codigo[0]).SetFocus;
                                           end
                                       else
                                           begin
                                                if (Codigo[0] is TDBEDit)
                                                   then
                                                       begin
                                                            for i:=0 to High(Codigo) do
                                                                TDBEdit(Codigo[i]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[i]).DataField).Value:=Null;
                                                            for i:=0 to High(EditDes) do
                                                                TEdit(EditDes[i]).Text:=EmptyStr;
                                                            TDBEdit(Codigo[0]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[0]).DataField).FocusControl;
                                                       end;
                                           end;
                                    Result:=false;
                                    Exit;
                               end
                           else
                               begin
                                    //mensagem(IntToStr(FormL.QueryLoc.RecordCount),Informacao);

                                    if FormL.QueryLoc.RecordCount > 1
                                       then
                                           begin
                                                {
                                                w:=0;
                                                for i := 0 to FieldCount - 1 do
                                                begin
                                                     FormL.DBGridB.Columns.Add.FieldName := Fields[i].FieldName;
                                                     FormL.DBGridB.Columns[i].Title.Caption := BuscaDisplayLabel(Ds,Fields[i].FieldName);

                                                     larg:=BuscaDisplayWidth(Ds,Fields[i].FieldName);
                                                     if larg >0
                                                        then
                                                            FormL.DBGridB.Columns[i].Width:=larg * 7  //7 p/ transformar em pixels a medida, ou seja, n�o � a mesma do displaywidth do campo
                                                        else
                                                            FormL.DBGridB.Columns[i].Width:=RetornaWidthDefault(Fields[i].DataType)*7;

                                                     if (i = (FieldCount - 1)) and ((FormL.DBGridB.Width - 22) > (w+FormL.DBGridB.Columns[i].Width)) //22 � o tamanho da barra de rolagem.
                                                        then
                                                           FormL.DBGridB.Columns[i].Width := (FormL.DBGridB.Width - 22)- w;

                                                     w:=w+FormL.DBGridB.Columns[i].Width;
                                                end;
                                                }
                                                FormL.buscaNovamenteOnShow := false;
                                                FormL.ShowModal;
                                           end;

                                    if (Active)
                                       then     //Usu�rio pressionou tecla ENTER no FormL
                                           begin

                                                SetLength(Anterior,High(Codigo)+1);
                                                for i:=0 to High(Codigo) do
                                                    Anterior[i]:=TCustomEdit(Codigo[i]).Text;

                                                if Codigo[0] <> nil
                                                   then
                                                       for i:=0 to High(Codigo) do
                                                       begin
                                                            if (Codigo[i] is TDBEdit)
                                                               then
                                                                   begin
                                                                        if TDBEdit(Codigo[i]).DataSource.DataSet.State in [dsinsert,dsedit]
                                                                           then
                                                                               TDBEdit(Codigo[i]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[i]).DataField).AsString := FormataValorEmString(Fields[i].Value)
                                                                   end
                                                               else
                                                                   TEdit(Codigo[i]).Text := FormataValorEmString(Fields[i].Value);
                                                       end;
                                                       
                                                if EditDes[0] <> nil
                                                   then
                                                       for i:=0 to High(EditDes) do
                                                           EditDes[i].Text:=FormataValorEmString(Fields[i + (High(Codigo)+1)].Value);
                                                NaoPassouOnEnter := true;  //Permite executar o evento OnEnterLookUp

                                                //Quando � permitido avan�ar depois de uma confirma��o do Lookup.
                                                //(TForm(TCustomEdit(Codigo[0]).Owner)).Perform(WM_NEXTDLGCTL,0,0);
                                           end
                                       else
                                           //Usu�rio pressionou tecla ESC no FormL
                                           begin
                                                Result:=false;
                                                if TCustomEdit(Codigo[0]).Visible
                                                   then
                                                       TCustomEdit(Codigo[0]).SetFocus;
                                           end;
                               end;
                        end;
                    end;
            finally
              BotaoAtivo:=false;
              FormL.Release;
              FormL:=nil
            end;
end;


constructor TCriadorSQL.Create(p_sql:string);
var campoCod:string;

     function retornaPrimeiroCampo:string;
     var posAux:integer;
         sqlaux:string;
     begin
          sqlaux := p_sql;
          //  procurando o 'select'
          posAux:=pos('SELECT',sqlaux);
          if posAux > 0
             then
                 begin
                      sqlaux := Trim(copy(sqlaux,posAux+6,Length(sqlaux)));
                      posAux := pos(',',sqlaux);
                      if posAux > 0
                         then
                             begin
                                  sqlaux := Trim(copy(sqlaux,0,posAux-1));
                                  posAux := pos(' AS ',sqlaux);
                                  if posAux > 0
                                     then
                                         sqlaux := Trim(copy(sqlaux,0,posAux-1));

                                  result := sqlaux;
                             end;
                 end;
     end;
     
     function removePrimeiroCampo:string;
     var posAux:integer;
         sqlaux:string;
         segundasql:string; // parte da sql depois do primeiro campo
     begin
          sqlaux := p_sql;

          //  procurando o 'select'
          posAux:=pos('SELECT',sqlaux);
          if posAux > 0
             then
                 begin
                      sqlaux := Trim(copy(sqlaux,posAux+6,Length(sqlaux)));
                      posAux := pos(',',sqlaux);
                      if posAux > 0
                         then
                             begin
                                  segundasql := Trim(copy(sqlaux,posAux+1,length(sqlaux)));
                                  result := 'SELECT '+segundasql;
                             end;
                 end;
     end;

     function adicionaCondicao(sqlaux,param:string):string;
     var posAux:integer;
         antes_where:string;
         depois_where:string;
     begin
          posAux := retornaPosUltimo('WHERE',sqlaux);
          if posAux > 0
             then
                 begin
                      antes_where := Trim(copy(sqlaux,0,posAux+5));
                      depois_where := Trim(copy(sqlaux,posAux+5,length(sqlaux)));
                 end
             else
                 begin
                      antes_where := sqlaux + ' WHERE ';
                      depois_where := '';
                 end;
          Result := antes_where + ' '+param+IfThen(length(depois_where) > 0,' and '+depois_where);
     end;



begin
     p_sql := UpperCase(p_sql);
     campoCod := retornaPrimeiroCampo;

     fSQL2 := p_sql;
     fSQL2 := adicionaCondicao(fSQL2,' cast('+campoCod + ' as varchar(20)) like :0 ')+' order by '+campoCod;

     // sql 1 sem o primeiro campo
     fSQL1 := removePrimeiroCampo;
     fSQL1 := adicionaCondicao(fSQL1,' '+campoCod + ' = :0');
     //criarSQLs;
end;

constructor TCriadorSQL.Create(p_Tabela:string;p_CampoCod:string;p_CampoDes:String);
begin
     self.fTabela   := p_Tabela;
     self.fCampoCod := p_CampoCod;
     self.fCampoDes := p_CampoDes;
     criarSQLs;
end;

function TCriadorSQL.ELookUP(Objeto:TObject; Evento: byte): WideString;
begin
     case Evento of
     1: Result := self.fSQL1;
     2: Result := self.fSQL2;
     end;
end;


procedure TCriadorSQL.criarSQLs;
var i:integer;
    l_campo_cod:string;
    pos_as:integer;
begin
     pos_as := pos(' as ',self.fCampoCod);
     l_campo_cod := IfThen(pos_as <= 0,self.fCampoCod,Trim(copy(self.fCampoCod,0,pos_as)));

     self.fSQL1 := EmptyStr;
     self.fSQL1 := self.fSQL1 + ' select '+self.fCampoDes;
     self.fSQL1 := self.fSQL1 + ' from '+self.fTabela+' ';
     self.fSQL1 := self.fSQL1 + ' where '+l_campo_cod+' = :0 ';


     self.fSQL2 := EmptyStr;
     self.fSQL2 := self.fSQL2 + 'select '+self.fCampoCod+IfThen(pos_as <= 0,' as Codigo')+
                                ','+self.fCampoDes+' ';
     self.fSQL2 := self.fSQL2 + ' from '+self.fTabela+' ';

     self.fSQL2 := self.fSQL2 + ' where cast('+l_campo_cod+' as varchar(20)) like :0 ';
     self.fSQL2 := self.fSQL2 + ' order by '+l_campo_cod;
end;

function OnClickDoBotaoLook(Ds:TDataSource;Codigo:array of TObject;EditDes:array of TEdit;TextoSqlL:String;Proximo:Boolean;DispEsqDir:Boolean; limitar:boolean = True):Boolean;
var
   i: byte;
   Anterior: array of String;
   w,larg: Integer;
   l_Key: Char;
begin
     try
       Result:=true;
       BotaoAtivo:=true;
       {if elookup_ultima_sql = TextoSqlL
          then
              begin
              end;}
       Application.CreateForm(TFormL,FormL);
       if TextoSqlL[Length(TextoSqlL)]=';' then Delete(TextoSqlL,Length(TextoSqlL),1);
       with TFDQuery(FormL.DsLoc.DataSet) do
       begin
            Close;
            SQL.Clear;
            if (Not(Limitar)) or (Pos(' limit ',TextoSqlL)>0)
               then
                   Sql.Text:=TextoSqlL
               else
                   Sql.Text:=TextoSqlL+' limit 50;';
            Params[0].Value:='%';
            //Mensagem(sql.Text,Informacao);
            sql_openQuery(forml.queryloc);
            if IsEmpty
               then                   begin
                        Mensagem('Listagem vazia!',Informacao);
                        if (Codigo[0] is TEdit)
                           then
                               begin
                                    for i:=0 to High(Codigo) do
                                        TEdit(Codigo[i]).Text:=EmptyStr;
                                    for i:=0 to High(EditDes) do
                                        TEdit(EditDes[i]).Text:=EmptyStr;
                                    if TEdit(Codigo[0]).Visible
                                      then
                                          TEdit(Codigo[0]).SetFocus;
                               end
                           else
                               begin
                                    if (Codigo[0] is TDBEDit)
                                       then
                                           begin
                                                for i:=0 to High(Codigo) do
                                                    TDBEdit(Codigo[i]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[i]).DataField).Value:=Null;
                                                for i:=0 to High(EditDes) do
                                                    TEdit(EditDes[i]).Text:=EmptyStr;
                                                TDBEdit(Codigo[0]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[0]).DataField).FocusControl;
                                           end;
                               end;
                        Result:=false;
                        Exit;
                   end
               else
                   begin
                        w:=0;
                        for i := 0 to FieldCount - 1 do
                        begin
                             FormL.DBGridB.Columns.Add.FieldName := Fields[i].FieldName;
                             FormL.DBGridB.Columns[i].Title.Caption := BuscaDisplayLabel(Ds,Fields[i].FieldName);

                             larg:=BuscaDisplayWidth(Ds,Fields[i].FieldName);
                             if larg >0
                                then
                                    FormL.DBGridB.Columns[i].Width:=larg * 7  //7 p/ transformar em pixels a medida, ou seja, n�o � a mesma do displaywidth do campo
                                else
                                    if (Fields[i].size > 0) and (Fields[i].size < 30)
                                       then
                                           FormL.DBGridB.Columns[i].Width:=Fields[i].size * 7
                                       else
                                           FormL.DBGridB.Columns[i].Width:=RetornaWidthDefault(Fields[i].DataType)*7;

                             if (i = (FieldCount - 1)) and ((FormL.DBGridB.Width - 22) > (w+FormL.DBGridB.Columns[i].Width)) //22 � o tamanho da barra de rolagem.
                                then
                                   FormL.DBGridB.Columns[i].Width := (FormL.DBGridB.Width - 22)- w;

                             w:=w+FormL.DBGridB.Columns[i].Width;
                        end;

                        FormL.ShowModal;
                        if (Active)
                           then     //Usu�rio pressionou tecla ENTER no FormL
                               begin
                                    SetLength(Anterior,High(Codigo)+1);
                                    for i:=0 to High(Codigo) do
                                        Anterior[i]:=TCustomEdit(Codigo[i]).Text;
                                    if DispEsqDir //Posicao dos campos Codigo e Descricao no FormL
                                       then
                                           begin
                                                if Codigo[0] <> nil
                                                   then
                                                       for i:=0 to High(Codigo) do
                                                       begin
                                                            if (Codigo[i] is TDBEdit)
                                                               then
                                                                   TDBEdit(Codigo[i]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[i]).DataField).AsString := FormataValorEmString(Fields[i].Value)
                                                               else
                                                                   TEdit(Codigo[i]).Text := FormataValorEmString(Fields[i].Value);
                                                       end;
                                                if EditDes[0] <> nil
                                                   then
                                                       for i:=0 to High(EditDes) do
                                                           EditDes[i].Text:=FormataValorEmString(Fields[i + (High(Codigo)+1)].Value);
                                           end
                                       else
                                           begin
                                                if Codigo[0] <> nil
                                                   then
                                                       for i:=0 to High(Codigo) do
                                                       begin
                                                            if (Codigo[i] is TDBEdit)
                                                               then
                                                                   TDBEdit(Codigo[i]).DataSource.DataSet.FieldByName(TDBEdit(Codigo[i]).DataField).AsString := FormataValorEmString(Fields[FieldCount-High(Codigo)-2+i].Value)
                                                               else
                                                                   TEdit(Codigo[i]).Text := FormataValorEmString(Fields[FieldCount-High(Codigo)-2+i].Value);
                                                       end;
                                                if EditDes[0] <> nil
                                                   then
                                                       for i:=0 to High(EditDes) do
                                                           EditDes[i].Text:=FormataValorEmString(Fields[i+FieldCount-High(EditDes)-1].Value);
                                           end;
                                    for i:=0 to High(Codigo) do
                                    begin
                                         if Anterior[i] <> TCustomEdit(Codigo[i]).Text
                                            then
                                                begin
                                                     Result:=true;
                                                     Break;
                                                end
                                            //else
                                              //  Result:=false;
                                    end;
                                    NaoPassouOnEnter := true;  //Permite executar o evento OnEnterLookUp

                                    l_Key:= #32;
                                    //Quando � permitido avan�ar depois de uma confirma��o do Lookup.
                                    if Proximo
                                       then
                                           begin
                                                if TCustomEdit(Codigo[0]).Owner is TFrame
                                                   then
                                                       TForm(TFrame(TCustomEdit(Codigo[0]).Owner).Owner).Perform(WM_NEXTDLGCTL,0,0)
                                                   else
                                                       (TForm(TCustomEdit(Codigo[0]).Owner)).Perform(WM_NEXTDLGCTL,0,0);
                                           end;
                                           //(TForm(TCustomEdit(Codigo[0]).Owner)).OnKeyPress(TForm(TCustomEdit(Codigo[0]).Owner),l_Key);
                                           //FormPreco.Perform(WM_NEXTDLGCTL,0,0);
                                           //(TForm(TCustomEdit(Codigo[0]).Owner)).Perform(WM_NEXTDLGCTL,0,0);

                               end
                           else
                               //Usu�rio pressionou tecla ESC no FormL
                               begin
                                    Result:=false;
                                    if TCustomEdit(Codigo[0]).Visible
                                       then
                                           TCustomEdit(Codigo[0]).SetFocus;
                               end;
                   end;
       end;
     finally
       BotaoAtivo:=false;
       FormL.Release;
       FormL:=nil
     end;
end;

end.
