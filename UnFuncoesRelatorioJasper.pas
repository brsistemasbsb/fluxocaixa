unit UnFuncoesRelatorioJasper;

interface

uses classes, Forms,windows, ShDocvW,pcnConversao, UnRelatorioJasper;

function StrToFormatoSaida(var ok: boolean; const s: string): TFormatoSaida;
function TFormatoSaidaToStr(const t: TFormatoSaida): string;

implementation

uses SysUtils, Eventos, EventosJasper;

function StrToFormatoSaida(var ok: boolean; const s: string): TFormatoSaida;
begin
  result := StrToEnumerado(ok, s, ['VIS','PDF','XLS','HTML'],
   [
    tsVisualizacao
    ,tsPDF
    ,tsXLS
    ,tsHTML
   ]);
end;

function TFormatoSaidaToStr(const t: TFormatoSaida): string;
begin
  result := EnumeradoToStr(t, ['VIS','PDF','XLS','HTML'],
                              [tsVisualizacao,tsPDF,tsXLS,tsHTML]);
end;

end.

