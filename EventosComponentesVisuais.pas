// 15.05.07.01 - 07/05/2015 - jonathan - op��o de somente numeros
// 15.07.20.01 - 20/07/2015 - jonathan - op��o de somente letras

unit EventosComponentesVisuais;

interface

uses SysUtils,DateUtils,variants,Eventos,Controls, forms,shellapi, windows
     ,classes, DB, StdCtrls, DBGrids, Dialogs, Buttons , DBCtrls, ComCtrls, Graphics
     ,Mask;

procedure mudaEstadoComponentes(controls:Array of TControl;habilitado:boolean;mudaDeCor:boolean;corHabilitado:TColor);overload;
procedure mudaEstadoComponentes(controls:Array of TControl;habilitado:boolean;corHabilitado:TColor);overload;
procedure mudaEstadoComponentes(controls:Array of TControl;habilitado:boolean);overload;

type
  TCompCtrl = class
  private
         fComponente:TObject;
         fComoMoeda:boolean;
         fComoNumero:boolean;
         fComoTexto:boolean;
         fComoLetras:boolean;
         procedure fOnEnter(Sender: TObject);
         procedure fOnExit(Sender: TObject);
         procedure fOnKeyPress(Sender: TObject; var Key: Char);
         function carregaOnEnter:TCompCtrl;
         function carregaOnExit:TCompCtrl;
         function carregaOnKeyPress:TCompCtrl;
  public
        constructor Create(p_Componente:TObject);
        function agirComoMoeda:TCompCtrl;
        function  agirComoNumero:TCompCtrl;
        function  agirComoTexto:TCompCtrl;
        function agirComoLetras:TCompCtrl;

  end;



implementation

uses Masks, EventosKey;

//// public
constructor TCompCtrl.Create(p_Componente:TObject);
begin
     self.fComponente := p_Componente;
     self.fComoMoeda  := false;
     self.fComoNumero := false;
     self.fComoTexto  := false;
     self.fComoLetras := false;
     self.carregaOnEnter;
     self.carregaOnExit;
     //self.carregaOnKeyPress;
end;

function  TCompCtrl.agirComoMoeda:TCompCtrl;
begin
     self.carregaOnKeyPress;
     self.fComoMoeda := true;
     result := self;
end;

function  TCompCtrl.agirComoNumero:TCompCtrl;
begin
     self.carregaOnKeyPress;
     self.fComoNumero := true;
     result := self;
end;

function  TCompCtrl.agirComoTexto:TCompCtrl;
begin
     self.carregaOnKeyPress;
     self.fComoTexto := true;
     result := self;
end;

function TCompCtrl.agirComoLetras:TCompCtrl;
begin
     self.carregaOnKeyPress;
     self.fComoLetras := true;
     result := self;
end;


// private

//// private

procedure TCompCtrl.fOnEnter(Sender: TObject);
begin
     OnEnterDoDBEdit(sender);
end;

procedure TCompCtrl.fOnExit(Sender: TObject);
begin
     OnExitDoDBEdit(sender);
end;

procedure TCompCtrl.fOnKeyPress(Sender: TObject; var Key: Char);
begin
     if fComoMoeda
        then
            begin
                 if (fComponente is TEdit)
                    then
                        OnKeyPressDoEditMoeda(Key,(Sender as TEdit).Text);

                 if (fComponente is TDBEdit)
                    then
                        OnKeyPressDoEditMoeda(Key,(Sender as TDBEdit).Text);
            end;

     if fComoNumero
        then
            OnKeyPressDosNumeros(Key);

     if fComoTexto
        then
            OnKeyPressDosTextos(Key);

     if fComoLetras
        then
            OnKeyPressDasLetras(Key);
            
end;

function TCompCtrl.carregaOnEnter:TCompCtrl;
begin
     // on enter
     if (fComponente is TEdit) and (not assigned((fComponente as TEdit).OnEnter))
        then
            (fComponente as TEdit).OnEnter := fOnEnter
        else
            if (fComponente is TDBEdit) and (not assigned((fComponente as TDBEdit).OnEnter))
               then
                   (fComponente as TDBEdit).OnEnter := fOnEnter
               else
                   if (fComponente is TDBComboBox) and (not assigned((fComponente as TDBComboBox).OnEnter))
                      then
                          (fComponente as TDBComboBox).OnEnter := fOnEnter
                   else
                       if (fComponente is TComboBox) and (not assigned((fComponente as TComboBox).OnEnter))
                          then
                              (fComponente as TComboBox).OnEnter := fOnEnter
                          else
                              if (fComponente is TMemo) and (not assigned((fComponente as TMemo).OnEnter))
                                 then
                                     (fComponente as TMemo).OnEnter := fOnEnter
                                 else
                                     if (fComponente is TDBMemo) and (not assigned((fComponente as TDBMemo).OnEnter))
                                        then
                                            (fComponente as TDBMemo).OnEnter := fOnEnter
                                        else
                                            if (fComponente is TDateTimePicker) and (not assigned((fComponente as TDateTimePicker).OnEnter))
                                               then
                                                   (fComponente as TDateTimePicker).OnEnter := fOnEnter;
     result := self;
end;

function TCompCtrl.carregaOnExit:TCompCtrl;
begin
     // on enter
     if (fComponente is TEdit) and (not assigned((fComponente as TEdit).OnExit))
        then
            (fComponente as TEdit).OnExit := fOnExit
        else
            if (fComponente is TDBEdit) and (not assigned((fComponente as TDBEdit).OnExit))
               then
                   (fComponente as TDBEdit).OnExit := fOnExit
               else
                   if (fComponente is TDBComboBox) and (not assigned((fComponente as TDBComboBox).OnExit))
                      then
                          (fComponente as TDBComboBox).OnExit := fOnExit
                   else
                       if (fComponente is TComboBox) and (not assigned((fComponente as TComboBox).OnExit))
                          then
                              (fComponente as TComboBox).OnExit := fOnExit
                          else
                              if (fComponente is TMemo) and (not assigned((fComponente as TMemo).OnExit))
                                 then
                                     (fComponente as TMemo).OnExit := fOnExit
                                 else
                                     if (fComponente is TDBMemo) and (not assigned((fComponente as TDBMemo).OnExit))
                                        then
                                            (fComponente as TDBMemo).OnExit := fOnExit
                                        else
                                            if (fComponente is TDateTimePicker) and (not assigned((fComponente as TDateTimePicker).OnExit))
                                               then
                                                   (fComponente as TDateTimePicker).OnExit := fOnExit;
     result := self;
end;

function TCompCtrl.carregaOnkeyPress:TCompCtrl;
begin
     if (fComponente is TEdit) and (not assigned((fComponente as TEdit).OnKeyPress))
        then
            (fComponente as TEdit).OnKeyPress := fOnKeyPress;

     if (fComponente is TDBEdit) and (not assigned((fComponente as TDBEdit).OnKeyPress))
        then
            (fComponente as TDBEdit).OnKeyPress := fOnKeyPress;
     result := self;
end;

procedure mudaEstadoComponentes(controls:Array of TControl;habilitado:boolean;mudaDeCor:boolean;corHabilitado:TColor);
var qtde,i:integer;
    cor:TColor;
begin
     if habilitado
        then
            cor := corHabilitado
        else
            cor := clBtnFace;
     qtde:=high(controls);
     for i:=0 to qtde do
     begin
          if (mudaDeCor) and (controls[i] is TEdit)
             then
                 (controls[i] as TEdit).Color := cor;

          if (mudaDeCor) and (controls[i] is TMaskEdit)
             then
                 (controls[i] as TMaskEdit).Color := cor;

          if (controls[i] is TDBEdit)
             then
                 (controls[i] as TDBEdit).ReadOnly := not habilitado
             else
                 if (controls[i] is TDBGrid)
                     then
                         (controls[i] as TDBGrid).ReadOnly := not habilitado
                     else
                         if (controls[i] is TDBCheckBox)
                             then
                                 (controls[i] as TDBCheckBox).ReadOnly := not habilitado
                             else
                                 if (controls[i] is TDBMemo)
                                    then
                                        (controls[i] as TDBMemo).ReadOnly := not habilitado
                                    else
                                        controls[i].Enabled := habilitado;
     end;
end;

procedure mudaEstadoComponentes(controls:Array of TControl;habilitado:boolean;corHabilitado:TColor);
begin
     mudaEstadoComponentes(controls,habilitado,true,corHabilitado);
end;

procedure mudaEstadoComponentes(controls:Array of TControl;habilitado:boolean);
begin
     mudaEstadoComponentes(controls,habilitado,false,clWhite);
end;

end.
