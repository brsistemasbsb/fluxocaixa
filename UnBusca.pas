unit UnBusca;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids;

type
  TFormBusca = class(TForm)
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    EditBusca: TEdit;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormBusca: TFormBusca;

implementation

{$R *.dfm}

uses UnDM,FuncoesDLL,UnPrincipal;

procedure TFormBusca.DBGrid1DblClick(Sender: TObject);
begin
     //DM.Abrir(DM.FDQueryBuscaparcod.AsInteger);
     with FormPrincipal do
          begin
               CodigoBusca    := DM.FDQueryBuscacod.AsInteger;
               DescricaoBusca := DM.FDQueryBuscades.AsString;
          end;
     Close;
end;

procedure TFormBusca.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     //DM.FDQueryBusca.Close;
     //DM.FDQueryBusca.Free;
end;

procedure TFormBusca.FormKeyPress(Sender: TObject; var Key: Char);
begin
                         if not (Key in [#8,#13,#27])
        then
            begin
                 if (Key = #32) and (EditBusca.Text=EmptyStr)
                    then Key:=#0  //Para n�o incluir espa�o no in�cio do texto a ser procurado.
                    else
                        begin
                             EditBusca.Text:=concat(EditBusca.Text,uppercase(Key));
                             DM.FDQueryBusca.DisableControls;
                             DM.FDQueryBusca.Filtered:= False;
                             //DM.FDQueryBusca.Filter:=DBGrid1.Columns[DBGrid1.SelectedIndex].FieldName+' ilike ''%'+EditBusca.Text+'%'' ';
                             DM.FDQueryBusca.Filter := 'UPPER('+DBGrid1.Columns[DBGrid1.SelectedIndex].FieldName+') Like ' +UpperCase(QuotedStr('%' + EditBusca.Text + '%'));
                             //Mensagem(DM.FDQueryBusca.SQL.Text,informacao);
                             DM.FDQueryBusca.Filtered:= True;
                             DM.FDQueryBusca.EnableControls;
                        end;
            end
        else
            if key=#8
               then
                   begin
                        //PanelBusca.Caption:=copy(PanelBusca.Caption,1,(length(PanelBusca.Caption)-1));
                        EditBusca.Text:=copy(EditBusca.Text,1,(length(EditBusca.Text)-1));
                        DM.FDQueryBusca.DisableControls;
                        DM.FDQueryBusca.Filtered:= False;
                        //DM.FDQueryBusca.Filter:=DBGrid1.Columns[DBGrid1.SelectedIndex].FieldName+' ilike ''%'+EditBusca.Text+'%'' ';
                        DM.FDQueryBusca.Filter := 'UPPER('+DBGrid1.Columns[DBGrid1.SelectedIndex].FieldName+') Like ' +UpperCase(QuotedStr('%' + EditBusca.Text + '%'));
                        //Mensagem(DM.FDQueryBusca.SQL.Text,informacao);
                        DM.FDQueryBusca.Filtered:= True;
                        DM.FDQueryBusca.EnableControls;
                   end;


     if (Key=#13)
        then
            begin
                 if not DM.FDQueryBusca.IsEmpty
                    then
                        begin
                             with FormPrincipal do
                             begin
                                  CodigoBusca := DM.FDQueryBuscacod.AsInteger;
                                  DescricaoBusca := DM.FDQueryBuscades.AsString
                             end;
                             Close;
                        end;
            end;
            //end;

      if (Key=#27)
        then
            begin
                 //if Mensagem('Tem certeza que deseja desistir da opera��o?',Confirmacao)=mrYes
                    //then
                    //DM.AbrirCli(0);
                    //DM.FDQueryBusca.Close;
                    //DM.FDQueryBusca.Free;
                    Close;
            end;
end;

procedure TFormBusca.FormShow(Sender: TObject);
begin
     //DM.FDQueryBusca.Close;
     //DM.FDQueryBusca.Open;

     EditBusca.Clear;
     DBGrid1.SetFocus;
     CodigoBusca := 0;
end;

end.
