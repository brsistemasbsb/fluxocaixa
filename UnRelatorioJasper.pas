unit UnRelatorioJasper;

interface

uses classes, Forms,windows, ShDocvW,pcnConversao, ShellAPI, UnFuncao, SysUtils;

type
    TFormatoSaida = (tsVisualizacao,tsPDF,tsXLS,tsHTML);
  TRelatorioJasper = class
  protected
         fArquivo:String;
         fListaParametros:TStringList;
         property Parametros:TStringList read fListaParametros write fListaParametros;
         property Arquivo:string read fArquivo write fArquivo;
  public
        constructor Create(p_Arquivo:string);
        procedure addParametro(p_key,p_valor:string);overload;
        procedure addParametro(p_key:string;p_valor:integer);overload;
        procedure addParametro(p_key:string;p_valor:TDateTime);overload;
  end;

  TRelatorioVisualizador = class(TRelatorioJasper)
  public
        procedure executar(p_formato:TFormatoSaida = tsVisualizacao);
        procedure abrirJasper(rel,parametros:string;formato_saida:string = 'VIS';valida_parametros:string = 'N');
        function TFormatoSaidaToStr(const t: TFormatoSaida): string;
  end;

implementation

uses UnDM;//, EventosJasper, UnFuncoesRelatorioJasper;

function TRelatorioVisualizador.TFormatoSaidaToStr(const t: TFormatoSaida): string;
begin
  result := EnumeradoToStr(t, ['VIS','PDF','XLS','HTML'],
                              [tsVisualizacao,tsPDF,tsXLS,tsHTML]);
end;

procedure TRelatorioVisualizador.abrirJasper(rel,parametros:string;formato_saida:string = 'VIS';valida_parametros:string = 'N');
var comando_jasper:string;
begin
     try
        comando_jasper := ' /k java -jar '+ExtractFileDir(Application.ExeName)+'\jasperviewer.jar '+
                          ' Thunder'+
                          ' '+ExtractFileDir(Application.ExeName)+'\relatorios\'+rel+
                          ' '+formato_saida+
                          ' '+valida_parametros;

        if parametros <> EmptyStr
           then
               comando_jasper := comando_jasper + ' '+parametros;
               
        ShellExecute(0, nil, 'cmd.exe', Pchar(comando_jasper), nil, SW_hide);

     except
           on e : Exception do
           begin
                Mensagem(e.Message,Advertencia);
           end;
     end
end;


procedure TRelatorioVisualizador.executar(p_formato:TFormatoSaida = tsVisualizacao);
var l_parametros:string;
    i:integer;
begin
     l_parametros := '';
     for i:=0 to Parametros.Count - 1 do
         l_parametros := l_parametros+' '+Parametros[i];

     abrirJasper(Arquivo,l_parametros,TFormatoSaidaToStr(p_formato));
end;

constructor TRelatorioJasper.Create(p_Arquivo:string);
begin
     self.Arquivo := p_Arquivo;
     self.Parametros := TStringList.Create;
     //self.addParametro('P_USU',IntToStr(CodigoFiscalLogado));
end;

procedure TRelatorioJasper.addParametro(p_key,p_valor:string);
begin
     if p_valor = EmptyStr
        then
            p_valor := '0';
     self.Parametros.Add(p_key+'='+p_valor);
end;

procedure TRelatorioJasper.addParametro(p_key:string;p_valor:integer);
begin
     self.addParametro(p_key,IntToStr(p_valor));
end;

procedure TRelatorioJasper.addParametro(p_key:string;p_valor:TDateTime);
begin
     self.addParametro(p_key,FormatDateTime('yyyy-mm-dd',p_valor));
end;

end.

