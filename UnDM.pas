unit UnDM;

interface

uses


   Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Phys.PGDef,
  FireDAC.Phys.PG, Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Comp.DataSet, Math, FuncoesDLL, StrUtils, IniFiles, Vcl.DBCtrls, Vcl.Mask;



type
  TDM = class(TDataModule)
    FDConnection1: TFDConnection;
    ExecutarSQL: TFDQuery;
    FDPhysPgDriverLink1: TFDPhysPgDriverLink;
    OpenDialog1: TOpenDialog;
    DsFluxo: TDataSource;
    FDQueryFluxo: TFDQuery;
    FDQueryFluxoano: TIntegerField;
    FDQueryFluxomes: TIntegerField;
    FDQueryFluxoapr: TWideStringField;
    FDQueryFluxovl_01: TFMTBCDField;
    FDQueryFluxovl_02: TFMTBCDField;
    FDQueryFluxovl_03: TFMTBCDField;
    FDQueryFluxovl_04: TFMTBCDField;
    FDQueryFluxovl_05: TFMTBCDField;
    FDQueryFluxovl_06: TFMTBCDField;
    FDQueryFluxovl_07: TFMTBCDField;
    FDQueryFluxovl_08: TFMTBCDField;
    FDQueryFluxovl_09: TFMTBCDField;
    FDQueryFluxovl_10: TFMTBCDField;
    FDQueryFluxovl_11: TFMTBCDField;
    FDQueryFluxovl_12: TFMTBCDField;
    FDQueryFluxovl_13: TFMTBCDField;
    FDQueryFluxovl_14: TFMTBCDField;
    FDQueryFluxovl_15: TFMTBCDField;
    FDQueryFluxovl_16: TFMTBCDField;
    FDQueryFluxovl_17: TFMTBCDField;
    FDQueryFluxovl_18: TFMTBCDField;
    FDQueryFluxovl_19: TFMTBCDField;
    FDQueryFluxovl_20: TFMTBCDField;
    FDQueryFluxovl_21: TFMTBCDField;
    FDQueryFluxovl_22: TFMTBCDField;
    FDQueryFluxovl_23: TFMTBCDField;
    FDQueryFluxovl_24: TFMTBCDField;
    FDQueryFluxovl_25: TFMTBCDField;
    FDQueryFluxovl_26: TFMTBCDField;
    FDQueryFluxovl_27: TFMTBCDField;
    FDQueryFluxovl_28: TFMTBCDField;
    FDQueryFluxovl_29: TFMTBCDField;
    FDQueryFluxovl_30: TFMTBCDField;
    FDQueryFluxovl_31: TFMTBCDField;
    DsFluxoTotal: TDataSource;
    FDQueryFluxoTotal: TFDQuery;
    FDQueryFluxoTotalano: TIntegerField;
    FDQueryFluxoTotalmes: TIntegerField;
    FDQueryFluxoTotalordem: TIntegerField;
    FDQueryFluxoTotalapr: TWideStringField;
    FDQueryFluxoTotalvl_01: TFMTBCDField;
    FDQueryFluxoTotalvl_02: TFMTBCDField;
    FDQueryFluxoTotalvl_03: TFMTBCDField;
    FDQueryFluxoTotalvl_04: TFMTBCDField;
    FDQueryFluxoTotalvl_05: TFMTBCDField;
    FDQueryFluxoTotalvl_06: TFMTBCDField;
    FDQueryFluxoTotalvl_07: TFMTBCDField;
    FDQueryFluxoTotalvl_08: TFMTBCDField;
    FDQueryFluxoTotalvl_09: TFMTBCDField;
    FDQueryFluxoTotalvl_10: TFMTBCDField;
    FDQueryFluxoTotalvl_11: TFMTBCDField;
    FDQueryFluxoTotalvl_12: TFMTBCDField;
    FDQueryFluxoTotalvl_13: TFMTBCDField;
    FDQueryFluxoTotalvl_14: TFMTBCDField;
    FDQueryFluxoTotalvl_15: TFMTBCDField;
    FDQueryFluxoTotalvl_16: TFMTBCDField;
    FDQueryFluxoTotalvl_17: TFMTBCDField;
    FDQueryFluxoTotalvl_18: TFMTBCDField;
    FDQueryFluxoTotalvl_19: TFMTBCDField;
    FDQueryFluxoTotalvl_20: TFMTBCDField;
    FDQueryFluxoTotalvl_21: TFMTBCDField;
    FDQueryFluxoTotalvl_22: TFMTBCDField;
    FDQueryFluxoTotalvl_23: TFMTBCDField;
    FDQueryFluxoTotalvl_24: TFMTBCDField;
    FDQueryFluxoTotalvl_25: TFMTBCDField;
    FDQueryFluxoTotalvl_26: TFMTBCDField;
    FDQueryFluxoTotalvl_27: TFMTBCDField;
    FDQueryFluxoTotalvl_28: TFMTBCDField;
    FDQueryFluxoTotalvl_29: TFMTBCDField;
    FDQueryFluxoTotalvl_30: TFMTBCDField;
    FDQueryFluxoTotalvl_31: TFMTBCDField;
    DsFluxoM: TDataSource;
    FDQueryFluxoM: TFDQuery;
    FDQueryFluxoMano: TIntegerField;
    FDQueryFluxoMapr: TWideStringField;
    FDQueryFluxoMordem: TIntegerField;
    FDQueryFluxoMvl_01: TFMTBCDField;
    FDQueryFluxoMvl_02: TFMTBCDField;
    FDQueryFluxoMvl_03: TFMTBCDField;
    FDQueryFluxoMvl_04: TFMTBCDField;
    FDQueryFluxoMvl_05: TFMTBCDField;
    FDQueryFluxoMvl_06: TFMTBCDField;
    FDQueryFluxoMvl_07: TFMTBCDField;
    FDQueryFluxoMvl_08: TFMTBCDField;
    FDQueryFluxoMvl_09: TFMTBCDField;
    FDQueryFluxoMvl_10: TFMTBCDField;
    FDQueryFluxoMvl_11: TFMTBCDField;
    FDQueryFluxoMvl_12: TFMTBCDField;
    DsFluxoMTotal: TDataSource;
    FDQueryFluxoMTotal: TFDQuery;
    FDQueryFluxoMTotalano: TIntegerField;
    FDQueryFluxoMTotalordem: TIntegerField;
    FDQueryFluxoMTotalapr: TWideStringField;
    FDQueryFluxoMTotalvl_01: TFMTBCDField;
    FDQueryFluxoMTotalvl_02: TFMTBCDField;
    FDQueryFluxoMTotalvl_03: TFMTBCDField;
    FDQueryFluxoMTotalvl_04: TFMTBCDField;
    FDQueryFluxoMTotalvl_05: TFMTBCDField;
    FDQueryFluxoMTotalvl_06: TFMTBCDField;
    FDQueryFluxoMTotalvl_07: TFMTBCDField;
    FDQueryFluxoMTotalvl_08: TFMTBCDField;
    FDQueryFluxoMTotalvl_09: TFMTBCDField;
    FDQueryFluxoMTotalvl_10: TFMTBCDField;
    FDQueryFluxoMTotalvl_11: TFMTBCDField;
    FDQueryFluxoMTotalvl_12: TFMTBCDField;
    FDQueryFluxovl_total: TFMTBCDField;
    FDQueryFluxoTotalvl_total: TFMTBCDField;
    FDQueryFluxoMvl_total: TFMTBCDField;
    FDQueryFluxoMTotalvl_total: TFMTBCDField;
    DsDetalhe: TDataSource;
    FDQueryDetalhe: TFDQuery;
    FDQueryDetalhecliente_id: TIntegerField;
    FDQueryDetalhefilial_id: TIntegerField;
    FDQueryDetalheano: TIntegerField;
    FDQueryDetalhemes: TIntegerField;
    FDQueryDetalhedia: TIntegerField;
    FDQueryDetalheordem: TIntegerField;
    FDQueryDetalhevlnat: TFMTBCDField;
    FDQueryDetalhegrupoconta_id: TIntegerField;
    FDQueryDetalhegrupoconta: TWideStringField;
    FDQueryDetalheconta_id: TIntegerField;
    FDQueryDetalheconta: TWideStringField;
    FDQueryDetalheclassifconta: TWideStringField;
    FDQueryDetalheparceiro_id: TIntegerField;
    FDQueryDetalheparceiro: TWideStringField;
    FDQueryDetalhecobranca_id: TIntegerField;
    FDQueryDetalhecobranca: TWideStringField;
    FDQueryDetalhenumdocumento: TWideStringField;
    FDQueryDetalheparc: TWideStringField;
    FDQueryDetalheobservacao: TWideMemoField;
    FDQueryDetalhetipo_movimento: TWideStringField;
    FDQueryDetalhetipo: TWideStringField;
    DsBusca: TDataSource;
    FDQueryBusca: TFDQuery;
    FDQueryBuscacod: TIntegerField;
    FDQueryBuscades: TWideStringField;
    procedure FDQueryConfAfterPost(DataSet: TDataSet);
    procedure FDQueryCertAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UnStatus;

{$R *.dfm}

procedure TDM.FDQueryCertAfterPost(DataSet: TDataSet);
begin
     FDConnection1.StartTransaction;
     try
        TFDQuery(DataSet).ApplyUpdates;
        FDConnection1.Commit;
        TFDQuery(DataSet).CommitUpdates;
      except
            TFDQuery(DataSet).CancelUpdates;
            FDConnection1.Rollback;
      end;
end;

procedure TDM.FDQueryConfAfterPost(DataSet: TDataSet);
begin
     FDConnection1.StartTransaction;
     try
        TFDQuery(DataSet).ApplyUpdates;
        FDConnection1.Commit;
        TFDQuery(DataSet).CommitUpdates;
      except
            TFDQuery(DataSet).CancelUpdates;
            FDConnection1.Rollback;
      end;
end;

end.
