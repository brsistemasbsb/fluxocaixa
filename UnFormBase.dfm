object FormBase: TFormBase
  Left = 284
  Top = 265
  Caption = 'FormBase'
  ClientHeight = 320
  ClientWidth = 663
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 301
    Width = 663
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object PanelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 663
    Height = 30
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = clMaroon
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 618
  end
end
