unit EventosDataSet;

interface

uses FireDAC.Comp.Client,SysUtils,DateUtils,variants,Controls, forms,shellapi, windows
     ,classes, DB, StdCtrls;

     procedure validaCamposObrigatorios(Ds:TDataSource);
     procedure OnNewRecordDoDataSet(Campo:TField);  //Manda o foco para o campo passado como par�metro
     procedure copiarRegistro(Ds:TDataSource;EditDes:array of TEdit;TextoSql:String; pSugerirNome:boolean = true;pColunaNome : integer = 1);
implementation

uses StrUtils, UnExcecaoDataSet, UnControleTransacao, Eventos;

procedure copiarRegistro(Ds:TDataSource;EditDes:array of TEdit;TextoSql:String; pSugerirNome:boolean = true;pColunaNome : integer = 1);
var
   i:byte;
   Edit:array of String;
   nome_campo:string;
begin
  //Armazenando os EditsDes
  SetLength(Edit,Length(EditDes));
  for i:=0 to (Length(EditDes)-1) do
  begin
      Edit[i]:=EditDes[i].Text;
  end;

  with TFDQuery.Create(Application) do
  begin
      ConnectionName := TControleTransacao.getBaseDados.ConnectionName;
      SQL.Clear;
      SQL.Text:=TextoSql;
      Open;
      Ds.DataSet.Insert;
      for i:=0 to Ds.DataSet.FieldCount-1 do
      begin
           nome_campo:=Ds.DataSet.Fields[i].FieldName;
           if not (fieldByName(nome_campo).IsNull)
              then
                  Ds.DataSet.FieldByName(nome_campo).Value:=fieldByName(nome_campo).Value;
      end;
      if pSugerirNome
        then
          Ds.DataSet.Fields[1].AsString:=concat(Fields[1].AsString+' - COPIA');
      Free;
  end;

  //Copiando os EditsDes
  for i:=0 to (Length(EditDes)-1) do
  begin
      EditDes[i].Text:=Edit[i];
  end;
end;


procedure validaCamposObrigatorios(Ds:TDataSource);
var i:byte;
begin
     with (Ds.DataSet) do
     begin
          for i:=0 to FieldCount - 1 do
          begin
               with Fields[i] do
               begin
                    if (Required)
                       then
                           if not (DataType in [ftString,ftBlob,ftMemo,ftFmtMemo,ftFixedChar,ftWideString])
                              then
                                  begin
                                       if IsNull
                                          then
                                              begin
                                                   Fields[i].FocusControl;
                                                   raise TExcecaoDataSet.Create('� obrigat�rio o preenchimento do campo "'+ (Ds.DataSet).Fields[i].DisplayLabel+ '"',Fields[i]);
                                              end;
                                  end
                              else
                                  if AsString = EmptyStr
                                     then
                                         begin
                                              Fields[i].FocusControl;
                                              raise TExcecaoDataSet.Create('� obrigat�rio o preenchimento do campo "'+ (Ds.DataSet).Fields[i].DisplayLabel+ '"',Fields[i]);
                                         end;
               end;
          end;
     end;
end;

procedure OnNewRecordDoDataSet(Campo:TField);  //Manda o foco para o campo passado como par�metro
begin
     ConteudoDBEditAntLook:=EmptyStr;
     Campo.FocusControl;
end;



end.
