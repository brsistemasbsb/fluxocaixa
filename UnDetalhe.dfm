object FormDetalhe: TFormDetalhe
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  ClientHeight = 522
  ClientWidth = 954
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  Visible = True
  TextHeight = 13
  object StaticText1: TStaticText
    Left = 88
    Top = 40
    Width = 4
    Height = 4
    TabOrder = 0
  end
  object DBGridDiario: TDBGrid
    Left = 0
    Top = 0
    Width = 954
    Height = 503
    Align = alClient
    DataSource = DM.DsDetalhe
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'tipo'
        Title.Caption = 'Tipo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'parceiro_id'
        Title.Caption = 'C'#243'd'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'parceiro'
        Title.Caption = 'Parceiro'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cobranca_id'
        Title.Caption = 'C'#243'd'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cobranca'
        Title.Caption = 'Cobran'#231'a'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'conta_id'
        Title.Caption = 'C'#243'd'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'conta'
        Title.Caption = 'Conta'
        Width = 196
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'vlnat'
        Title.Caption = 'Valor'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'numdocumento'
        Title.Caption = 'Num.Doc.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'parc'
        Title.Caption = 'Parc.'
        Width = 45
        Visible = True
      end>
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 503
    Width = 954
    Height = 19
    Panels = <
      item
        Width = 800
      end
      item
        Width = 50
      end>
  end
end
