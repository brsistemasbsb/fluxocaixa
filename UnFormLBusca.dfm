object FormLBusca: TFormLBusca
  Left = 0
  Top = 77
  HorzScrollBar.Smooth = True
  VertScrollBar.Smooth = True
  Caption = 'Localiza'#231#227'o'
  ClientHeight = 449
  ClientWidth = 674
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 674
    Height = 449
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 0
    object DBGridB: TDBGrid
      Left = 2
      Top = 2
      Width = 670
      Height = 421
      Align = alClient
      Color = clInfoBk
      DataSource = DsLoc
      DrawingStyle = gdsClassic
      FixedColor = 12615680
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnColEnter = DBGridBColEnter
      OnDrawColumnCell = DBGridBDrawColumnCell
      OnDblClick = DBGridBDblClick
      OnKeyPress = DBGridBKeyPress
      OnTitleClick = DBGridBTitleClick
    end
    object Panel2: TPanel
      Left = 2
      Top = 423
      Width = 670
      Height = 24
      Align = alBottom
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 1
      object StatusBar1: TStatusBar
        Left = 2
        Top = 1
        Width = 666
        Height = 21
        Panels = <
          item
            Text = 'Busca por C'#211'DIGO:'
            Width = 125
          end
          item
            Bevel = pbRaised
            Width = 90
          end
          item
            Bevel = pbNone
            Width = 50
          end>
      end
    end
  end
  object DsLoc: TDataSource
    DataSet = QueryLoc
    Left = 120
    Top = 48
  end
  object Timer1: TTimer
    Interval = 300
    OnTimer = Timer1Timer
    Left = 144
    Top = 152
  end
  object QueryLoc: TFDQuery
    Connection = DMConexao.dbzae
    Left = 160
    Top = 48
  end
end
