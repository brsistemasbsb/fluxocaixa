unit FuncoesDLL;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Phys.PGDef, Vcl.StdCtrls, FireDAC.Phys.PG, Data.DB,
  FireDAC.Comp.Client;

type

  TTipoMensagem  = (tmExclusao,tmGravacao,tmAdvertencia,tmInformacao,tmConfirmacao,tmResAC,tmResAEC,tmErro);
  TTipoMensagens = set of TTipoMensagem;

const

     Exclusao    = [tmExclusao];
     Gravacao    = [tmGravacao];
     Advertencia = [tmAdvertencia];
     Informacao  = [tmInformacao];
     Confirmacao = [tmConfirmacao];
     ResAC       = [tmResAC];
     ResAEC      = [tmResAEC];
     Erro        = [tmErro];

function VersaoDLL: String; stdcall;


function Mensagem(Texto:WideString;TipoMensagem: TTipoMensagens):Integer; stdcall;

function SomenteNumeros(Documento:String):String;  //Retorna apenas n�meros de uma determinada string


implementation

  function VersaoDLL; external 'ThunderDLL.dll';

  function Mensagem; external 'ThunderDLL.dll';

  function Somentenumeros; external 'ThunderDLL.dll';


end.
