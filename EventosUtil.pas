unit EventosUtil;
interface

uses
  Messages, SysUtils, comctrls, dbgrids, Classes, Db, dbctrls, forms,
  FireDAC.Comp.Client;


   {Fim Se��o de Declara��o de Vari�veis}

   procedure AbrirQuery(query:TFDQuery);

implementation

uses Eventos;

//uses Eventos;

procedure AbrirQuery(query:TFDQuery);
var  lSql:TStringList;
     lCont:integer;
begin
     if UsuarioLogado = 'thunder'
        then
            begin
                 lSql := TStringList.Create;
                 lSql.Text := query.SQL.Text;
                 lSql.Add('Parametros');
                 for lCont := 0 to query.Params.Count-1 do
                 begin
                      lSql.Add(':'+query.Params[lCont].Name + ' - '+query.Params[lCont].AsString);
                 end;
                 lSql.Add('==============================================================================================');
                 lSql.SaveToFile(ExtractFilePath(Application.ExeName)+'\'+FormatDateTime('yyyy_mm_dd_hh',RetornaDataHora)+'_'+query.Name+'.th');
            end;
     query.Open;
end;


end.
